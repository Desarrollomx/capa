require 'test_helper'

class MultypromsControllerTest < ActionController::TestCase
  setup do
    @multyprom = multyproms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:multyproms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create multyprom" do
    assert_difference('Multyprom.count') do
      post :create, multyprom: { fecha: @multyprom.fecha, h10: @multyprom.h10, h1: @multyprom.h1, h2: @multyprom.h2, h3: @multyprom.h3, h4: @multyprom.h4, h5: @multyprom.h5, h7: @multyprom.h7, h8: @multyprom.h8, h9: @multyprom.h9, h: @multyprom.h, m10: @multyprom.m10, m1: @multyprom.m1, m2: @multyprom.m2, m3: @multyprom.m3, m4: @multyprom.m4, m5: @multyprom.m5, m6: @multyprom.m6, m7: @multyprom.m7, m8: @multyprom.m8, m9: @multyprom.m9, m: @multyprom.m }
    end

    assert_redirected_to multyprom_path(assigns(:multyprom))
  end

  test "should show multyprom" do
    get :show, id: @multyprom
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @multyprom
    assert_response :success
  end

  test "should update multyprom" do
    patch :update, id: @multyprom, multyprom: { fecha: @multyprom.fecha, h10: @multyprom.h10, h1: @multyprom.h1, h2: @multyprom.h2, h3: @multyprom.h3, h4: @multyprom.h4, h5: @multyprom.h5, h7: @multyprom.h7, h8: @multyprom.h8, h9: @multyprom.h9, h: @multyprom.h, m10: @multyprom.m10, m1: @multyprom.m1, m2: @multyprom.m2, m3: @multyprom.m3, m4: @multyprom.m4, m5: @multyprom.m5, m6: @multyprom.m6, m7: @multyprom.m7, m8: @multyprom.m8, m9: @multyprom.m9, m: @multyprom.m }
    assert_redirected_to multyprom_path(assigns(:multyprom))
  end

  test "should destroy multyprom" do
    assert_difference('Multyprom.count', -1) do
      delete :destroy, id: @multyprom
    end

    assert_redirected_to multyproms_path
  end
end
