require 'test_helper'

class EtniaControllerTest < ActionController::TestCase
  setup do
    @etnium = etnia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:etnia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create etnium" do
    assert_difference('Etnium.count') do
      post :create, etnium: { etnia: @etnium.etnia }
    end

    assert_redirected_to etnium_path(assigns(:etnium))
  end

  test "should show etnium" do
    get :show, id: @etnium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @etnium
    assert_response :success
  end

  test "should update etnium" do
    patch :update, id: @etnium, etnium: { etnia: @etnium.etnia }
    assert_redirected_to etnium_path(assigns(:etnium))
  end

  test "should destroy etnium" do
    assert_difference('Etnium.count', -1) do
      delete :destroy, id: @etnium
    end

    assert_redirected_to etnia_path
  end
end
