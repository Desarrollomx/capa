require 'test_helper'

class TratamientosBcsControllerTest < ActionController::TestCase
  setup do
    @tratamientos_bc = tratamientos_bcs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tratamientos_bcs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tratamientos_bc" do
    assert_difference('TratamientosBc.count') do
      post :create, tratamientos_bc: { fecha: @tratamientos_bc.fecha, h: @tratamientos_bc.h, m: @tratamientos_bc.m }
    end

    assert_redirected_to tratamientos_bc_path(assigns(:tratamientos_bc))
  end

  test "should show tratamientos_bc" do
    get :show, id: @tratamientos_bc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tratamientos_bc
    assert_response :success
  end

  test "should update tratamientos_bc" do
    patch :update, id: @tratamientos_bc, tratamientos_bc: { fecha: @tratamientos_bc.fecha, h: @tratamientos_bc.h, m: @tratamientos_bc.m }
    assert_redirected_to tratamientos_bc_path(assigns(:tratamientos_bc))
  end

  test "should destroy tratamientos_bc" do
    assert_difference('TratamientosBc.count', -1) do
      delete :destroy, id: @tratamientos_bc
    end

    assert_redirected_to tratamientos_bcs_path
  end
end
