require 'test_helper'

class BeneficiadosControllerTest < ActionController::TestCase
  setup do
    @beneficiado = beneficiados(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:beneficiados)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create beneficiado" do
    assert_difference('Beneficiado.count') do
      post :create, beneficiado: { fecha: @beneficiado.fecha, h10: @beneficiado.h10, h1: @beneficiado.h1, h2: @beneficiado.h2, h3: @beneficiado.h3, h4: @beneficiado.h4, h5: @beneficiado.h5, h7: @beneficiado.h7, h8: @beneficiado.h8, h9: @beneficiado.h9, h: @beneficiado.h, m10: @beneficiado.m10, m1: @beneficiado.m1, m2: @beneficiado.m2, m3: @beneficiado.m3, m4: @beneficiado.m4, m5: @beneficiado.m5, m6: @beneficiado.m6, m7: @beneficiado.m7, m8: @beneficiado.m8, m9: @beneficiado.m9, m: @beneficiado.m }
    end

    assert_redirected_to beneficiado_path(assigns(:beneficiado))
  end

  test "should show beneficiado" do
    get :show, id: @beneficiado
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @beneficiado
    assert_response :success
  end

  test "should update beneficiado" do
    patch :update, id: @beneficiado, beneficiado: { fecha: @beneficiado.fecha, h10: @beneficiado.h10, h1: @beneficiado.h1, h2: @beneficiado.h2, h3: @beneficiado.h3, h4: @beneficiado.h4, h5: @beneficiado.h5, h7: @beneficiado.h7, h8: @beneficiado.h8, h9: @beneficiado.h9, h: @beneficiado.h, m10: @beneficiado.m10, m1: @beneficiado.m1, m2: @beneficiado.m2, m3: @beneficiado.m3, m4: @beneficiado.m4, m5: @beneficiado.m5, m6: @beneficiado.m6, m7: @beneficiado.m7, m8: @beneficiado.m8, m9: @beneficiado.m9, m: @beneficiado.m }
    assert_redirected_to beneficiado_path(assigns(:beneficiado))
  end

  test "should destroy beneficiado" do
    assert_difference('Beneficiado.count', -1) do
      delete :destroy, id: @beneficiado
    end

    assert_redirected_to beneficiados_path
  end
end
