require 'test_helper'

class CapacitacionesOtsControllerTest < ActionController::TestCase
  setup do
    @capacitaciones_ot = capacitaciones_ots(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:capacitaciones_ots)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create capacitaciones_ot" do
    assert_difference('CapacitacionesOt.count') do
      post :create, capacitaciones_ot: { fecha: @capacitaciones_ot.fecha, h: @capacitaciones_ot.h, m: @capacitaciones_ot.m }
    end

    assert_redirected_to capacitaciones_ot_path(assigns(:capacitaciones_ot))
  end

  test "should show capacitaciones_ot" do
    get :show, id: @capacitaciones_ot
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @capacitaciones_ot
    assert_response :success
  end

  test "should update capacitaciones_ot" do
    patch :update, id: @capacitaciones_ot, capacitaciones_ot: { fecha: @capacitaciones_ot.fecha, h: @capacitaciones_ot.h, m: @capacitaciones_ot.m }
    assert_redirected_to capacitaciones_ot_path(assigns(:capacitaciones_ot))
  end

  test "should destroy capacitaciones_ot" do
    assert_difference('CapacitacionesOt.count', -1) do
      delete :destroy, id: @capacitaciones_ot
    end

    assert_redirected_to capacitaciones_ots_path
  end
end
