require 'test_helper'

class ConsultasSubsControllerTest < ActionController::TestCase
  setup do
    @consultas_sub = consultas_subs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:consultas_subs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create consultas_sub" do
    assert_difference('ConsultasSub.count') do
      post :create, consultas_sub: { fecha: @consultas_sub.fecha, h: @consultas_sub.h, m: @consultas_sub.m }
    end

    assert_redirected_to consultas_sub_path(assigns(:consultas_sub))
  end

  test "should show consultas_sub" do
    get :show, id: @consultas_sub
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @consultas_sub
    assert_response :success
  end

  test "should update consultas_sub" do
    patch :update, id: @consultas_sub, consultas_sub: { fecha: @consultas_sub.fecha, h: @consultas_sub.h, m: @consultas_sub.m }
    assert_redirected_to consultas_sub_path(assigns(:consultas_sub))
  end

  test "should destroy consultas_sub" do
    assert_difference('ConsultasSub.count', -1) do
      delete :destroy, id: @consultas_sub
    end

    assert_redirected_to consultas_subs_path
  end
end
