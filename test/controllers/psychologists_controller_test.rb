require 'test_helper'

class PsychologistsControllerTest < ActionController::TestCase
  setup do
    @psychologist = psychologists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:psychologists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create psychologist" do
    assert_difference('Psychologist.count') do
      post :create, psychologist: { apellidoM: @psychologist.apellidoM, apellidoP: @psychologist.apellidoP, nombre: @psychologist.nombre, telefono: @psychologist.telefono }
    end

    assert_redirected_to psychologist_path(assigns(:psychologist))
  end

  test "should show psychologist" do
    get :show, id: @psychologist
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @psychologist
    assert_response :success
  end

  test "should update psychologist" do
    patch :update, id: @psychologist, psychologist: { apellidoM: @psychologist.apellidoM, apellidoP: @psychologist.apellidoP, nombre: @psychologist.nombre, telefono: @psychologist.telefono }
    assert_redirected_to psychologist_path(assigns(:psychologist))
  end

  test "should destroy psychologist" do
    assert_difference('Psychologist.count', -1) do
      delete :destroy, id: @psychologist
    end

    assert_redirected_to psychologists_path
  end
end
