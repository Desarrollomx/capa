require 'test_helper'

class CivilsControllerTest < ActionController::TestCase
  setup do
    @civil = civils(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:civils)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create civil" do
    assert_difference('Civil.count') do
      post :create, civil: { civil: @civil.civil }
    end

    assert_redirected_to civil_path(assigns(:civil))
  end

  test "should show civil" do
    get :show, id: @civil
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @civil
    assert_response :success
  end

  test "should update civil" do
    patch :update, id: @civil, civil: { civil: @civil.civil }
    assert_redirected_to civil_path(assigns(:civil))
  end

  test "should destroy civil" do
    assert_difference('Civil.count', -1) do
      delete :destroy, id: @civil
    end

    assert_redirected_to civils_path
  end
end
