require 'test_helper'

class ConsultasPvsControllerTest < ActionController::TestCase
  setup do
    @consultas_pv = consultas_pvs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:consultas_pvs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create consultas_pv" do
    assert_difference('ConsultasPv.count') do
      post :create, consultas_pv: { fecha: @consultas_pv.fecha, h: @consultas_pv.h, m: @consultas_pv.m }
    end

    assert_redirected_to consultas_pv_path(assigns(:consultas_pv))
  end

  test "should show consultas_pv" do
    get :show, id: @consultas_pv
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @consultas_pv
    assert_response :success
  end

  test "should update consultas_pv" do
    patch :update, id: @consultas_pv, consultas_pv: { fecha: @consultas_pv.fecha, h: @consultas_pv.h, m: @consultas_pv.m }
    assert_redirected_to consultas_pv_path(assigns(:consultas_pv))
  end

  test "should destroy consultas_pv" do
    assert_difference('ConsultasPv.count', -1) do
      delete :destroy, id: @consultas_pv
    end

    assert_redirected_to consultas_pvs_path
  end
end
