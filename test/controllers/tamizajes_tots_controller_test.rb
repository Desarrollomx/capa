require 'test_helper'

class TamizajesTotsControllerTest < ActionController::TestCase
  setup do
    @tamizajes_tot = tamizajes_tots(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tamizajes_tots)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tamizajes_tot" do
    assert_difference('TamizajesTot.count') do
      post :create, tamizajes_tot: { fecha: @tamizajes_tot.fecha, h: @tamizajes_tot.h, m: @tamizajes_tot.m }
    end

    assert_redirected_to tamizajes_tot_path(assigns(:tamizajes_tot))
  end

  test "should show tamizajes_tot" do
    get :show, id: @tamizajes_tot
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tamizajes_tot
    assert_response :success
  end

  test "should update tamizajes_tot" do
    patch :update, id: @tamizajes_tot, tamizajes_tot: { fecha: @tamizajes_tot.fecha, h: @tamizajes_tot.h, m: @tamizajes_tot.m }
    assert_redirected_to tamizajes_tot_path(assigns(:tamizajes_tot))
  end

  test "should destroy tamizajes_tot" do
    assert_difference('TamizajesTot.count', -1) do
      delete :destroy, id: @tamizajes_tot
    end

    assert_redirected_to tamizajes_tots_path
  end
end
