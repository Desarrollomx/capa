require 'test_helper'

class CapacitacionesRecsControllerTest < ActionController::TestCase
  setup do
    @capacitaciones_rec = capacitaciones_recs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:capacitaciones_recs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create capacitaciones_rec" do
    assert_difference('CapacitacionesRec.count') do
      post :create, capacitaciones_rec: { fecha: @capacitaciones_rec.fecha, h: @capacitaciones_rec.h, m: @capacitaciones_rec.m }
    end

    assert_redirected_to capacitaciones_rec_path(assigns(:capacitaciones_rec))
  end

  test "should show capacitaciones_rec" do
    get :show, id: @capacitaciones_rec
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @capacitaciones_rec
    assert_response :success
  end

  test "should update capacitaciones_rec" do
    patch :update, id: @capacitaciones_rec, capacitaciones_rec: { fecha: @capacitaciones_rec.fecha, h: @capacitaciones_rec.h, m: @capacitaciones_rec.m }
    assert_redirected_to capacitaciones_rec_path(assigns(:capacitaciones_rec))
  end

  test "should destroy capacitaciones_rec" do
    assert_difference('CapacitacionesRec.count', -1) do
      delete :destroy, id: @capacitaciones_rec
    end

    assert_redirected_to capacitaciones_recs_path
  end
end
