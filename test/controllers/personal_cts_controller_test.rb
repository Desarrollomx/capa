require 'test_helper'

class PersonalCtsControllerTest < ActionController::TestCase
  setup do
    @personal_ct = personal_cts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:personal_cts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create personal_ct" do
    assert_difference('PersonalCt.count') do
      post :create, personal_ct: { fecha: @personal_ct.fecha, h: @personal_ct.h, m: @personal_ct.m }
    end

    assert_redirected_to personal_ct_path(assigns(:personal_ct))
  end

  test "should show personal_ct" do
    get :show, id: @personal_ct
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @personal_ct
    assert_response :success
  end

  test "should update personal_ct" do
    patch :update, id: @personal_ct, personal_ct: { fecha: @personal_ct.fecha, h: @personal_ct.h, m: @personal_ct.m }
    assert_redirected_to personal_ct_path(assigns(:personal_ct))
  end

  test "should destroy personal_ct" do
    assert_difference('PersonalCt.count', -1) do
      delete :destroy, id: @personal_ct
    end

    assert_redirected_to personal_cts_path
  end
end
