require 'test_helper'

class AdolescentesItsControllerTest < ActionController::TestCase
  setup do
    @adolescentes_it = adolescentes_its(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:adolescentes_its)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create adolescentes_it" do
    assert_difference('AdolescentesIt.count') do
      post :create, adolescentes_it: { fecha: @adolescentes_it.fecha, h: @adolescentes_it.h, m: @adolescentes_it.m }
    end

    assert_redirected_to adolescentes_it_path(assigns(:adolescentes_it))
  end

  test "should show adolescentes_it" do
    get :show, id: @adolescentes_it
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @adolescentes_it
    assert_response :success
  end

  test "should update adolescentes_it" do
    patch :update, id: @adolescentes_it, adolescentes_it: { fecha: @adolescentes_it.fecha, h: @adolescentes_it.h, m: @adolescentes_it.m }
    assert_redirected_to adolescentes_it_path(assigns(:adolescentes_it))
  end

  test "should destroy adolescentes_it" do
    assert_difference('AdolescentesIt.count', -1) do
      delete :destroy, id: @adolescentes_it
    end

    assert_redirected_to adolescentes_its_path
  end
end
