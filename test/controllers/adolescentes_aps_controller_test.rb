require 'test_helper'

class AdolescentesApsControllerTest < ActionController::TestCase
  setup do
    @adolescentes_ap = adolescentes_aps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:adolescentes_aps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create adolescentes_ap" do
    assert_difference('AdolescentesAp.count') do
      post :create, adolescentes_ap: { fecha: @adolescentes_ap.fecha, h: @adolescentes_ap.h, m: @adolescentes_ap.m }
    end

    assert_redirected_to adolescentes_ap_path(assigns(:adolescentes_ap))
  end

  test "should show adolescentes_ap" do
    get :show, id: @adolescentes_ap
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @adolescentes_ap
    assert_response :success
  end

  test "should update adolescentes_ap" do
    patch :update, id: @adolescentes_ap, adolescentes_ap: { fecha: @adolescentes_ap.fecha, h: @adolescentes_ap.h, m: @adolescentes_ap.m }
    assert_redirected_to adolescentes_ap_path(assigns(:adolescentes_ap))
  end

  test "should destroy adolescentes_ap" do
    assert_difference('AdolescentesAp.count', -1) do
      delete :destroy, id: @adolescentes_ap
    end

    assert_redirected_to adolescentes_aps_path
  end
end
