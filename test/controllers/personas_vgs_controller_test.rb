require 'test_helper'

class PersonasVgsControllerTest < ActionController::TestCase
  setup do
    @personas_vg = personas_vgs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:personas_vgs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create personas_vg" do
    assert_difference('PersonasVg.count') do
      post :create, personas_vg: { fecha: @personas_vg.fecha, h: @personas_vg.h, m: @personas_vg.m }
    end

    assert_redirected_to personas_vg_path(assigns(:personas_vg))
  end

  test "should show personas_vg" do
    get :show, id: @personas_vg
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @personas_vg
    assert_response :success
  end

  test "should update personas_vg" do
    patch :update, id: @personas_vg, personas_vg: { fecha: @personas_vg.fecha, h: @personas_vg.h, m: @personas_vg.m }
    assert_redirected_to personas_vg_path(assigns(:personas_vg))
  end

  test "should destroy personas_vg" do
    assert_difference('PersonasVg.count', -1) do
      delete :destroy, id: @personas_vg
    end

    assert_redirected_to personas_vgs_path
  end
end
