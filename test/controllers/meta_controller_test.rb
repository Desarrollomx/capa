require 'test_helper'

class MetaControllerTest < ActionController::TestCase
  setup do
    @metum = meta(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:meta)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create metum" do
    assert_difference('Metum.count') do
      post :create, metum: { adol_acc_prev: @metum.adol_acc_prev, adol_ini_trat: @metum.adol_ini_trat, cap_otor_prev_trat: @metum.cap_otor_prev_trat, cap_rec_prev_trat: @metum.cap_rec_prev_trat, cons_1a_vez: @metum.cons_1a_vez, cons_subs: @metum.cons_subs, pers_cap_ces_tab: @metum.pers_cap_ces_tab, pers_det_viol_gen: @metum.pers_det_viol_gen, tamizajes: @metum.tamizajes, trat_brev_conc: @metum.trat_brev_conc }
    end

    assert_redirected_to metum_path(assigns(:metum))
  end

  test "should show metum" do
    get :show, id: @metum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @metum
    assert_response :success
  end

  test "should update metum" do
    patch :update, id: @metum, metum: { adol_acc_prev: @metum.adol_acc_prev, adol_ini_trat: @metum.adol_ini_trat, cap_otor_prev_trat: @metum.cap_otor_prev_trat, cap_rec_prev_trat: @metum.cap_rec_prev_trat, cons_1a_vez: @metum.cons_1a_vez, cons_subs: @metum.cons_subs, pers_cap_ces_tab: @metum.pers_cap_ces_tab, pers_det_viol_gen: @metum.pers_det_viol_gen, tamizajes: @metum.tamizajes, trat_brev_conc: @metum.trat_brev_conc }
    assert_redirected_to metum_path(assigns(:metum))
  end

  test "should destroy metum" do
    assert_difference('Metum.count', -1) do
      delete :destroy, id: @metum
    end

    assert_redirected_to meta_path
  end
end
