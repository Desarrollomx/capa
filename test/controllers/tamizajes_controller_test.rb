require 'test_helper'

class TamizajesControllerTest < ActionController::TestCase
  setup do
    @tamizaje = tamizajes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tamizajes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tamizaje" do
    assert_difference('Tamizaje.count') do
      post :create, tamizaje: { apellidoM: @tamizaje.apellidoM, apellidoP: @tamizaje.apellidoP, colonia_escuela: @tamizaje.colonia_escuela, edad: @tamizaje.edad, folio_cuestionario: @tamizaje.folio_cuestionario, grado: @tamizaje.grado, grupo: @tamizaje.grupo, municipio_escuela: @tamizaje.municipio_escuela, nivel_escolar: @tamizaje.nivel_escolar, nombre: @tamizaje.nombre, nombre_escuela: @tamizaje.nombre_escuela, p_conductaA: @tamizaje.p_conductaA, p_interesL: @tamizaje.p_interesL, p_nivelE: @tamizaje.p_nivelE, p_relacionA: @tamizaje.p_relacionA, p_relacionF: @tamizaje.p_relacionF, p_saludM: @tamizaje.p_saludM, p_usoS: @tamizaje.p_usoS, resultado: @tamizaje.resultado, sexo: @tamizaje.sexo, total_conducta_agresiva: @tamizaje.total_conducta_agresiva, total_interes_laboral: @tamizaje.total_interes_laboral, total_nivel_educativo: @tamizaje.total_nivel_educativo, total_relacion_amigos: @tamizaje.total_relacion_amigos, total_relacion_familiar: @tamizaje.total_relacion_familiar, total_salud_mental: @tamizaje.total_salud_mental, total_uso_sustancias: @tamizaje.total_uso_sustancias }
    end

    assert_redirected_to tamizaje_path(assigns(:tamizaje))
  end

  test "should show tamizaje" do
    get :show, id: @tamizaje
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tamizaje
    assert_response :success
  end

  test "should update tamizaje" do
    patch :update, id: @tamizaje, tamizaje: { apellidoM: @tamizaje.apellidoM, apellidoP: @tamizaje.apellidoP, colonia_escuela: @tamizaje.colonia_escuela, edad: @tamizaje.edad, folio_cuestionario: @tamizaje.folio_cuestionario, grado: @tamizaje.grado, grupo: @tamizaje.grupo, municipio_escuela: @tamizaje.municipio_escuela, nivel_escolar: @tamizaje.nivel_escolar, nombre: @tamizaje.nombre, nombre_escuela: @tamizaje.nombre_escuela, p_conductaA: @tamizaje.p_conductaA, p_interesL: @tamizaje.p_interesL, p_nivelE: @tamizaje.p_nivelE, p_relacionA: @tamizaje.p_relacionA, p_relacionF: @tamizaje.p_relacionF, p_saludM: @tamizaje.p_saludM, p_usoS: @tamizaje.p_usoS, resultado: @tamizaje.resultado, sexo: @tamizaje.sexo, total_conducta_agresiva: @tamizaje.total_conducta_agresiva, total_interes_laboral: @tamizaje.total_interes_laboral, total_nivel_educativo: @tamizaje.total_nivel_educativo, total_relacion_amigos: @tamizaje.total_relacion_amigos, total_relacion_familiar: @tamizaje.total_relacion_familiar, total_salud_mental: @tamizaje.total_salud_mental, total_uso_sustancias: @tamizaje.total_uso_sustancias }
    assert_redirected_to tamizaje_path(assigns(:tamizaje))
  end

  test "should destroy tamizaje" do
    assert_difference('Tamizaje.count', -1) do
      delete :destroy, id: @tamizaje
    end

    assert_redirected_to tamizajes_path
  end
end
