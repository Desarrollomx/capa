# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160815053132) do

  create_table "adolescentes_aps", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "m3"
    t.integer  "m4"
  end

  add_index "adolescentes_aps", ["psychologist_id"], name: "index_adolescentes_aps_on_psychologist_id"

  create_table "adolescentes_its", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "m3"
    t.integer  "m4"
  end

  add_index "adolescentes_its", ["psychologist_id"], name: "index_adolescentes_its_on_psychologist_id"

  create_table "aplicacion_tamizajes", force: :cascade do |t|
    t.text     "escuela"
    t.string   "colonia"
    t.string   "status"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "municipio_id"
    t.integer  "nivel_estudio_id"
    t.integer  "psychologist_id"
  end

  add_index "aplicacion_tamizajes", ["municipio_id"], name: "index_aplicacion_tamizajes_on_municipio_id"
  add_index "aplicacion_tamizajes", ["nivel_estudio_id"], name: "index_aplicacion_tamizajes_on_nivel_estudio_id"
  add_index "aplicacion_tamizajes", ["psychologist_id"], name: "index_aplicacion_tamizajes_on_psychologist_id"

  create_table "beneficiados", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "h6"
    t.integer  "psychologist_id"
  end

  add_index "beneficiados", ["psychologist_id"], name: "index_beneficiados_on_psychologist_id"

  create_table "capacitaciones_ots", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h6"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
  end

  add_index "capacitaciones_ots", ["psychologist_id"], name: "index_capacitaciones_ots_on_psychologist_id"

  create_table "capacitaciones_recs", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h6"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
  end

  add_index "capacitaciones_recs", ["psychologist_id"], name: "index_capacitaciones_recs_on_psychologist_id"

  create_table "civils", force: :cascade do |t|
    t.string   "civil"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "consultas_pvs", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h6"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
  end

  add_index "consultas_pvs", ["psychologist_id"], name: "index_consultas_pvs_on_psychologist_id"

  create_table "consultas_subs", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h6"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
  end

  add_index "consultas_subs", ["psychologist_id"], name: "index_consultas_subs_on_psychologist_id"

  create_table "estatuses", force: :cascade do |t|
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "etnia", force: :cascade do |t|
    t.string   "etnia"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "expedientes", force: :cascade do |t|
    t.integer  "id_expediente"
    t.string   "pago"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "patient_id"
    t.integer  "programa_id"
    t.integer  "sustancium_id"
    t.integer  "psychologist_id"
    t.integer  "sesion",          default: 0
    t.integer  "riesgo_id"
  end

  add_index "expedientes", ["patient_id"], name: "index_expedientes_on_patient_id"
  add_index "expedientes", ["programa_id"], name: "index_expedientes_on_programa_id"
  add_index "expedientes", ["psychologist_id"], name: "index_expedientes_on_psychologist_id"
  add_index "expedientes", ["riesgo_id"], name: "index_expedientes_on_riesgo_id"
  add_index "expedientes", ["sustancium_id"], name: "index_expedientes_on_sustancium_id"

  create_table "familiars", force: :cascade do |t|
    t.string   "nom"
    t.string   "app"
    t.string   "apm"
    t.integer  "edad"
    t.integer  "tel",           limit: 12
    t.string   "direccion"
    t.integer  "state_id"
    t.integer  "municipio_id"
    t.integer  "parentesco_id"
    t.integer  "patient_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "familiars", ["municipio_id"], name: "index_familiars_on_municipio_id"
  add_index "familiars", ["parentesco_id"], name: "index_familiars_on_parentesco_id"
  add_index "familiars", ["patient_id"], name: "index_familiars_on_patient_id"
  add_index "familiars", ["state_id"], name: "index_familiars_on_state_id"

  create_table "grupos_escuelas", force: :cascade do |t|
    t.text     "grupos"
    t.integer  "aplicacion_tamizaje_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "grupos_escuelas", ["aplicacion_tamizaje_id"], name: "index_grupos_escuelas_on_aplicacion_tamizaje_id"

  create_table "meetings", force: :cascade do |t|
    t.string   "name"
    t.datetime "start_time"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
    t.integer  "psychologist_id"
    t.integer  "patient_id"
  end

  add_index "meetings", ["patient_id"], name: "index_meetings_on_patient_id"
  add_index "meetings", ["psychologist_id"], name: "index_meetings_on_psychologist_id"
  add_index "meetings", ["user_id"], name: "index_meetings_on_user_id"

  create_table "meta", force: :cascade do |t|
    t.integer  "tamizajes"
    t.integer  "adol_acc_prev"
    t.integer  "cons_1a_vez"
    t.integer  "trat_brev_conc"
    t.integer  "adol_ini_trat"
    t.integer  "cons_subs"
    t.integer  "pers_cap_ces_tab"
    t.integer  "cap_otor_prev_trat"
    t.integer  "cap_rec_prev_trat"
    t.integer  "pers_det_viol_gen"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.date     "fecha"
    t.integer  "beneficiados",       default: 99
    t.integer  "multip_promot"
  end

  create_table "multyproms", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "h6"
    t.integer  "psychologist_id"
  end

  add_index "multyproms", ["psychologist_id"], name: "index_multyproms_on_psychologist_id"

  create_table "municipios", force: :cascade do |t|
    t.string   "municipio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "state_id"
  end

  add_index "municipios", ["state_id"], name: "index_municipios_on_state_id"

  create_table "nivel_estudios", force: :cascade do |t|
    t.string   "nivel"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "parentescos", force: :cascade do |t|
    t.string   "parentesco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "patient_counts", force: :cascade do |t|
    t.date     "fecha"
    t.string   "aasm_status"
    t.integer  "patient_id"
    t.integer  "psychologist_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "patient_counts", ["patient_id"], name: "index_patient_counts_on_patient_id"
  add_index "patient_counts", ["psychologist_id"], name: "index_patient_counts_on_psychologist_id"

  create_table "patients", force: :cascade do |t|
    t.string   "nom"
    t.string   "app"
    t.string   "apm"
    t.string   "sexo"
    t.string   "tel"
    t.integer  "edad"
    t.string   "calle"
    t.string   "col"
    t.string   "aasm_state"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "state_id"
    t.integer  "municipio_id"
    t.date     "fecha_nacimiento"
    t.integer  "civil_id"
    t.integer  "etnia_id"
    t.integer  "familiar_id"
  end

  add_index "patients", ["civil_id"], name: "index_patients_on_civil_id"
  add_index "patients", ["etnia_id"], name: "index_patients_on_etnia_id"
  add_index "patients", ["familiar_id"], name: "index_patients_on_familiar_id"
  add_index "patients", ["municipio_id"], name: "index_patients_on_municipio_id"
  add_index "patients", ["state_id"], name: "index_patients_on_state_id"

  create_table "personal_cts", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h6"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
  end

  add_index "personal_cts", ["psychologist_id"], name: "index_personal_cts_on_psychologist_id"

  create_table "personas_vgs", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h6"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
  end

  add_index "personas_vgs", ["psychologist_id"], name: "index_personas_vgs_on_psychologist_id"

  create_table "programas", force: :cascade do |t|
    t.string   "programa"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "sesiones"
  end

  create_table "psychologists", force: :cascade do |t|
    t.string   "nombre"
    t.string   "apellidoP"
    t.string   "apellidoM"
    t.integer  "telefono",   limit: 12
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "status"
    t.string   "color",                 default: "#fff"
    t.integer  "asignado"
  end

  create_table "riesgos", force: :cascade do |t|
    t.string   "riesgo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "clave"
  end

  create_table "states", force: :cascade do |t|
    t.string   "estados"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sustancia", force: :cascade do |t|
    t.string   "sustancia"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "clave"
  end

  create_table "tamizajes", force: :cascade do |t|
    t.string   "nombre"
    t.string   "apellidoP"
    t.string   "apellidoM"
    t.string   "edad"
    t.string   "sexo"
    t.string   "grado"
    t.string   "grupo"
    t.string   "folio_cuestionario"
    t.string   "p_usoS"
    t.integer  "total_uso_sustancias"
    t.string   "p_saludM"
    t.integer  "total_salud_mental"
    t.string   "p_relacionF"
    t.integer  "total_relacion_familiar"
    t.string   "p_relacionA"
    t.integer  "total_relacion_amigos"
    t.string   "p_nivelE"
    t.integer  "total_nivel_educativo"
    t.string   "p_interesL"
    t.integer  "total_interes_laboral"
    t.string   "p_conductaA"
    t.integer  "total_conducta_agresiva"
    t.integer  "resultado"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "aplicacion_tamizaje_id"
  end

  add_index "tamizajes", ["aplicacion_tamizaje_id"], name: "index_tamizajes_on_aplicacion_tamizaje_id"

  create_table "tamizajes_tots", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h6"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
  end

  add_index "tamizajes_tots", ["psychologist_id"], name: "index_tamizajes_tots_on_psychologist_id"

  create_table "tratamientos_bcs", force: :cascade do |t|
    t.integer  "h"
    t.integer  "m"
    t.date     "fecha"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "psychologist_id"
    t.integer  "h1"
    t.integer  "h2"
    t.integer  "h3"
    t.integer  "h4"
    t.integer  "h5"
    t.integer  "h6"
    t.integer  "h7"
    t.integer  "h8"
    t.integer  "h9"
    t.integer  "h10"
    t.integer  "m1"
    t.integer  "m2"
    t.integer  "m3"
    t.integer  "m4"
    t.integer  "m5"
    t.integer  "m6"
    t.integer  "m7"
    t.integer  "m8"
    t.integer  "m9"
    t.integer  "m10"
  end

  add_index "tratamientos_bcs", ["psychologist_id"], name: "index_tratamientos_bcs_on_psychologist_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "psychologist_id"
    t.integer  "permisson_level",        default: 1
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["psychologist_id"], name: "index_users_on_psychologist_id"
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
