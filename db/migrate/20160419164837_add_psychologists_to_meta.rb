class AddPsychologistsToMeta < ActiveRecord::Migration
  def change
  	add_reference :meta, :psychologist, index: true, foreign_key: true
  end
end
