class AddReferenceEdadToPersonalCap < ActiveRecord::Migration
  def change
  	add_reference :personal_cts, :edades, index: true, foreign_key: true
  end
end
