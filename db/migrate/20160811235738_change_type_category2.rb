class ChangeTypeCategory2 < ActiveRecord::Migration
   def up
  	change_table :multyproms do |t|
      t.change :h, :integer
      t.change :m, :integer
      t.change :h1, :integer
      t.change :h2, :integer
      t.change :h3, :integer
      t.change :h4, :integer
      t.change :h5, :integer
      t.change :h7, :integer
      t.change :h8, :integer
      t.change :h9, :integer
      t.change :h10, :integer
      t.change :m1, :integer
      t.change :m2, :integer
      t.change :m3, :integer
      t.change :m4, :integer
      t.change :m5, :integer
      t.change :m6, :integer
      t.change :m7, :integer
      t.change :m8, :integer
      t.change :m9, :integer
      t.change :m10, :integer
    end
 	end
  def down
    change_table :multyproms do |t|
      t.change :h, :string
      t.change :m, :string
      t.change :h1, :string
      t.change :h2, :string
      t.change :h3, :string
      t.change :h4, :string
      t.change :h5, :string
      t.change :h7, :string
      t.change :h8, :string
      t.change :h9, :string
      t.change :h10, :string
      t.change :m1, :string
      t.change :m2, :string
      t.change :m3, :string
      t.change :m4, :string
      t.change :m5, :string
      t.change :m6, :string
      t.change :m7, :string
      t.change :m8, :string
      t.change :m9, :string
      t.change :m10, :string
    end
  end
end
