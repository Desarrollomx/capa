class CreateCapacitacionesRecs < ActiveRecord::Migration
  def change
    create_table :capacitaciones_recs do |t|
      t.integer :h
      t.integer :m
      t.date :fecha

      t.timestamps null: false
    end
  end
end
