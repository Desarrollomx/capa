class AddColumnToConsultasPv < ActiveRecord::Migration
  def change
  	add_reference :consultas_pvs, :psychologist, index: true, foreign_key: true  
  end
end
