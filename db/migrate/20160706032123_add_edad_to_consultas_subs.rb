class AddEdadToConsultasSubs < ActiveRecord::Migration
  def change
    add_column :consultas_subs, :h1, :integer
    add_column :consultas_subs, :h2, :integer
    add_column :consultas_subs, :h3, :integer
    add_column :consultas_subs, :h4, :integer
    add_column :consultas_subs, :h5, :integer
    add_column :consultas_subs, :h6, :integer
    add_column :consultas_subs, :h7, :integer
    add_column :consultas_subs, :h8, :integer
    add_column :consultas_subs, :h9, :integer
    add_column :consultas_subs, :h10, :integer
    add_column :consultas_subs, :m1, :integer
    add_column :consultas_subs, :m2, :integer
    add_column :consultas_subs, :m3, :integer
    add_column :consultas_subs, :m4, :integer
    add_column :consultas_subs, :m5, :integer
    add_column :consultas_subs, :m6, :integer
    add_column :consultas_subs, :m7, :integer
    add_column :consultas_subs, :m8, :integer
    add_column :consultas_subs, :m9, :integer
    add_column :consultas_subs, :m10, :integer
  end
end
