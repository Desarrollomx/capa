class ChangeTelFromFamiliar < ActiveRecord::Migration
  def down
    change_table :familiars do |t|
      t.change :tel, :integer
    end
  end
  def up
    change_table :familiars do |t|
      t.change :tel, :string
    end
  end
 end
