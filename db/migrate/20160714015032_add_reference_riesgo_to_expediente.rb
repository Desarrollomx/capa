class AddReferenceRiesgoToExpediente < ActiveRecord::Migration
  def change
	add_reference :expedientes, :riesgo, index: true, foreign_key: true  
  end
end
