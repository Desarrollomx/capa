class ChangesustanciaFromSustancium < ActiveRecord::Migration
  def change
    rename_column :expedientes, :sustancia_id, :sustancium_id    
  end
end
