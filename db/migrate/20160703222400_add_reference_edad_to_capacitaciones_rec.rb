class AddReferenceEdadToCapacitacionesRec < ActiveRecord::Migration
  def change
  	add_reference :capacitaciones_recs, :edades, index: true, foreign_key: true
  end
end
