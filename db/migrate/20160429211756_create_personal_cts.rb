class CreatePersonalCts < ActiveRecord::Migration
  def change
    create_table :personal_cts do |t|
      t.integer :h
      t.integer :m
      t.date :fecha

      t.timestamps null: false
    end
  end
end
