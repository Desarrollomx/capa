class AddColumnMunicipioIdToPatients < ActiveRecord::Migration
  def change
  	add_reference :patients, :municipio, index: true, foreign_key: true
  end
end
