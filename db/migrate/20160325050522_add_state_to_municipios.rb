class AddStateToMunicipios < ActiveRecord::Migration
  def change
  	add_reference :municipios, :state, index: true, foreign_key: true
  end
end
