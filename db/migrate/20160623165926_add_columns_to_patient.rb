class AddColumnsToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :fecha_nacimiento, :date
    add_column :patients, :familiar, :integer
    add_column :patients, :estado_civil, :integer
    add_column :patients, :etnia, :integer
  end
end
