class AddColumnToAdolescentesAp < ActiveRecord::Migration
  def change
  	add_reference :adolescentes_aps, :psychologist, index: true, foreign_key: true  
  end
end
