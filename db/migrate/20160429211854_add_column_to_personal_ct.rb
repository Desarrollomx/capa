class AddColumnToPersonalCt < ActiveRecord::Migration
  def change
  	add_reference :personal_cts, :psychologist, index: true, foreign_key: true 
  end
end
