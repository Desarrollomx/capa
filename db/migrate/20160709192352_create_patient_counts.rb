class CreatePatientCounts < ActiveRecord::Migration
  def change
    create_table :patient_counts do |t|
      t.date :fecha
      t.string :aasm_status
      t.references :patient, index: true, foreign_key: true
      t.references :psychologist, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
