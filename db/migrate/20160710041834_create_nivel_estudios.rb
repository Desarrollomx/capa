class CreateNivelEstudios < ActiveRecord::Migration
  def change
    create_table :nivel_estudios do |t|
      t.string :nivel

      t.timestamps null: false
    end
  end
end
