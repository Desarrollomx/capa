class AddPatientToExpediente < ActiveRecord::Migration
  def change
  	add_reference :expedientes, :patient, index: true, foreign_key: true
  	add_reference :expedientes, :user, index: true, foreign_key: true
  	add_reference :expedientes, :programa, index: true, foreign_key: true
  	add_reference :expedientes, :sustancia, index: true, foreign_key: true
  end
end
