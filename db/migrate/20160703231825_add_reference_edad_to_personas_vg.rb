class AddReferenceEdadToPersonasVg < ActiveRecord::Migration
  def change
  	add_reference :personas_vgs, :edades, index: true, foreign_key: true
  end
end
