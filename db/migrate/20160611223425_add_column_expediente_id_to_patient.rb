class AddColumnExpedienteIdToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :expediente_id, :string,  index: true, foreign_key: true
  end
end
