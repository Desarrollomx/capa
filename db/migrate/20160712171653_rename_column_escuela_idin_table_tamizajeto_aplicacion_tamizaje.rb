class RenameColumnEscuelaIdinTableTamizajetoAplicacionTamizaje < ActiveRecord::Migration
  def change
      rename_column :tamizajes, :escuela_id, :aplicacion_tamizaje_id      
  end
end
