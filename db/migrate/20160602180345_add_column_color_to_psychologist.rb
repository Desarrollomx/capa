class AddColumnColorToPsychologist < ActiveRecord::Migration
  def change
    add_column :psychologists, :color, :string, default: "#fff"
  end
end
