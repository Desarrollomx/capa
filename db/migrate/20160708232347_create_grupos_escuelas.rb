class CreateGruposEscuelas < ActiveRecord::Migration
  def change
    create_table :grupos_escuelas do |t|
      t.text :grupos
      t.references :aplicacion_tamizaje, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
