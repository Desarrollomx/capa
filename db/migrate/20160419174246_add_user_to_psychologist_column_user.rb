class AddUserToPsychologistColumnUser < ActiveRecord::Migration
  def change
    add_reference :users, :psychologist, index: true, foreign_key: true
  end
end
