class CreateTamizajesTots < ActiveRecord::Migration
  def change
    create_table :tamizajes_tots do |t|
      t.integer :h
      t.integer :m
      t.date :fecha

      t.timestamps null: false
    end
  end
end
