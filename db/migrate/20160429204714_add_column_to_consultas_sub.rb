class AddColumnToConsultasSub < ActiveRecord::Migration
  def change
  	add_reference :consultas_subs, :psychologist, index: true, foreign_key: true  
  end
end
