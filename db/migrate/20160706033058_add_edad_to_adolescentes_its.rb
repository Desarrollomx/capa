class AddEdadToAdolescentesIts < ActiveRecord::Migration
  def change
    add_column :adolescentes_its, :h3, :integer
    add_column :adolescentes_its, :h4, :integer
    add_column :adolescentes_its, :m3, :integer
    add_column :adolescentes_its, :m4, :integer
  end
end
