class AddUserToMeetingsColumnPsychologist < ActiveRecord::Migration
  def change
    add_reference :meetings, :psychologist, index: true, foreign_key: true
    add_reference :meetings, :patient, index: true, foreign_key: true
  end
end
