class AddColumnToCapacitacionesOt < ActiveRecord::Migration
  def change
  	add_reference :capacitaciones_ots, :psychologist, index: true, foreign_key: true  
  end
end
