class CreateCivils < ActiveRecord::Migration
  def change
    create_table :civils do |t|
      t.string :civil

      t.timestamps null: false
    end
  end
end
