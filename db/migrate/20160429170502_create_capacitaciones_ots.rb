class CreateCapacitacionesOts < ActiveRecord::Migration
  def change
    create_table :capacitaciones_ots do |t|
      t.integer :h
      t.integer :m
      t.date :fecha

      t.timestamps null: false
    end
  end
end
