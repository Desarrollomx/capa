class RemoveColumnsToAplicacionTamizaje < ActiveRecord::Migration
  def change
    remove_column :aplicacion_tamizajes, :municipio, :string
    remove_column :aplicacion_tamizajes, :nivel_escolar, :string
    remove_column :aplicacion_tamizajes, :psic_id, :integer
  end
end
