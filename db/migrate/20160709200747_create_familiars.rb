class CreateFamiliars < ActiveRecord::Migration
  def change
    create_table :familiars do |t|
      t.string :nom
      t.string :app
      t.string :apm
      t.integer :edad
      t.integer :tel
      t.string :direccion
      t.references :state, index: true, foreign_key: true
      t.references :municipio, index: true, foreign_key: true
      t.references :parentesco, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
