class AddEdadToAdolescentesAp < ActiveRecord::Migration
  def change
    add_column :adolescentes_aps, :h3, :integer
    add_column :adolescentes_aps, :h4, :integer
    add_column :adolescentes_aps, :m3, :integer
    add_column :adolescentes_aps, :m4, :integer
  end
end
