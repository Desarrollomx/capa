class AddReferencesToTamizaje < ActiveRecord::Migration
  def change
      add_reference :tamizajes, :escuela, index: true, foreign_key: true
  end
end
