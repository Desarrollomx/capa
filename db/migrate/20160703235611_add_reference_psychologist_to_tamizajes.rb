class AddReferencePsychologistToTamizajes < ActiveRecord::Migration
  def change
    add_reference :tamizajes, :psychologist, index: true, foreign_key: true
  end
end
