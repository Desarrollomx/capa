class AddColumnToTamizajesTot < ActiveRecord::Migration
  def change
  	add_reference :tamizajes_tots, :psychologist, index: true, foreign_key: true 
  end
end
