class RemoveColumnExpienteIdFromPatient < ActiveRecord::Migration
  def change
    remove_column :patients, :expediente_id
  end
end
