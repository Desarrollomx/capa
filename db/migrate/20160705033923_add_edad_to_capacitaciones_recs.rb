class AddEdadToCapacitacionesRecs < ActiveRecord::Migration
  def change
    add_column :capacitaciones_recs, :h1, :integer
    add_column :capacitaciones_recs, :h2, :integer
    add_column :capacitaciones_recs, :h3, :integer
    add_column :capacitaciones_recs, :h4, :integer
    add_column :capacitaciones_recs, :h5, :integer
    add_column :capacitaciones_recs, :h6, :integer
    add_column :capacitaciones_recs, :h7, :integer
    add_column :capacitaciones_recs, :h8, :integer
    add_column :capacitaciones_recs, :h9, :integer
    add_column :capacitaciones_recs, :h10, :integer
    add_column :capacitaciones_recs, :m1, :integer
    add_column :capacitaciones_recs, :m2, :integer
    add_column :capacitaciones_recs, :m3, :integer
    add_column :capacitaciones_recs, :m4, :integer
    add_column :capacitaciones_recs, :m5, :integer
    add_column :capacitaciones_recs, :m6, :integer
    add_column :capacitaciones_recs, :m7, :integer
    add_column :capacitaciones_recs, :m8, :integer
    add_column :capacitaciones_recs, :m9, :integer
    add_column :capacitaciones_recs, :m10, :integer
  end
end
