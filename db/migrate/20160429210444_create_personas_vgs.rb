class CreatePersonasVgs < ActiveRecord::Migration
  def change
    create_table :personas_vgs do |t|
      t.integer :h
      t.integer :m
      t.date :fecha

      t.timestamps null: false
    end
  end
end
