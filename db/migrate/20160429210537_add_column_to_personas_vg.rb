class AddColumnToPersonasVg < ActiveRecord::Migration
  def change
  	add_reference :personas_vgs, :psychologist, index: true, foreign_key: true 
  end
end
