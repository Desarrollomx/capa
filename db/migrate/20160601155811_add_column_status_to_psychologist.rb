class AddColumnStatusToPsychologist < ActiveRecord::Migration
  def change
    add_column :psychologists, :status, :string, deafult: "inactivo"
  end
end
