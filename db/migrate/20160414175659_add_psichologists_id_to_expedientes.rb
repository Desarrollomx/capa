class AddPsichologistsIdToExpedientes < ActiveRecord::Migration
  def change
  	add_reference :expedientes, :psychologist, index: true, foreign_key: true
  end
end
