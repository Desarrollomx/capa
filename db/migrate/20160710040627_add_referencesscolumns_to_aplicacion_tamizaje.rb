class AddReferencesscolumnsToAplicacionTamizaje < ActiveRecord::Migration
  def change
    add_reference :aplicacion_tamizajes, :municipio, index: true, foreign_key: true
    add_reference :aplicacion_tamizajes, :nivel_estudio, index: true, foreign_key: true
    add_reference :aplicacion_tamizajes, :psychologist, index: true, foreign_key: true
  end
end
