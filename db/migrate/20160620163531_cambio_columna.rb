class CambioColumna < ActiveRecord::Migration
  def self.up
     rename_column :patients, :status, :aasm_state
   end

   def self.down

   end
end
