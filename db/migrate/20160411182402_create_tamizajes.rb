class CreateTamizajes < ActiveRecord::Migration
  def change
    create_table :tamizajes do |t|
      t.string :nombre
      t.string :apellidoP
      t.string :apellidoM
      t.string :edad
      t.string :sexo
      t.string :nombre_escuela
      t.string :nivel_escolar
      t.string :grado
      t.string :grupo
      t.string :folio_cuestionario
      t.string :colonia_escuela
      t.string :municipio_escuela
      t.string :p_usoS
      t.integer :total_uso_sustancias
      t.string :p_saludM
      t.integer :total_salud_mental
      t.string :p_relacionF
      t.integer :total_relacion_familiar
      t.string :p_relacionA
      t.integer :total_relacion_amigos
      t.string :p_nivelE
      t.integer :total_nivel_educativo
      t.string :p_interesL
      t.integer :total_interes_laboral
      t.string :p_conductaA
      t.integer :total_conducta_agresiva
      t.integer :resultado

      t.timestamps null: false
    end
  end
end
