class CreateAplicacionTamizajes < ActiveRecord::Migration
  def change
    create_table :aplicacion_tamizajes do |t|
      t.text :escuela
      t.string :colonia
      t.string :municipio
      t.string :nivel_escolar
      t.string :id_psic
      t.string :status

      t.timestamps null: false
    end
  end
end
