class AddColumnStateIdToPatients < ActiveRecord::Migration
  def change
  	add_reference :patients, :state, index: true, foreign_key: true
  end
end
