class CreateExpedientes < ActiveRecord::Migration
  def change
    create_table :expedientes do |t|
      t.integer :id_expediente
      t.string :pago
      t.string :observaciones
      t.string :seguimiento

      t.timestamps null: false
    end
  end
end
