class RemoveColumnEtniaFamiliarEsatoCivilFromPatient < ActiveRecord::Migration
  def change
    remove_column :patients, :etnia
    remove_column :patients, :familiar
    remove_column :patients, :estado_civil
  end
end
