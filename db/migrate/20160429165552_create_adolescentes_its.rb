class CreateAdolescentesIts < ActiveRecord::Migration
  def change
    create_table :adolescentes_its do |t|
      t.integer :h
      t.integer :m
      t.date :fecha

      t.timestamps null: false
    end
  end
end
