class RemoveUserIdFromExpedientes < ActiveRecord::Migration
  def change
    remove_column :expedientes, :user_id, :integer
  end
end
