class CreateConsultasPvs < ActiveRecord::Migration
  def change
    create_table :consultas_pvs do |t|
      t.integer :h
      t.integer :m
      t.date :fecha

      t.timestamps null: false
    end
  end
end
