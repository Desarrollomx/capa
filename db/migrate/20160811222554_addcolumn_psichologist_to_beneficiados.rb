class AddcolumnPsichologistToBeneficiados < ActiveRecord::Migration
  def change
  	  	add_reference :beneficiados, :psychologist, index: true, foreign_key: true
  end
end
