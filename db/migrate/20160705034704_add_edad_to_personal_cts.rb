class AddEdadToPersonalCts < ActiveRecord::Migration
  def change
    add_column :personal_cts, :h1, :integer
    add_column :personal_cts, :h2, :integer
    add_column :personal_cts, :h3, :integer
    add_column :personal_cts, :h4, :integer
    add_column :personal_cts, :h5, :integer
    add_column :personal_cts, :h6, :integer
    add_column :personal_cts, :h7, :integer
    add_column :personal_cts, :h8, :integer
    add_column :personal_cts, :h9, :integer
    add_column :personal_cts, :h10, :integer
    add_column :personal_cts, :m1, :integer
    add_column :personal_cts, :m2, :integer
    add_column :personal_cts, :m3, :integer
    add_column :personal_cts, :m4, :integer
    add_column :personal_cts, :m5, :integer
    add_column :personal_cts, :m6, :integer
    add_column :personal_cts, :m7, :integer
    add_column :personal_cts, :m8, :integer
    add_column :personal_cts, :m9, :integer
    add_column :personal_cts, :m10, :integer
  end
end
