class CreateProgramas < ActiveRecord::Migration
  def change
    create_table :programas do |t|
      t.string :programa

      t.timestamps null: false
    end
  end
end
