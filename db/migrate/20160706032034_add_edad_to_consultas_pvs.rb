class AddEdadToConsultasPvs < ActiveRecord::Migration
  def change
    add_column :consultas_pvs, :h1, :integer
    add_column :consultas_pvs, :h2, :integer
    add_column :consultas_pvs, :h3, :integer
    add_column :consultas_pvs, :h4, :integer
    add_column :consultas_pvs, :h5, :integer
    add_column :consultas_pvs, :h6, :integer
    add_column :consultas_pvs, :h7, :integer
    add_column :consultas_pvs, :h8, :integer
    add_column :consultas_pvs, :h9, :integer
    add_column :consultas_pvs, :h10, :integer
    add_column :consultas_pvs, :m1, :integer
    add_column :consultas_pvs, :m2, :integer
    add_column :consultas_pvs, :m3, :integer
    add_column :consultas_pvs, :m4, :integer
    add_column :consultas_pvs, :m5, :integer
    add_column :consultas_pvs, :m6, :integer
    add_column :consultas_pvs, :m7, :integer
    add_column :consultas_pvs, :m8, :integer
    add_column :consultas_pvs, :m9, :integer
    add_column :consultas_pvs, :m10, :integer
  end
end
