class CreateMeta < ActiveRecord::Migration
  def change
    create_table :meta do |t|
      t.integer :tamizajes
      t.integer :adol_acc_prev
      t.integer :cons_1a_vez
      t.integer :trat_brev_conc
      t.integer :adol_ini_trat
      t.integer :cons_subs
      t.integer :pers_cap_ces_tab
      t.integer :cap_otor_prev_trat
      t.integer :cap_rec_prev_trat
      t.integer :pers_det_viol_gen

      t.timestamps null: false
    end
  end
end
