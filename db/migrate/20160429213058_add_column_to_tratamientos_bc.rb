class AddColumnToTratamientosBc < ActiveRecord::Migration
  def change
  	add_reference :tratamientos_bcs, :psychologist, index: true, foreign_key: true 
  end
end
