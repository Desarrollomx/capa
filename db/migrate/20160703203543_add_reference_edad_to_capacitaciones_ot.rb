class AddReferenceEdadToCapacitacionesOt < ActiveRecord::Migration
  def change
  	add_reference :capacitaciones_ots, :edades, index: true, foreign_key: true
  end
end
