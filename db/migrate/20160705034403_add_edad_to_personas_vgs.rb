class AddEdadToPersonasVgs < ActiveRecord::Migration
  def change
    add_column :personas_vgs, :h1, :integer
    add_column :personas_vgs, :h2, :integer
    add_column :personas_vgs, :h3, :integer
    add_column :personas_vgs, :h4, :integer
    add_column :personas_vgs, :h5, :integer
    add_column :personas_vgs, :h6, :integer
    add_column :personas_vgs, :h7, :integer
    add_column :personas_vgs, :h8, :integer
    add_column :personas_vgs, :h9, :integer
    add_column :personas_vgs, :h10, :integer
    add_column :personas_vgs, :m1, :integer
    add_column :personas_vgs, :m2, :integer
    add_column :personas_vgs, :m3, :integer
    add_column :personas_vgs, :m4, :integer
    add_column :personas_vgs, :m5, :integer
    add_column :personas_vgs, :m6, :integer
    add_column :personas_vgs, :m7, :integer
    add_column :personas_vgs, :m8, :integer
    add_column :personas_vgs, :m9, :integer
    add_column :personas_vgs, :m10, :integer
  end
end
