class AddEdadToTamizajesTots < ActiveRecord::Migration
  def change
    add_column :tamizajes_tots, :h3, :integer
    add_column :tamizajes_tots, :h4, :integer
    add_column :tamizajes_tots, :h5, :integer
    add_column :tamizajes_tots, :h6, :integer
    add_column :tamizajes_tots, :m3, :integer
    add_column :tamizajes_tots, :m4, :integer
    add_column :tamizajes_tots, :m5, :integer
    add_column :tamizajes_tots, :m6, :integer
  end
end
