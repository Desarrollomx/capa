class CreateTratamientosBcs < ActiveRecord::Migration
  def change
    create_table :tratamientos_bcs do |t|
      t.integer :h
      t.integer :m
      t.date :fecha

      t.timestamps null: false
    end
  end
end
