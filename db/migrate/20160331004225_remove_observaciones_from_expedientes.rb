class RemoveObservacionesFromExpedientes < ActiveRecord::Migration
  def change
    remove_column :expedientes, :observaciones, :string
    remove_column :expedientes, :seguimiento, :string
  end
end
