class RemoveH1FromTamizajesTot < ActiveRecord::Migration
  def change
    remove_column :tamizajes_tots, :h1, :integer
    remove_column :tamizajes_tots, :h2, :integer
    remove_column :tamizajes_tots, :h7, :integer
    remove_column :tamizajes_tots, :h8, :integer
    remove_column :tamizajes_tots, :h9, :integer
    remove_column :tamizajes_tots, :h10, :integer
    remove_column :tamizajes_tots, :m1, :integer
    remove_column :tamizajes_tots, :m2, :integer
    remove_column :tamizajes_tots, :m7, :integer
    remove_column :tamizajes_tots, :m8, :integer
    remove_column :tamizajes_tots, :m9, :integer
    remove_column :tamizajes_tots, :m10, :integer
  end
end
