class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :nom
      t.string :app
      t.string :apm
      t.string :sexo
      t.string :tel
      t.integer :edad
      t.string :calle
      t.string :col
      t.string :status

      t.timestamps null: false
    end
  end
end
