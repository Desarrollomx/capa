class CreatePsychologists < ActiveRecord::Migration
  def change
    create_table :psychologists do |t|
      t.string :nombre
      t.string :apellidoP
      t.string :apellidoM
      t.integer :telefono

      t.timestamps null: false
    end
  end
end
