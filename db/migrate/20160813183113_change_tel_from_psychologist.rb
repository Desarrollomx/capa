class ChangeTelFromPsychologist < ActiveRecord::Migration
  def change
    change_table :psychologists do |t|
      t.change :telefono, :string
    end
  end
end
