class AddColumnBeneficiariosToMetas < ActiveRecord::Migration
  def change
    add_column :meta, :beneficiados, :integer, default: 99
  end
end
