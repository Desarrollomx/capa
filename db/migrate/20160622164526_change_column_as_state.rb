class ChangeColumnAsState < ActiveRecord::Migration
  def change
    def up
      change_table :patients do |t|
        t.change :aasm_state, :string, default: "primera"
      end
    end

    def down
      change_table :patients do |t|
        t.change :aasm_state, :string
      end
    end
  end
end
