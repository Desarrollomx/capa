class CreateMultyproms < ActiveRecord::Migration
  def change
    create_table :multyproms do |t|
      t.string :h
      t.string :m
      t.date :fecha
      t.string :h1
      t.string :h2
      t.string :h3
      t.string :h4
      t.string :h5
      t.string :h7
      t.string :h8
      t.string :h9
      t.string :h10
      t.string :m1
      t.string :m2
      t.string :m3
      t.string :m4
      t.string :m5
      t.string :m6
      t.string :m7
      t.string :m8
      t.string :m9
      t.string :m10

      t.timestamps null: false
    end
  end
end
