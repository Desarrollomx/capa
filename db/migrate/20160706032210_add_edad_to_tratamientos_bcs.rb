class AddEdadToTratamientosBcs < ActiveRecord::Migration
  def change
    add_column :tratamientos_bcs, :h1, :integer
    add_column :tratamientos_bcs, :h2, :integer
    add_column :tratamientos_bcs, :h3, :integer
    add_column :tratamientos_bcs, :h4, :integer
    add_column :tratamientos_bcs, :h5, :integer
    add_column :tratamientos_bcs, :h6, :integer
    add_column :tratamientos_bcs, :h7, :integer
    add_column :tratamientos_bcs, :h8, :integer
    add_column :tratamientos_bcs, :h9, :integer
    add_column :tratamientos_bcs, :h10, :integer
    add_column :tratamientos_bcs, :m1, :integer
    add_column :tratamientos_bcs, :m2, :integer
    add_column :tratamientos_bcs, :m3, :integer
    add_column :tratamientos_bcs, :m4, :integer
    add_column :tratamientos_bcs, :m5, :integer
    add_column :tratamientos_bcs, :m6, :integer
    add_column :tratamientos_bcs, :m7, :integer
    add_column :tratamientos_bcs, :m8, :integer
    add_column :tratamientos_bcs, :m9, :integer
    add_column :tratamientos_bcs, :m10, :integer
  end
end
