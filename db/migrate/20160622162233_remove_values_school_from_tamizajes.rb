class RemoveValuesSchoolFromTamizajes < ActiveRecord::Migration
  def change
    remove_column :tamizajes, :nombre_escuela, :string
    remove_column :tamizajes, :nivel_escolar, :string
    remove_column :tamizajes, :colonia_escuela, :string
    remove_column :tamizajes, :municipio_escuela, :string
  end
end
