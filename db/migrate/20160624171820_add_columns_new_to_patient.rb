class AddColumnsNewToPatient < ActiveRecord::Migration
  def change
    add_reference :patients, :estado_civil, index: true, foreign_key: true
    add_reference :patients, :etnia, index: true, foreign_key: true
    add_reference :patients, :familiar, index: true, foreign_key: true
  end
end
