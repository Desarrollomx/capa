class AddReferencesToAplicacionTamizaje < ActiveRecord::Migration
  def change
    add_reference :aplicacion_tamizajes, :psic, index: true, foreign_key: true
  end
end
