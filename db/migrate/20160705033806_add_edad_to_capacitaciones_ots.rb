class AddEdadToCapacitacionesOts < ActiveRecord::Migration
  def change
    add_column :capacitaciones_ots, :h1, :integer
    add_column :capacitaciones_ots, :h2, :integer
    add_column :capacitaciones_ots, :h3, :integer
    add_column :capacitaciones_ots, :h4, :integer
    add_column :capacitaciones_ots, :h5, :integer
    add_column :capacitaciones_ots, :h6, :integer
    add_column :capacitaciones_ots, :h7, :integer
    add_column :capacitaciones_ots, :h8, :integer
    add_column :capacitaciones_ots, :h9, :integer
    add_column :capacitaciones_ots, :h10, :integer
    add_column :capacitaciones_ots, :m1, :integer
    add_column :capacitaciones_ots, :m2, :integer
    add_column :capacitaciones_ots, :m3, :integer
    add_column :capacitaciones_ots, :m4, :integer
    add_column :capacitaciones_ots, :m5, :integer
    add_column :capacitaciones_ots, :m6, :integer
    add_column :capacitaciones_ots, :m7, :integer
    add_column :capacitaciones_ots, :m8, :integer
    add_column :capacitaciones_ots, :m9, :integer
    add_column :capacitaciones_ots, :m10, :integer
  end
end
