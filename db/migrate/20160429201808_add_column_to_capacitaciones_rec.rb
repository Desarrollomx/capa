class AddColumnToCapacitacionesRec < ActiveRecord::Migration
  def change
  	add_reference :capacitaciones_recs, :psychologist, index: true, foreign_key: true  
  end
end
