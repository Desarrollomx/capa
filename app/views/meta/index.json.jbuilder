json.array!(@meta) do |metum|
  json.extract! metum, :id, :tamizajes, :adol_acc_prev, :cons_1a_vez, :trat_brev_conc, :adol_ini_trat, :cons_subs, :pers_cap_ces_tab, :cap_otor_prev_trat, :cap_rec_prev_trat, :pers_det_viol_gen, :beneficiados, :multip_promot 
  json.url metum_url(metum, format: :json)
end
