json.array!(@estatuses) do |estatus|
  json.extract! estatus, :id, :status
  json.url estatus_url(estatus, format: :json)
end
