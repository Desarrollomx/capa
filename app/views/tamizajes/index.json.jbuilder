json.array!(@tamizajes) do |tamizaje|
  json.extract! tamizaje, :id, :nombre, :apellidoP, :apellidoM, :edad, :sexo, :nombre_escuela, :nivel_escolar, :grado, :grupo, :folio_cuestionario, :colonia_escuela, :municipio_escuela, :p_usoS, :total_uso_sustancias, :p_saludM, :total_salud_mental, :p_relacionF, :total_relacion_familiar, :p_relacionA, :total_relacion_amigos, :p_nivelE, :total_nivel_educativo, :p_interesL, :total_interes_laboral, :p_conductaA, :total_conducta_agresiva, :resultado
  json.url tamizaje_url(tamizaje, format: :json)
end
