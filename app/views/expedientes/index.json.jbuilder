json.array!(@expedientes) do |expediente|
  json.extract! expediente, :id, :id_expediente, :pago, :observaciones, :seguimiento
  json.url expediente_url(expediente, format: :json)
end
