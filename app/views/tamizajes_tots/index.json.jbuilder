json.array!(@tamizajes_tots) do |tamizajes_tot|
  json.extract! tamizajes_tot, :id, :h, :m, :fecha, :h3, :h4, :h5, :h6, :m3, :m4, :m5, :m6
  json.url tamizajes_tot_url(tamizajes_tot, format: :json)
end
