json.array!(@indicadores) do |indicador|
  json.extract! indicador, :id, :indicador
  json.url indicador_url(indicador, format: :json)
end
