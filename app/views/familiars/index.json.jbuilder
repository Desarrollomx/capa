json.array!(@familiars) do |familiar|
  json.extract! familiar, :id, :nom, :app, :apm, :edad, :tel, :direccion, :state_id, :municipio_id, :parentesco_id, :patient_id
  json.url familiar_url(familiar, format: :json)
end
