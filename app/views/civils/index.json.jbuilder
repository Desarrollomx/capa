json.array!(@civils) do |civil|
  json.extract! civil, :id, :civil
  json.url civil_url(civil, format: :json)
end
