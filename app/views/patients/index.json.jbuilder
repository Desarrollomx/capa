json.array!(@patients) do |patient|
  json.extract! patient, :id, :nom, :app, :apm, :sexo, :tel, :edad, :calle, :col, :status
  json.url patient_url(patient, format: :json)
end
