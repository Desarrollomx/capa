json.array!(@psychologists) do |psychologist|
  json.extract! psychologist, :id, :nombre, :apellidoP, :apellidoM, :telefono
  json.url psychologist_url(psychologist, format: :json)
end
