json.array!(@programas) do |programa|
  json.extract! programa, :id, :programa
  json.url programa_url(programa, format: :json)
end