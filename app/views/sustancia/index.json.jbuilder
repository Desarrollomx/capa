json.array!(@sustancia) do |sustancium|
  json.extract! sustancium, :id, :sustancia
  json.url sustancium_url(sustancium, format: :json)
end
