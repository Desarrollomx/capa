json.array!(@riesgos) do |riesgo|
  json.extract! riesgo, :id, :riesgo
  json.url riesgo_url(riesgo, format: :json)
end
