json.array!(@parentescos) do |parentesco|
  json.extract! parentesco, :id, :parentesco
  json.url parentesco_url(parentesco, format: :json)
end
