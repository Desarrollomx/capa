json.array!(@multyproms) do |multyprom|
  json.extract! multyprom, :id, :h, :m, :fecha, :h1, :h2, :h3, :h4, :h5, :h6, :h7, :h8, :h9, :h10, :m1, :m2, :m3, :m4, :m5, :m6, :m7, :m8, :m9, :m10, :psychologist_id
  json.url multyprom_url(multyprom, format: :json)
end
