json.array!(@adolescentes_aps) do |adolescentes_ap|
  json.extract! adolescentes_ap, :id, :h, :m, :fecha, :h3, :h4, :m3, :m4
  json.url adolescentes_ap_url(adolescentes_ap, format: :json)
end
