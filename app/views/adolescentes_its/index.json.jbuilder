json.array!(@adolescentes_its) do |adolescentes_it|
  json.extract! adolescentes_it, :id, :h, :m, :fecha, :h3, :h4, :m3, :m4
  json.url adolescentes_it_url(adolescentes_it, format: :json)
end
