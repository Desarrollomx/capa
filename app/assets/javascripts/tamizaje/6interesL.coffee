$(document).ready ->
    interesL = [];
    
    ordenar = (arr) ->
        i=0; j=i+1;
        for i in [0..arr.length-1]
            for j in [i..arr.length-1]
                if arr[i] > arr[j]
                    aux = arr[i]
                    arr[i] = arr[j]
                    arr[j] = aux
                    
    total = (arr) ->
        if( arr.length >= 3)
            $("#tamizaje_total_interes_laboral").css("background", "#FF3C3C");
            $("#tamizaje_total_interes_laboral").attr("value",interesL.length);
        else
            if (arr.length < 3)
                $("#tamizaje_total_interes_laboral").css("background", "#FFFFFF");
                $("#tamizaje_total_interes_laboral").attr("value",interesL.length);
        
        valUs = $("#tamizaje_total_uso_sustancias").val();
        valSm = $("#tamizaje_total_salud_mental").val();
        valRf = $("#tamizaje_total_relacion_familiar").val();
        valRa = $("#tamizaje_total_relacion_amigos").val();
        valNe = $("#tamizaje_total_nivel_educativo").val();
        valIl = $("#tamizaje_total_interes_laboral").val();
        valCa = $("#tamizaje_total_conducta_agresiva").val();
        
        valResultado = parseInt(valUs) + parseInt(valSm) + parseInt(valRf) + parseInt(valRa) + parseInt(valNe) + parseInt(valIl) + parseInt(valCa);
        $("#tamizaje_resultado").attr("value",valResultado);
        
        
    
    #coffee-stir <tamizajes.coffee>
    $("#tamizaje_total_interes_laboral").attr("value","0");
    #$("#tamizaje_total_uso_sustancias").attr("disabled",true);

    $("input[name=ch27]").click ->
        if( $(this).is(':checked'))
            interesL.push("27"); ordenar(interesL); total(interesL);
            $("[name=lch27]").text("Si");
            $("#tamizaje_p_interesL").attr("value",interesL);
        if( !$(this).is(':checked'))
            interesL.splice(interesL.indexOf("27"),1);
            $("#tamizaje_p_interesL").attr("value",interesL);
            $("[name=lch27]").text("No"); total(interesL);
            
    $("input[name=ch36]").click ->
        if( $(this).is(':checked'))
            interesL.push("36"); ordenar(interesL); total(interesL);
            $("[name=lch36]").text("Si");
            $("#tamizaje_p_interesL").attr("value",interesL);
        if( !$(this).is(':checked'))
            interesL.splice(interesL.indexOf("36"),1);
            $("#tamizaje_p_interesL").attr("value",interesL);
            $("[name=lch36]").text("No"); total(interesL);
            
    $("input[name=ch51]").click ->
        if( $(this).is(':checked'))
            interesL.push("51"); ordenar(interesL); total(interesL);
            $("[name=lch51]").text("Si");
            $("#tamizaje_p_interesL").attr("value",interesL);
        if( !$(this).is(':checked'))
            interesL.splice(interesL.indexOf("51"),1);
            $("#tamizaje_p_interesL").attr("value",interesL);
            $("[name=lch51]").text("No"); total(interesL);
            
    $("input[name=ch78]").click ->
        if( $(this).is(':checked'))
            interesL.push("78"); ordenar(interesL); total(interesL);
            $("[name=lch78]").text("Si");
            $("#tamizaje_p_interesL").attr("value",interesL);
        if( !$(this).is(':checked'))
            interesL.splice(interesL.indexOf("78"),1);
            $("#tamizaje_p_interesL").attr("value",interesL);
            $("[name=lch78]").text("No"); total(interesL);
    #Edad entre 13 y 15        
    $("input[name=ch16]").click ->
        if( $(this).is(':checked'))
            $("[name=lch16]").text("Si"); edad = $("#tamizaje_edad").val();
            if(edad >= 13 && edad <= 15)
                if( interesL.indexOf("16") == -1)
                    interesL.push("16"); 
                ordenar(interesL); total(interesL);
                $("#tamizaje_p_interesL").attr("value",interesL);
        if( !$(this).is(':checked'))
            $("[name=lch16]").text("No");
            interesL.splice(interesL.indexOf("16"),1);
            $("#tamizaje_p_interesL").attr("value",interesL);
            total(interesL);
    #Edad entre 13 y 15        
    $("input[name=ch44]").click ->
        if( $(this).is(':checked'))
            $("[name=lch44]").text("Si"); edad = $("#tamizaje_edad").val();
            if(edad >= 13 && edad <= 15)
                if( interesL.indexOf("44") == -1)
                    interesL.push("44"); 
                ordenar(interesL); total(interesL);
                $("#tamizaje_p_interesL").attr("value",interesL);
        if( !$(this).is(':checked'))
            $("[name=lch44]").text("No");
            interesL.splice(interesL.indexOf("44"),1);
            $("#tamizaje_p_interesL").attr("value",interesL);
            total(interesL);            
    #Edad entre 13 y 15        
    $("#tamizaje_edad").blur ->
        edad = $("#tamizaje_edad").val();
        if(edad >= 13 && edad <= 15)
            if ($("input[name=ch16]").is(':checked'))
                if( interesL.indexOf("16") == -1)
                    interesL.push("16"); 
                ordenar(interesL); total(interesL);
                $("#tamizaje_p_interesL").attr("value",interesL);
            else
                interesL.splice(interesL.indexOf("16"),1);
                $("#tamizaje_p_interesL").attr("value",interesL);
                total(interesL);
                
            if ($("input[name=ch44]").is(':checked'))
                if( interesL.indexOf("44") == -1)
                    interesL.push("44"); 
                ordenar(interesL); total(interesL);
                $("#tamizaje_p_interesL").attr("value",interesL);
            else
                interesL.splice(interesL.indexOf("44"),1);
                $("#tamizaje_p_interesL").attr("value",interesL);
                total(interesL);
        else 
            interesL.splice(interesL.indexOf("16"),1);
            interesL.splice(interesL.indexOf("44"),1);
            $("#tamizaje_p_interesL").attr("value",interesL);
            total(interesL);