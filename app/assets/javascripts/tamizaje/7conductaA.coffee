$(document).ready ->
    conductaA = [];
    
    ordenar = (arr) ->
        i=0; j=i+1;
        for i in [0..arr.length-1]
            for j in [i..arr.length-1]
                if arr[i] > arr[j]
                    aux = arr[i]
                    arr[i] = arr[j]
                    arr[j] = aux
                    
    total = (arr) ->
        if( arr.indexOf("24") > -1 || arr.indexOf("50") > -1 || arr.length >= 5)
            $("#tamizaje_total_conducta_agresiva").css("background", "#FF3C3C");
        else
            if (arr.indexOf("24") == -1 && arr.indexOf("50") == -1 && arr.length < 5)
                $("#tamizaje_total_conducta_agresiva").css("background", "#FFFFFF");
        
        $("#tamizaje_total_conducta_agresiva").attr("value",conductaA.length);
        
        valUs = $("#tamizaje_total_uso_sustancias").val();
        valSm = $("#tamizaje_total_salud_mental").val();
        valRf = $("#tamizaje_total_relacion_familiar").val();
        valRa = $("#tamizaje_total_relacion_amigos").val();
        valNe = $("#tamizaje_total_nivel_educativo").val();
        valIl = $("#tamizaje_total_interes_laboral").val();
        valCa = $("#tamizaje_total_conducta_agresiva").val();
        
        valResultado = parseInt(valUs) + parseInt(valSm) + parseInt(valRf) + parseInt(valRa) + parseInt(valNe) + parseInt(valIl) + parseInt(valCa);
        $("#tamizaje_resultado").attr("value",valResultado);
        
        
    
    #coffee-stir <tamizajes.coffee>
    $("#tamizaje_total_conducta_agresiva").attr("value","0");
    total(conductaA);
    #$("#tamizaje_total_uso_sustancias").attr("disabled",true);

    $("input[name=ch1]").click ->
        if( $(this).is(':checked'))
            conductaA.push("1"); ordenar(conductaA); total(conductaA);
            $("[name=lch1]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("1"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch1]").text("No"); total(conductaA);
            
    $("input[name=ch9]").click ->
        if( $(this).is(':checked'))
            conductaA.push("9"); ordenar(conductaA); total(conductaA);
            $("[name=lch9]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("9"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch9]").text("No"); total(conductaA);
            
    $("input[name=ch11]").click ->
        if( $(this).is(':checked'))
            conductaA.push("11"); ordenar(conductaA); total(conductaA);
            $("[name=lch11]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("11"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch11]").text("No"); total(conductaA);
            
    $("input[name=ch24]").click ->
        if( $(this).is(':checked'))
            conductaA.push("24"); ordenar(conductaA); total(conductaA);
            $("[name=lch24]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("24"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch24]").text("No"); total(conductaA);
            
    $("input[name=ch30]").click ->
        if( $(this).is(':checked'))
            conductaA.push("30"); ordenar(conductaA); total(conductaA);
            $("[name=lch30]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("30"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch30]").text("No"); total(conductaA);
            
    $("input[name=ch31]").click ->
        if( $(this).is(':checked'))
            conductaA.push("31"); ordenar(conductaA); total(conductaA);
            $("[name=lch31]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("31"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch31]").text("No"); total(conductaA);
            
    $("input[name=ch35]").click ->
        if( $(this).is(':checked'))
            conductaA.push("35"); ordenar(conductaA); total(conductaA);
            $("[name=lch35]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("35"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch35]").text("No"); total(conductaA);
            
    $("input[name=ch37]").click ->
        if( $(this).is(':checked'))
            conductaA.push("37"); ordenar(conductaA); total(conductaA);
            $("[name=lch37]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("37"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch37]").text("No"); total(conductaA);
            
    $("input[name=ch49]").click ->
        if( $(this).is(':checked'))
            conductaA.push("49"); ordenar(conductaA); total(conductaA);
            $("[name=lch49]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("49"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch49]").text("No"); total(conductaA);
            
    $("input[name=ch50]").click ->
        if( $(this).is(':checked'))
            conductaA.push("50"); ordenar(conductaA); total(conductaA);
            $("[name=lch50]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("50"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch50]").text("No"); total(conductaA);
            
    $("input[name=ch53]").click ->
        if( $(this).is(':checked'))
            conductaA.push("53"); ordenar(conductaA); total(conductaA);
            $("[name=lch53]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("53"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch53]").text("No"); total(conductaA);
            
    $("input[name=ch59]").click ->
        if( $(this).is(':checked'))
            conductaA.push("1"); ordenar(conductaA); total(conductaA);
            $("[name=lch59]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("59"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch59]").text("No"); total(conductaA);
            
    $("input[name=ch64]").click ->
        if( $(this).is(':checked'))
            conductaA.push("64"); ordenar(conductaA); total(conductaA);
            $("[name=lch64]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("64"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch64]").text("No"); total(conductaA);
            
    $("input[name=ch81]").click ->
        if( $(this).is(':checked'))
            conductaA.push("81"); ordenar(conductaA); total(conductaA);
            $("[name=lch81]").text("Si");
            $("#tamizaje_p_conductaA").attr("value",conductaA);
        if( !$(this).is(':checked'))
            conductaA.splice(conductaA.indexOf("81"),1);
            $("#tamizaje_p_conductaA").attr("value",conductaA);
            $("[name=lch81]").text("No"); total(conductaA);