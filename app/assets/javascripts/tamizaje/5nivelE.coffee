$(document).ready ->
    nivelE = ["12","18","26","69"];
    
    ordenar = (arr) ->
        i=0; j=i+1;
        for i in [0..arr.length-1]
            for j in [i..arr.length-1]
                if arr[i] > arr[j]
                    aux = arr[i]
                    arr[i] = arr[j]
                    arr[j] = aux
                    
    total = (arr) ->
        if( arr.indexOf("26") > -1 || arr.indexOf("42") > -1 || arr.indexOf("66") > -1 || arr.indexOf("69") > -1 || arr.indexOf("72") > -1 || arr.indexOf("80") > -1 || arr.length >= 5)
            $("#tamizaje_total_nivel_educativo").css("background", "#FF3C3C");
        else
            if (arr.indexOf("26") == -1 && arr.indexOf("42") == -1 && arr.indexOf("66") == -1 && arr.indexOf("69") == -1 && arr.indexOf("72") == -1 && arr.indexOf("80") == -1 && arr.length < 5)
                $("#tamizaje_total_nivel_educativo").css("background", "#FFFFFF");
        
        $("#tamizaje_total_nivel_educativo").attr("value",nivelE.length);
        
        valUs = $("#tamizaje_total_uso_sustancias").val();
        valSm = $("#tamizaje_total_salud_mental").val();
        valRf = $("#tamizaje_total_relacion_familiar").val();
        valRa = $("#tamizaje_total_relacion_amigos").val();
        valNe = $("#tamizaje_total_nivel_educativo").val();
        valIl = $("#tamizaje_total_interes_laboral").val();
        valCa = $("#tamizaje_total_conducta_agresiva").val();
        
        valResultado = parseInt(valUs) + parseInt(valSm) + parseInt(valRf) + parseInt(valRa) + parseInt(valNe) + parseInt(valIl) + parseInt(valCa);
        $("#tamizaje_resultado").attr("value",valResultado);
        
        
    
    #coffee-stir <tamizajes.coffee>
    $("#tamizaje_total_nivel_educativo").attr("value",nivelE.length);
    $("#tamizaje_p_nivelE").attr("value",nivelE);
    total(nivelE);
    
    #$("#tamizaje_total_uso_sustancias").attr("disabled",true);

    $("input[name=ch7]").click ->
        if( $(this).is(':checked'))
            nivelE.push("7"); ordenar(nivelE); total(nivelE);
            $("[name=lch7]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("7"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch7]").text("No"); total(nivelE);
            
    $("input[name=ch8]").click ->
        if( $(this).is(':checked'))
            nivelE.push("8"); ordenar(nivelE); total(nivelE);
            $("[name=lch8]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("8"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch8]").text("No"); total(nivelE);
    #Pregunta en NO        
    $("input[name=ch12]").click ->
        if( $(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("12"),1); total(nivelE);
            $("[name=lch12]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.push("12"); ordenar(nivelE);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch12]").text("No"); total(nivelE);
            
    $("input[name=ch15]").click ->
        if( $(this).is(':checked'))
            nivelE.push("15"); ordenar(nivelE); total(nivelE);
            $("[name=lch15]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("15"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch15]").text("No"); total(nivelE);
    #Pregunta en NO        
    $("input[name=ch18]").click ->
        if( $(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("18"),1); total(nivelE);
            $("[name=lch18]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.push("18"); ordenar(nivelE);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch18]").text("No"); total(nivelE);
    #Pregunta en NO        
    $("input[name=ch26]").click ->
        if( $(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("26"),1); total(nivelE);
            $("[name=lch26]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.push("26"); ordenar(nivelE);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch26]").text("No"); total(nivelE);
            
    $("input[name=ch34]").click ->
        if( $(this).is(':checked'))
            nivelE.push("34"); ordenar(nivelE); total(nivelE);
            $("[name=lch34]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("34"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch34]").text("No"); total(nivelE);
            
    $("input[name=ch40]").click ->
        if( $(this).is(':checked'))
            nivelE.push("40"); ordenar(nivelE); total(nivelE);
            $("[name=lch40]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("40"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch40]").text("No"); total(nivelE);
            
    $("input[name=ch42]").click ->
        if( $(this).is(':checked'))
            nivelE.push("42"); ordenar(nivelE); total(nivelE);
            $("[name=lch42]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("42"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch42]").text("No"); total(nivelE);
            
    $("input[name=ch61]").click ->
        if( $(this).is(':checked'))
            nivelE.push("61"); ordenar(nivelE); total(nivelE);
            $("[name=lch61]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("61"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch61]").text("No"); total(nivelE);
            
    $("input[name=ch66]").click ->
        if( $(this).is(':checked'))
            nivelE.push("66"); ordenar(nivelE); total(nivelE);
            $("[name=lch66]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("66"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch66]").text("No"); total(nivelE);
    #Pregunta en NO        
    $("input[name=ch69]").click ->
        if( $(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("69"),1); total(nivelE);
            $("[name=lch69]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.push("69"); ordenar(nivelE);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch69]").text("No"); total(nivelE);
            
    $("input[name=ch72]").click ->
        if( $(this).is(':checked'))
            nivelE.push("72"); ordenar(nivelE); total(nivelE);
            $("[name=lch72]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("72"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch72]").text("No"); total(nivelE);
            
    $("input[name=ch74]").click ->
        if( $(this).is(':checked'))
            nivelE.push("74"); ordenar(nivelE); total(nivelE);
            $("[name=lch74]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("74"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch74]").text("No"); total(nivelE);
            
    $("input[name=ch79]").click ->
        if( $(this).is(':checked'))
            nivelE.push("79"); ordenar(nivelE); total(nivelE);
            $("[name=lch79]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("79"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch79]").text("No"); total(nivelE);
            
    $("input[name=ch80]").click ->
        if( $(this).is(':checked'))
            nivelE.push("80"); ordenar(nivelE); total(nivelE);
            $("[name=lch80]").text("Si");
            $("#tamizaje_p_nivelE").attr("value",nivelE);
        if( !$(this).is(':checked'))
            nivelE.splice(nivelE.indexOf("80"),1);
            $("#tamizaje_p_nivelE").attr("value",nivelE);
            $("[name=lch80]").text("No"); total(nivelE);