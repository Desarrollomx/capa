# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->

    num = ""
    url = ""
    ordenar = (arr) ->
        i=0; j=i+1;
        for i in [0..arr.length-1]
            for j in [i..arr.length-1]
                if arr[i] > arr[j]
                    aux = arr[i]
                    arr[i] = arr[j]
                    arr[j] = aux
                    
    append = (gx,num,name,texto) ->
        $(".div-txt#{gx}-#{num}").append("
        <div class='txtf_#{gx}-#{num}'>
            <label for='#{name}' class='col-md-3'>#{texto}:</label>
            
                <input name='#{name}' id='txtf_#{gx}-#{num}' value='' class='grupo02 col-md-3 padding-txt' type='text'>
            
        </div>")

        
    $(document).keypress (e) ->
        if(e.keyCode == 116)
            $('input:checkbox').prop('checked',false);
            
    $("#no_grupos").blur ->
        num = $("#no_grupos").val()
       # if num != ""
        x = $("#btn_no_grupos").attr("href")
      #      if x != ""
        arr = x.split "F"
        url = arr[0] + "F"
      #      else
               # url = "/tamizajes/.00b1b22cf54d65=1434%3F"
       # else
       #$("#btn_no_grupos").removeAttr("href")
    
    $("#btn_no_grupos").click ->
       # if num != ""
         $("#btn_no_grupos").attr("href",url+num)
       # else
           # $("#btn_no_grupos").removeAttr("href")
        
    if($("#snackbar").attr("class") == "show")
        setTimeout (-> $("#snackbar").attr("class","")), 9500
    
    
    #$(window).load ->
        #alert "mensaje hola"

    #$('#show').click ->
        #location.reload();
        #url = $('#show').attr('href');
        #window.location = url

        
    gd = ""; gp = "" #grado grupo
    #Leer grupos por selected
    $("#grupos-cont").on "blur", ".grupo01", (e) -> 
        id = ($ e.target).attr("id")
        id_serie = id.split "-"
        serie = id_serie[1]
        name_id = id_serie[0]
        
        if (name_id == "select_gd")
            gd = $('#'+id).val()
            if($("#otrogp-"+serie).is(':checked'))
                gp = $('#txtf_gp-'+serie).val()
            else
                gp = $('#select_gp-'+serie).val()
        else
            if (name_id == "select_gp")
                gp = $('#'+id).val()
                if($("#otrogd-"+serie).is(':checked'))
                    gd = $('#txtf_gd-'+serie).val()
                else
                    gd = $('#select_gd-'+serie).val()
        
        if (gp != "" && gd != "")
            index = parseInt(serie)
            gg = gd + "-"+gp
            array[index] = gg
            $("#txtf_gs").attr("value",array)
        else
            i = parseInt(serie)
            if((gd == "" && gp == ""))
                gg = "Grado y grupo "+(i+1)+" no definido"
            else
                if(gd == "")
                    gg = "Grado "+(i+1)+" no definido"
                else
                    if(gp == "")
                        gg = "Grupo "+(i+1)+" no definido"
                        
            index = parseInt(serie)
            array[index] = gg
            $("#txtf_gs").attr("value",array)      


    #Leer grupos por textfield
    $("#grupos-cont").on "blur", ".grupo02", (e) -> 
        id = ($ e.target).attr("id")
        id_serie = id.split "-"
        serie = id_serie[1]
        name_id = id_serie[0]
        
        if (name_id == "txtf_gd")
            gd = $('#'+id).val()            
            if($("#otrogp-"+serie).is(':checked'))
                gp = $('#txtf_gp-'+serie).val()
            else
                gp = $('#select_gp-'+serie).val()
        else
            if (name_id == "txtf_gp")
                gp = $('#'+id).val()
                if($("#otrogd-"+serie).is(':checked'))
                    gd = $('#txtf_gd-'+serie).val()
                else
                    gd = $('#select_gd-'+serie).val()
        
        if (gp != "" && gd != "")
            index = parseInt(serie)
            gg = gd + "-"+gp
            array[index] = gg
            $("#txtf_gs").attr("value",array)
        else
            i = parseInt(serie)
            if((gd == "" && gp == ""))
                gg = "Grado y grupo "+(i+1)+" no definido"
            else
                if(gd == "")
                    gg = "Grado "+(i+1)+" no definido"
                else
                    if(gp == "")
                        gg = "Grupo "+(i+1)+" no definido"
                        
            index = parseInt(serie)
            array[index] = gg
            $("#txtf_gs").attr("value",array)      
            
            
    #Add Textfield grado o remove
    $("#grupos-cont").on "click", ".check01", (e) -> 
        id = ($ e.target).attr("id")
        id_serie = id.split "-"
        serie = id_serie[1]
        name_id = id_serie[0]
        
        if($("#otrogd-"+serie).is(':checked'))
            $("#select_gd-"+serie).attr("disabled","disabled")
            $("#select_gd-"+serie).val("")
            $("#select_gd-"+serie).attr("class","select btn-xs grupo01 select-dis")
            gd = ""
            
            append("gd",serie,"grado","Grado")
        else
            if(!$("#otrogd-"+serie).is(':checked'))
                $("#select_gd-"+serie).removeAttr("disabled")
                $("#select_gd-"+serie).attr("class","select btn-xs grupo01")
                $(".txtf_gd-#{serie}").remove() #quitar por 'class'
        
    #Add Textfield grupo o remove
    $("#grupos-cont").on "click", ".check02", (e) -> 
        id = ($ e.target).attr("id")
        id_serie = id.split "-"
        serie = id_serie[1]
        name_id = id_serie[0]
    
        if($("#otrogp-"+serie).is(':checked'))
            $("#select_gp-"+serie).attr("disabled","disabled")
            $("#select_gp-"+serie).val("")
            $("#select_gp-"+serie).attr("class","select btn-xs grupo01 select-dis")
            gp = ""
            #(gx: gd o gp,numero de grupo,name: nombre o id,texto)
            append("gp",serie,"grupo","Grupo")
            
        else
            if(!$("#otrogp-"+serie).is(':checked'))
                $("#select_gp-"+serie).removeAttr("disabled")    
                $("#select_gp-"+serie).attr("class","select btn-xs grupo01")
                $(".txtf_gp-#{serie}").remove() #quitar por 'class'
    
    
        
    $("#tamizaje_grado").blur ->
        index = $("#tamizaje_grado").prop('selectedIndex')
        $("#tamizaje_grupo").prop('selectedIndex', index)
        
    $("#tamizaje_grupo").blur ->
        index = $("#tamizaje_grupo").prop('selectedIndex')
        $("#tamizaje_grado").prop('selectedIndex', index)
    #$('#abcd').click ->
        #window.location = location.pathname + "/2"
    
        #alert $("#show_index").attr("data")
        #window.px = "Hola desde coffee"
        #window.px = escape("Hola desde coffee")
        #root =  exports ? this
        #root.foo = -> 'Hello World'
        #@px = "Hola desde coffee" #variable global
        #alert "coffe-"+px
    #$('#snackbar').click ->
        #$.snackbar({content: "algo",style: "toast", timeout: 10000});
        #$.snackbar({content: "algo",style: "toast", timeout: 10000});
        #alert url
    #Uso Abuso

            
    #$("input[name=ch38]").click ->
        #$("[name=p_usoS]").text("Agregado");
        
    #$("input[name=ch41]").click ->
#        $("[name=p_usoS]").text("Agregado");
        
#    $("input[name=ch46]").click ->
#        $("[name=p_usoS]").text("Agregado");
        
#    $("input[name=ch47]").click ->
#        $("[name=p_usoS]").text("Agregado");
        
        
        

    
#    tabla = document.getElementById("contenido");
#    $('#Python').click ->
#        tabla.innerHTML = "Python";
            

#    $('#Django').click ->
#        tabla.innerHTML = "Django";
            

#    $('#Jython').click ->
#        tabla.innerHTML = "Jython";      
            
#    $('#check1').click ->
#        $(this).is(":checked") ->
#            alert("Click");

#        $('#textbox1').val($(this).is(':checked')); 

        
#    $("input[name=edad]").click ->
#        alert("La edad seleccionada es:" + $('input:radio[name=edad]:checked').val());
#        alert("La edad seleccionada es:" + $(this).val());
    
#    $("input[name=ch]").click ->
#        alert("El valor es:" + $('input:checkbox[name=ch]:checked').val());
        
#    $("input:radio[name=sexo]").click ->
#        alert("Valor "+$(this).val());

#    $("input[name=ch1]").click ->
#        if( $(this).is(':checked'))
#            alert("Seleccion:" + $(this).val());
#        if( !$(this).is(':checked'))
#            alert("Dese:" + $(this).val());
            
#    $("#guardar").click ->
#        valor = document.formul.valor.value;

#        $("#division").data("midato",valor);
#        $("#division").html('He guardado en este elemento un dato llamado "midato" con el valor "' + valor + '"');

#    $("input[name=check1]").click ->
#        if( $("input[name='check1']:checkbox").is(':checked')) 
#            alert("El valor es:" + $('input:checkbox[name=check1]:checked').val());

            
            
            
    
        
        
