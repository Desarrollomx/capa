# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
    valUs = parseInt($("#r_us").text()); 
    valSm = parseInt($("#r_sm").text());
    valRf = parseInt($("#r_rf").text());
    valRa = parseInt($("#r_ra").text());
    valNe = parseInt($("#r_ne").text());
    valIl = parseInt($("#r_il").text());
    valCa = parseInt($("#r_ca").text());
    valRg = parseInt($("#r_rg").text());
    colors1 = ["#A6A57A","#E55F00"];
    colors2 = ["#947E00","#FF007C","#006FFF","#E55F00","#008A85","#289F00","#9200AC","#A6A57A"];
        
    grafica = (flagColor, arrColors) ->
        $('#grafica').highcharts({
            title:{
                text: "Grafica Tamizaje"
            },
            chart: {
                type: 'bar'
                #type: 'column'
                plotBackgroundColor: '#FCFFC5'
                backgroundColor: {
                    linearGradient: [0, 0, 500, 500],
                    stops: [
                        [0, 'rgb(255, 255, 255)'],
                        [1, 'rgb(200, 200, 255)']
                    ]
                },            
            },        
            colors: arrColors,
            xAxis: {
                categories: ['Uso Abuso', 'Salud Mental', 'Relacion F.', 'Relacion A.', 'Nivel E.', 'Interes L.', 'Conducta A.', 'Resultado']
            },
            yAxis:{
                title:{ 
                    text: "Puntaje"
                }

            },
            series: [{
                id: 'riesgo',
                name: 'Riesgo',
                data: [1,5,3,1,5,3,5,34],
                #color: "#A6A57A"
                style: {
                   color: '#3E576F'
                }

            }, {
                id: 'resultado',
                name: 'Resultado',
                data: [valUs , valSm , valRf , valRa , valNe , valIl , valCa , valRg ]
                borderWidth: 2,
                borderColor: 'black'        
                #color: "#947E00"
            }]
            plotOptions: {
                series: {
                    colorByPoint: flagColor
                    states: {
                        hover: {
                            halo: {
                                size: 9,
                                attributes: {
                                    fill: Highcharts.getOptions().colors[2],
                                    'stroke-width': 2,
                                    stroke: Highcharts.getOptions().colors[1]
                                }
                            }

                        }
                    }
                }
                colum:{
                    shadow: true
                }
            },             
            tooltip: {
                valueSuffix: ' p.'
            },                                 
            legend: {
                layout: 'vertical',
                backgroundColor: '#FFFFFF',
                floating: true,
                align: 'right',
                verticalAlign: 'top',
                y: 70
                labelFormatter: ->
                    return '-'+this.name;
                shadow: true
                #symbolPadding: 20,
                #symbolWidth: 50            
                navigation: {
                    activeColor: '#3E576F',
                    animation: true,
                    arrowSize: 12,
                    inactiveColor: '#CCC',
                    style: {
                        fontWeight: 'bold',
                        color: '#333',
                        fontSize: '12px'
                    }
                },
                                 
                title: {
                    text: 'Parametros<br/><span style="font-size: 10px; color: #666; font-weight: normal">(Click mostrar/ocultar)</span>',
                    style: {
                        fontStyle: 'italic'
                    }
                },                                 
                itemStyle: {
                    color: '#000000',
                    fontWeight: 'bold'
                }


            },                            
        });            
    
    grafica(false,colors1)
    
    bandera = 2
    colorby = true
    colors = []
    
    $('#color').click ->
        if(bandera == 1)
            colors = colors1;    
            bandera = 2
            colorby = false
        else
            colors = colors2;
            bandera = 1
            colorby = true
        
        grafica(colorby,colors);


