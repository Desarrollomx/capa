class AdolescentesAp < ActiveRecord::Base
	belongs_to :psychologist
	before_save :default_values
	before_save :suma

    validates :psychologist_id, :fecha, :presence=> true

	def default_values
		self.h ||=0
		self.h3 ||=0
		self.h4 ||=0
		self.m ||=0
		self.m3 ||=0
		self.m4 ||=0
	end

	def suma
		self.h=self.h3+self.h4
		self.m=self.m3+self.m4
	end

	#def self.query(psy, id)
        #tmp=psy
        #tmp1=id
        #tmp2=" "
        #tmp3=" "
        #if tmp1.eql?(1)
            #date = Date.today
            #año= date.strftime"%Y".to_s
            #mes= date.strftime"%m".to_s
            #tmp2=año+"-"+mes+"-"+"01"
            #tmp3=año+"-"+mes+"-"+"19"
        #elsif tmp1.eql?(2)
            #date = Date.today
            #año= date.strftime"%Y".to_s
            #mes=(((date.strftime"%m").to_i)-1).to_s
            #tmp2=año+"-"+mes+"-"+"20"
            #tmp3=año+"-"+mes+"-"+"31"
        #end 
        
    #qf1= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14).count
    #qf2= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17).count    	
    #qf3= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "subsecuente").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14).count
    #qf4= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "subsecuente").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17).count
    #qf5= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "recaida").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14).count
    #qf6= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "recaida").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17).count
    #qf7= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "segimiento").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14).count	
    #qf8= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "segimiento").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17).count
    	
    #tf12_14=qf1+qf3+qf5+qf7
    #tf15_17=qf2+qf4+qf6+qf8
    #tf=tf12_14+tf15_17

    #qm1= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14).count       
    #qm2= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17).count     
    #qm3= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "subsecuente").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14).count       
    #qm4= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "subsecuente").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17).count     
    #qm5= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "recaida").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14).count     
    #qm6= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "recaida").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17).count     
    #qm7= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "segimiento").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14).count     
    ##qm8= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "segimiento").count
    #qm8= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "segimiento").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17).count         	
    #tm12_14=qm1+qm3+qm5+qm7
    #tm15_17=qm2+qm4+qm6+qm8
    #tm=tm12_14+tm15_17
    ##Meeting.joins(:patient).where(:psychologist_id=>params[tmp], :fecha=>(params[:date_start]..params[:date_end]),(date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31")).includes(patient_counts)
    #AdolescentesAp.create(h:tm, m:tf, fecha:tmp3, psychologist_id:tmp, h3:tm12_14, h4:tm15_17, m3:tf12_14, m4:tf15_17)

	#end
end
