class GruposEscuela < ActiveRecord::Base
  belongs_to :aplicacion_tamizaje
    
    validates :grupos, :presence => {:message => "El campo de grupos, no debe ir en blanco." }
end
