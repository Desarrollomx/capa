class Meeting < ActiveRecord::Base
	before_save :upcase
	belongs_to :user
	belongs_to :patient
	belongs_to :psychologist
	validates :name, :presence => true
	validates :start_time, :presence =>true
	validates :patient_id, :presence =>true

	#scope :crear , -> {create(fecha:Time.now,aasm_status: @meetings.patient.aasm_state, patient_id: @meetings.patient_id, psychologist_id: @meetings.psychologist_id )}
	def upcase
		self.name.upcase!
	end
end
