class AdolescentesIt < ActiveRecord::Base
	belongs_to :psychologist
	before_save :default_values
	before_save :suma

    validates :psychologist_id, :fecha, :presence=> true

	def default_values
		self.h ||=0
		self.h3 ||=0
		self.h4 ||=0
		self.m ||=0
		self.m3 ||=0
		self.m4 ||=0
	end

	def suma
		self.h=self.h3+self.h4
		self.m=self.m3+self.m4
	end

	def self.query(psy, id)
        tmp=psy
        tmp1=id
        tmp2=" "
        tmp3=" "
        if tmp1.eql?(1)
            date = Date.today
            año= date.strftime"%Y".to_s
            mes= date.strftime"%m".to_s
            tmp2=año+"-"+mes+"-"+"01"
            tmp3=año+"-"+mes+"-"+"19"
        elsif tmp1.eql?(2)
            date = Date.today
            año= date.strftime"%Y".to_s
            mes=(((date.strftime"%m").to_i)-1).to_s
            tmp2=año+"-"+mes+"-"+"20"
            tmp3=año+"-"+mes+"-"+"31"
        end 
        
		##Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",20,29 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,4,"2016-03-01","2016-06-31").includes(:patient_counts).where("aasm_state = ?", "primera").count
    #qf1= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf1= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14).count
    #qf2= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf2= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17).count
    	
    ##queryf3= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "subsecuente").count
    ##queryf4= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "subsecuente").count
    ##queryf5= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "recaida").count
    ##queryf6= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "recaida").count
    ##queryf7= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "segimiento").count
    ##queryf8= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "segimiento").count
    ##queryf9= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "alta").count
    ##queryf10= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "alta").count
    tf=qf1+qf2

		#qm1= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm1= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14).count     
    #qm2= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm2= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17).count     
    ##querym3= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "subsecuente").count
    ##querym4= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "subsecuente").count
    ##querym5= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "recaida").count
    ##querym6= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "recaida").count
    ##querym7= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "segimiento").count
    ##querym8= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "segimiento").count
    ##querym9= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "alta").count
    ##querym10= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,params[:id],date.strftime "%Y-%m-01",date.strftime "%Y-%m-31").includes(:patient_counts).where("aasm_state = ?", "alta").count
    tm=qm1+qm2
    ##Meeting.joins(:patient).where(:psychologist_id=>params[tmp], :fecha=>(params[:date_start]..params[:date_end]),(date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31")).includes(patient_counts)
    AdolescentesIt.create(h:tm, m:tf, fecha:tmp3, psychologist_id:tmp, h3:qm1, h4:qm2, m3:qf1, m4:qf2)
	end

end
