class Tamizaje < ActiveRecord::Base
    belongs_to :aplicacion_tamizaje
    before_save :default_values
    
    validates :nombre, :presence => {:message => "Nombre no debe ir en blanco." }
    validates :apellidoP, :presence => {:message => "Apellido paterno, no debe ir en blanco." }
    validates :apellidoM, :presence => {:message => "Apellido materno, no debe ir en blanco." }
    validates :sexo, :presence => {:message => "Genero no debe ir en blanco." }
    validates :edad, :presence => {:message => "La edad, no debe ir en blanco." }
    validates :grado, :presence => {:message => "El grado, no debe ir en blanco." }
    validates :grupo, :presence => {:message => "El grupo, no debe ir en blanco." }
    validates :total_uso_sustancias, :presence => {:message => "El total de uso de sustancias fue modificado." }
    validates :total_salud_mental, :presence => {:message => "El total de salud mental fue modificado." }
    validates :total_relacion_familiar, :presence => {:message => "El total de relacion familiar fue modificado." }
    validates :total_relacion_amigos, :presence => {:message => "El total de relacion con amigos fue modificado." }
    validates :total_nivel_educativo, :presence => {:message => "El total de nivel educativo fue modificado." }
    validates :total_interes_laboral,  :presence => {:message => "El total de interes laboral fue modificado." }
    validates :total_conducta_agresiva, :presence => {:message => "El total de conducta agresiva fue modificado." }
    validates :resultado,  presence: {:message => "El resultado fue modificado." }
    validates :aplicacion_tamizaje_id, presence:true
    

    
    def default_values
        if p_usoS.blank?
            self.p_usoS ="0"
        end
        if p_saludM.blank?
            self.p_saludM ="0"
        end
        if p_relacionF.blank?
            self.p_relacionF ="0"
        end
        if p_interesL.blank?
            self.p_interesL ="0"
        end
        if p_relacionA.blank?
            self.p_relacionA ="0"
        end
        if p_nivelE.blank?
            self.p_nivelE ="0"
        end
        if p_conductaA.blank?
            self.p_conductaA ="0"
        end
    end
end
