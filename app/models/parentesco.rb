class Parentesco < ActiveRecord::Base
  before_save :upcase

  validates :parentesco, :presence => true

  def upcase
    self.parentesco.upcase!
  end
end
