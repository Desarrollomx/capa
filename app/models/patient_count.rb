class PatientCount < ActiveRecord::Base
  belongs_to :patient
  belongs_to :psychologist
end
