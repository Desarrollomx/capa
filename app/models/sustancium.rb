class Sustancium < ActiveRecord::Base
	before_save :upcase
	has_one :expediente
	validates :sustancia, :clave, :presence=>true
	scope :ultimos, -> { order("created_at DESC").limit(5)}

	def upcase
    self.sustancia.upcase!
		self.clave.upcase!
	end
end
