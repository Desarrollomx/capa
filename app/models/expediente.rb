class Expediente < ActiveRecord::Base
	belongs_to :patient
	belongs_to :programa
	belongs_to :psychologist
	belongs_to :sustancium
	belongs_to :riesgo
  	validates :pago, :length => {:in => 1..3}
  	validates :id_expediente, :length => {:in => 1..5}
  	validates :patient_id, :psychologist_id, :programa_id, :sustancium_id, :presence=>true

	scope :ultimos, -> { order("created_at DESC").limit(5)}
	def update_sesion_count
		self.save if self.sesion.nil?
		self.update(sesion: self.sesion + 1 )
	end
end
