class Patient < ActiveRecord::Base
	include AASM
	before_save :upcase
	before_save :conocido
	belongs_to :state
	belongs_to :municipio
	#belongs_to :familiar
	belongs_to :etnium
	belongs_to :civil
	has_one :expediente
	has_many :meetings
	has_one :familiar
	has_many :patient_counts

	validates :tel, :length => {:in => 7..10}
	#validates :length => {:in => 1..3}
	validates :nom, :app, :apm, :municipio_id, :presence=> true
	accepts_nested_attributes_for :familiar#, :allow_destroy => true

	scope :ultimos, -> { order("created_at DESC").limit(5)}

	aasm do #column: "status" do
		state :primera, initial: true
		state :subsecuente
		state :recaida
		state :alta
		state :segimiento
		state :dejar

		event :primer do
			transitions from: :subsecuente, to: :primera
		end

		event :primer_consulta do
			transitions from: :primera, to: :subsecuente
		end

		event :termino_programa do
			transitions from: :subsecuente, to: :alta
		end

		event :recaer do
			transitions from: :alta,  to: :primera
		end
	end

	def upcase
    		self.nom.upcase!
		self.app.upcase!
		self.apm.upcase!
		self.calle.upcase!
		self.col.upcase!
    end
    def conocido
	if self.calle.blank?
		self.calle="CONOCIDO"
	end
	if self.col.blank?
		self.col="CONOCIDO"
	end
    end

end
