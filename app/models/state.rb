class State < ActiveRecord::Base
	has_many :municipios
	has_many :patients
	before_save :upcase

	validates :estados, :presence=> true

	scope :orden, -> { order("estados ASC")}
	def upcase
    self.estados.upcase!
	end
end
