class Estatus < ActiveRecord::Base
  before_save :upcase

  def upcase
    self.status.upcase!
  end
end
