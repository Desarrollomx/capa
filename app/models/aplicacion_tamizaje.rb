class AplicacionTamizaje < ActiveRecord::Base
    before_save :upcase
    has_many :tamizajes
    has_many :grupos_escuelas
    belongs_to :municipio
    belongs_to :psychologist
    belongs_to :nivel_estudio

    accepts_nested_attributes_for :grupos_escuelas

    validates :escuela, :presence => {:message => "El nombre de la escuela, no debe ir en blanco." }
    validates :colonia, :presence => {:message => "La colonia, no debe ir en blanco." }
    validates :municipio_id, :presence => {:message => "Municipio no guardado." }
    validates :nivel_estudio_id,:presence => {:message => "Nivel de estudio no guardado." }
    validates :psychologist_id, :presence => {:message => "Psicologo no asignado." }

    def upcase
      self.escuela.upcase!
      self.colonia.upcase!
    end

end
