class Etnium < ActiveRecord::Base
  has_many :patients
  before_save :upcase
  def upcase
    self.etnia.upcase!
  end
end
