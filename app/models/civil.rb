class Civil < ActiveRecord::Base
  has_many :patients
  before_save :upcase

  def upcase
    self.civil.upcase!
  end
end
