class Municipio < ActiveRecord::Base
	belongs_to :state
	before_save :upcase
	has_many :patients
  has_many :aplicacion_tamizajes

	validates :state_id, :presence=> true

	scope :ultimos, -> { order("created_at DESC").limit(5)}

	def upcase
    self.municipio.upcase!
	end
end
