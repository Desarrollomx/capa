class CapacitacionesOt < ActiveRecord::Base
	belongs_to :psychologist
	before_save :default_values
	before_save :suma
	
    validates :psychologist_id, :fecha, :presence=> true

	def default_values
		self.h ||=0
		self.h1 ||=0
		self.h2 ||=0
		self.h3 ||=0
		self.h4 ||=0
		self.h5 ||=0
		self.h6 ||=0
		self.h7 ||=0
		self.h8 ||=0
		self.h9 ||=0
		self.h10 ||=0
		self.m ||=0
		self.m1 ||=0
		self.m2 ||=0
		self.m3 ||=0
		self.m4 ||=0
		self.m5 ||=0
		self.m6 ||=0
		self.m7 ||=0
		self.m8 ||=0
		self.m9 ||=0
		self.m10 ||=0
		#self.psychologist_id ||= current_user.psychologist_id
	end

	def suma
		self.h = (self.h1+self.h2+self.h3+self.h4+self.h5+self.h6+self.h7+self.h8+self.h9+self.h10)
		self.m = (self.m1+self.m2+self.m3+self.m4+self.m5+self.m6+self.m7+self.m8+self.m9+self.m10)
	end
end
