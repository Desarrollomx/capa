module PermissionsConcern
	extend ActiveSupport::Concern
	def is_normal_user?
		self.permission_level >= 1
	end
	def is_recepcion?
		self.permisson_level >= 2
	end
	def is_psicologo?
		self.permisson_level >= 3
	end
  def is_admin?
		self.permisson_level >= 4
	end
end
