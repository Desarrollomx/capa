class Familiar < ActiveRecord::Base
  before_save :upcase
  belongs_to :state
  belongs_to :municipio
  belongs_to :parentesco
  belongs_to :patient

  validates :nom, :app, :apm, :state_id, :municipio_id, :parentesco_id ,presence: true
  validates :tel, :length => {:in => 7..10}
	validates :edad, :length => {:in => 1..3}

  def upcase
    self.nom.upcase!
    self.app.upcase!
    self.apm.upcase!
    self.direccion.upcase!
  end
end
