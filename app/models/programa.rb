class Programa < ActiveRecord::Base
	before_save :upcase
	has_many :expedientes
	validates :programa, :presence=>true

	scope :ultimos, -> { order("created_at DESC").limit(5)}

	def upcase
    self.programa.upcase!
	end
end
