class ConsultasPv < ActiveRecord::Base
	belongs_to :psychologist
	before_save :default_values
	before_save :suma

    validates :psychologist_id, :fecha, :presence=> true

	def default_values
		self.h ||=0
		self.h1 ||=0
		self.h2 ||=0
		self.h3 ||=0
		self.h4 ||=0
		self.h5 ||=0
		self.h6 ||=0
		self.h7 ||=0
		self.h8 ||=0
		self.h9 ||=0
		self.h10 ||=0
		self.m ||=0
		self.m1 ||=0
		self.m2 ||=0
		self.m3 ||=0
		self.m4 ||=0
		self.m5 ||=0
		self.m6 ||=0
		self.m7 ||=0
		self.m8 ||=0
		self.m9 ||=0
		self.m10 ||=0
	end

	def suma
		self.h = (self.h1+self.h2+self.h3+self.h4+self.h5+self.h6+self.h7+self.h8+self.h9+self.h10)
		self.m = (self.m1+self.m2+self.m3+self.m4+self.m5+self.m6+self.m7+self.m8+self.m9+self.m10)
	end

	def self.query(psy, id)
    tmp=psy
    tmp1=id
    tmp2=" "
    tmp3=" "
    if tmp1.eql?(1)
      date = Date.today
        año= date.strftime"%Y".to_s
        mes= date.strftime"%m".to_s
        tmp2=año+"-"+mes+"-"+"01"
        tmp3=año+"-"+mes+"-"+"19"
    elsif tmp1.eql?(2)
      date = Date.today
        año= date.strftime"%Y".to_s
        mes=(((date.strftime"%m").to_i)-1).to_s
        tmp2=año+"-"+mes+"-"+"20"
        tmp3=año+"-"+mes+"-"+"31"
    end 
    
    
    ##@consulta= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",20,29 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,5,"2016-01-01","2016-06-31").includes(:patient_counts).where("aasm_state = ?", "primera").count
    	##Psychologist.find(5).patient_counts.count
    ##Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",20,29 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,4,"2016-03-01","2016-06-31").includes(:patient_counts).where("aasm_state = ?", "primera").count
    
    #qf1= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",5,9 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf1= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",5,9).count
    #qf2= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",10,11 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf2= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",10,11).count
    #qf3= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf3= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",12,14).count
    #qf4= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf4= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",15,17).count
    #qf5= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",18,19 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf5= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",18,19).count    
    #qf6= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",20,29 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf6= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",20,29).count    
    #qf7= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",30,34 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf7= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",30,34).count    
    #qf8= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",35,49 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf8= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",35,49).count
    #qf9= Patient.where("sexo =? AND edad >=? AND edad<=?","Femenino",50,59 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf9= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Femenino",50,59).count    
    #qf10= Patient.where("sexo =? AND edad >=?","Femenino",60 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qf10= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=?","Femenino",60).count    
    tf=qf1+qf2+qf3+qf4+qf5+qf6+qf7+qf8+qf9+qf10

    #qm1= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",5,9 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm1= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",5,9).count
    #qm2= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",10,11 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm2= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",10,11).count
    #qm3= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm3= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",12,14).count
    #qm4= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm4= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",15,17).count
    #qm5= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",18,19 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm5= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",18,19).count
    #qm6= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",20,29 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm6= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",20,29).count
    #qm7= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",30,34 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm7= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",30,34).count
    #qm8= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",35,49 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm8= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",35,49).count
    #qm9= Patient.where("sexo =? AND edad >=? AND edad<=?","Masculino",50,59 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm9= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=? AND edad<=?","Masculino",50,59).count
    #qm10= Patient.where("sexo =? AND edad >=?","Masculino",60 ).joins(:meetings).where("termino = ? AND psychologist_id=? AND start_time BETWEEN ? AND ? ",2,tmp,fechaIni,fechaFin).includes(:patient_counts).where("aasm_state = ?", "primera").count
    qm10= PatientCount.where("psychologist_id=? AND fecha BETWEEN ? AND ? AND aasm_status=?",tmp,tmp2,tmp3, "primera").joins(:patient).where("sexo =? AND edad >=?","Masculino",60).count
    tm=qm1+qm2+qm3+qm4+qm5+qm6+qm7+qm8+qm9+qm10
    ##Meeting.joins(:patient).where(:psychologist_id=>params[tmp], :fecha=>(params[:date_start]..params[:date_end]),(date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31")).includes(patient_counts)
    ConsultasPv.create(h:tm, m:tf, fecha:tmp3, psychologist_id:tmp, h1:qm1, h2:qm2, h3:qm3, h4:qm4, h5:qm5, h6:qm6, h7:qm7, h8:qm8, h9:qm9, h10:qm10, m1:qf1, m2:qf2, m3:qf3, m4:qf4, m5:qf5, m6:qf6, m7:qf7, m8:qf8, m9:qf9, m10:qf10)
  end
end
