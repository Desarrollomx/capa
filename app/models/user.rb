class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :meetings
  belongs_to :psychologist
  include PermissionsConcern

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
