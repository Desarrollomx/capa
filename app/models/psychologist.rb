class Psychologist < ActiveRecord::Base
	before_save :upcase
	has_many :personas_vgs
	has_many :capacitaciones_recs
	has_many :capacitaciones_ots
	has_many :personal_cts
	has_many :consultas_subs
	has_many :adolescentes_its
	has_many :tratamientos_bcs
	has_many :consultas_pvs
	has_many :adolescentes_aps
	has_many :tamizajes_tots
	#has_many :meta
  	has_many :aplicacion_tamizajes
	has_many :patient_counts
	has_many :expedientes
	has_many :meetings
	has_many :user
	has_many :beneficiados
	has_many :multyproms

	validates :nombre, presence: true
  #lengh => tamañano del contenido (minimo 20 car.)
  validates :apellidoP, presence: true
  validates :apellidoM, presence: true
  validates :telefono, presence: true

	def upcase
		self.nombre.upcase!
		self.apellidoP.upcase!
		self.apellidoM.upcase!
	end
end
