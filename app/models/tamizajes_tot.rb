class TamizajesTot < ActiveRecord::Base
	belongs_to :psychologist
	before_save :default_values
	before_save :suma

    validates :psychologist_id, :fecha, :presence=> true

	def default_values
		self.h ||=0
		self.h3 ||=0
		self.h4 ||=0
		self.h5 ||=0
		self.h6 ||=0
		self.m ||=0
		self.m3 ||=0
		self.m4 ||=0
		self.m5 ||=0
		self.m6 ||=0
	end

	def suma
		self.h= self.h3+self.h4+self.h5+self.h6
		self.m= self.m3+self.m4+self.m5+self.m6
	end

	def self.query(psy, id)
		tmp=psy
		tmp1=id
		tmp2=" "
		tmp3=" "
		if tmp1.eql?(1)
			date = Date.today
    		año= date.strftime"%Y".to_s
    		mes= date.strftime"%m".to_s
    		tmp2=año+"-"+mes+"-"+"01"
    		tmp3=año+"-"+mes+"-"+"19"
		elsif tmp1.eql?(2)
			date = Date.today
    		año= date.strftime"%Y".to_s
    		mes=(((date.strftime"%m").to_i)-1).to_s
    		tmp2=año+"-"+mes+"-"+"20"
    		tmp3=año+"-"+mes+"-"+"31"
		end	
		
		#AplicacionTamizaje.where(:psychologist_id=>1).includes(:tamizajes).pluck(:sexo, :edad, :created_at)
		consulta=AplicacionTamizaje.where("psychologist_id=?",tmp).select(:id)
		apltmz_id=consulta.map {|e| e[:id]}
		$tf=0
		$qf3=0
		$qf4=0
		$qf5=0
		$qf6=0
		$tm=0
		$qm3=0
		$qm4=0
		$qm5=0
		$qm6=0
		apltmz_id.each do |item|
			$qf3+=Tamizaje.where("aplicacion_tamizaje_id =? AND sexo =? AND edad >=? AND edad<=? AND created_at BETWEEN ? AND ? ", item,"F",12,14,tmp2,tmp3).count
		 	$qf4+=Tamizaje.where("aplicacion_tamizaje_id =? AND sexo =? AND edad >=? AND edad<=? AND created_at BETWEEN ? AND ? ", item,"F",15,17,tmp2,tmp3).count
		 	$qf5+=Tamizaje.where("aplicacion_tamizaje_id =? AND sexo =? AND edad >=? AND edad<=? AND created_at BETWEEN ? AND ? ", item,"F",18,19,tmp2,tmp3).count
		 	$qf6+=Tamizaje.where("aplicacion_tamizaje_id =? AND sexo =? AND edad >=? AND edad<=? AND created_at BETWEEN ? AND ? ", item,"F",20,25,tmp2,tmp3).count
		 	$tf=($qf3+$qf4+$qf5+$qf6)
			$qm3+=Tamizaje.where("aplicacion_tamizaje_id =? AND sexo =? AND edad >=? AND edad<=? AND created_at BETWEEN ? AND ? ", item,"M",12,14,tmp2,tmp3).count
		 	$qm4+=Tamizaje.where("aplicacion_tamizaje_id =? AND sexo =? AND edad >=? AND edad<=? AND created_at BETWEEN ? AND ? ", item,"M",15,17,tmp2,tmp3).count
		 	$qm5+=Tamizaje.where("aplicacion_tamizaje_id =? AND sexo =? AND edad >=? AND edad<=? AND created_at BETWEEN ? AND ? ", item,"M",18,19,tmp2,tmp3).count
		 	$qm6+=Tamizaje.where("aplicacion_tamizaje_id =? AND sexo =? AND edad >=? AND edad<=? AND created_at BETWEEN ? AND ? ", item,"M",20,25,tmp2,tmp3).count
		 	$tm=($qm3+$qm4+$qm5+$qm6)
		end
		TamizajesTot.create(h:$tm, m:$tf, fecha:tmp3, psychologist_id:tmp, h3:$qm3, h4:$qm4, h5:$qm5, h6:$qm6, m3:$qf3, m4:$qf4, m5:$qf5, m6:$qf6)
	end
end
