class Riesgo < ActiveRecord::Base
	before_save :upcase
	has_one :expediente
	scope :ultimos, -> { order("created_at DESC").limit(5)}

	def upcase
    self.riesgo.upcase!
		self.clave.upcase!
	end
end
