class AdolescentesApsController < ApplicationController
  before_action :set_adolescentes_ap, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /adolescentes_aps
  # GET /adolescentes_aps.json
  def index
    @adolescentes_aps = AdolescentesAp.all
    #@consul1=Patient.find_by_h(params[])Meeting.where(:termino=>params['2'], =>params[:start_time])
  end

  # GET /adolescentes_aps/1
  # GET /adolescentes_aps/1.json
  def show
  end

  # GET /adolescentes_aps/new
  def new
    @adolescentes_ap = AdolescentesAp.new
    date = Date.today

    @adoltot = Meeting.joins(:patient).where("start_time >= ? AND start_time <= ? AND termino = ?",(date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,2)
    #@adoltot = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Femenino").count

  end

  # GET /adolescentes_aps/1/edit
  def edit
  end

  # POST /adolescentes_aps
  # POST /adolescentes_aps.json
  def create
    @adolescentes_ap = AdolescentesAp.new(adolescentes_ap_params)

    respond_to do |format|
      if @adolescentes_ap.save
        format.html { redirect_to @adolescentes_ap, notice: 'Adolescentes ap was successfully created.' }
        format.json { render :show, status: :created, location: @adolescentes_ap }
      else
        format.html { render :new }
        format.json { render json: @adolescentes_ap.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adolescentes_aps/1
  # PATCH/PUT /adolescentes_aps/1.json
  def update
    respond_to do |format|
      if @adolescentes_ap.update(adolescentes_ap_params)
        format.html { redirect_to @adolescentes_ap, notice: 'Adolescentes ap was successfully updated.' }
        format.json { render :show, status: :ok, location: @adolescentes_ap }
      else
        format.html { render :edit }
        format.json { render json: @adolescentes_ap.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adolescentes_aps/1
  # DELETE /adolescentes_aps/1.json
  def destroy
    @adolescentes_ap.destroy
    respond_to do |format|
      format.html { redirect_to adolescentes_aps_url, notice: 'Adolescentes ap was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adolescentes_ap
      @adolescentes_ap = AdolescentesAp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adolescentes_ap_params
      params.require(:adolescentes_ap).permit(:h, :m, :fecha, :psychologist_id, :h3, :h4, :m3, :m4)
    end
end
