class CapacitacionesOtsController < ApplicationController
  before_action :set_capacitaciones_ot, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :authenticate_psicologo!,only: [:create,:show,:new]

  # GET /capacitaciones_ots
  # GET /capacitaciones_ots.json
  def index
    @capacitaciones_ots = CapacitacionesOt.all
  end

  # GET /capacitaciones_ots/1
  # GET /capacitaciones_ots/1.json
  def show

  end

  # GET /capacitaciones_ots/new
  def new
    @capacitaciones_ot = CapacitacionesOt.new
  end

  # GET /capacitaciones_ots/1/edit
  def edit
  end

  # POST /capacitaciones_ots
  # POST /capacitaciones_ots.json
  def create
    @capacitaciones_ot = CapacitacionesOt.new(capacitaciones_ot_params)

    respond_to do |format|
      if @capacitaciones_ot.save
        format.html { redirect_to @capacitaciones_ot, notice: 'Capacitaciones ot was successfully created.' }
        format.json { render :show, status: :created, location: @capacitaciones_ot }
      else
        format.html { render :new }
        format.json { render json: @capacitaciones_ot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /capacitaciones_ots/1
  # PATCH/PUT /capacitaciones_ots/1.json
  def update
    respond_to do |format|
      if @capacitaciones_ot.update(capacitaciones_ot_params)
        format.html { redirect_to @capacitaciones_ot, notice: 'Capacitaciones ot was successfully updated.' }
        format.json { render :show, status: :ok, location: @capacitaciones_ot }
      else
        format.html { render :edit }
        format.json { render json: @capacitaciones_ot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /capacitaciones_ots/1
  # DELETE /capacitaciones_ots/1.json
  def destroy
    @capacitaciones_ot.destroy
    respond_to do |format|
      format.html { redirect_to capacitaciones_ots_url, notice: 'Capacitaciones ot was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_capacitaciones_ot
      @capacitaciones_ot = CapacitacionesOt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def capacitaciones_ot_params
      params.require(:capacitaciones_ot).permit(:h, :m, :fecha, :psychologist_id, :h1, :h2, :h3, :h4, :h5, :h6, :h7, :h8, :h9, :h10, :m1, :m2, :m3, :m4, :m5, :m6, :m7, :m8, :m9 ,:m10)
    end
end
