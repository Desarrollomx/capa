class BeneficiadosController < ApplicationController
  before_action :set_beneficiado, only: [:show, :edit, :update, :destroy]

  # GET /beneficiados
  # GET /beneficiados.json
  def index
    @beneficiados = Beneficiado.al
    
  end

  # GET /beneficiados/1
  # GET /beneficiados/1.json
  def show
  end

  # GET /beneficiados/new
  def new
    @beneficiado = Beneficiado.new
  end

  # GET /beneficiados/1/edit
  def edit
  end

  # POST /beneficiados
  # POST /beneficiados.json
  def create
    @beneficiado = Beneficiado.new(beneficiado_params)

    respond_to do |format|
      if @beneficiado.save
        format.html { redirect_to @beneficiado, notice: 'Beneficiado was successfully created.' }
        format.json { render :show, status: :created, location: @beneficiado }
      else
        format.html { render :new }
        format.json { render json: @beneficiado.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /beneficiados/1
  # PATCH/PUT /beneficiados/1.json
  def update
    respond_to do |format|
      if @beneficiado.update(beneficiado_params)
        format.html { redirect_to @beneficiado, notice: 'Beneficiado was successfully updated.' }
        format.json { render :show, status: :ok, location: @beneficiado }
      else
        format.html { render :edit }
        format.json { render json: @beneficiado.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /beneficiados/1
  # DELETE /beneficiados/1.json
  def destroy
    @beneficiado.destroy
    respond_to do |format|
      format.html { redirect_to beneficiados_url, notice: 'Beneficiado was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_beneficiado
      @beneficiado = Beneficiado.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def beneficiado_params
      params.require(:beneficiado).permit(:h, :m, :fecha, :h1, :h2, :h3, :h4, :h5, :h6, :h7, :h8, :h9, :h10, :m1, :m2, :m3, :m4, :m5, :m6, :m7, :m8, :m9, :m10, :psychologist_id)
    end
end
