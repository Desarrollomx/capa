class SicecaController < ApplicationController
	before_action :set_siceca, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!
  
  def index

    @id = Psychologist.find(current_user.psychologist_id)
    @fi=params[:date_start]
    @ff=params[:date_end]    
  #$tls=Array.new
  #$tls2=Array.new
  $rs1=Array.new
  #r = Patient.joins(:meetings).where("meetings.termino = ? AND meetings.psychologist_id =? AND meetings.start_time BETWEEN '2016-03-01' AND '2016-06-30'", 2, 6 ).includes(:municipio).pluck(:municipio, :sexo, :edad)
  query = Patient.joins(:meetings).where("meetings.psychologist_id = ? AND meetings.termino = '2' AND meetings.start_time BETWEEN ? AND ?", current_user.psychologist_id, params[:date_start], params[:date_end]).includes(:municipio).pluck(:municipio, :sexo, :edad)
  
  @aux=query.sort

  @mun= Array.new 
  $tmp1= Array.new 
  $tmp2= Array.new 
  $tmp3= Array.new 

  @municipio=[]

  #$rs.clear
  #$tls.clear
  #$tls2.clear
  #limpiar_variables_2
  @aux.each do |e|
    @mun<<e.fetch(0)
  end

  @aux.each do |e|
    $tmp1=e.fetch(0)
    $tmp2=e.fetch(1)
    $tmp3=e.fetch(2)
    mat_resultados($tmp1, $tmp2, $tmp3)#, @mun)
    #genero($temp2, $tmp3)
  end  
  @municipio=$rs1 
  $rs1=[] 




  
  limpiar_variables

  query2 = Patient.joins(:meetings).where("meetings.psychologist_id = ? AND meetings.termino = '2' AND meetings.start_time BETWEEN ? AND ?", current_user.psychologist_id, params[:date_start], params[:date_end]).pluck(:sexo, :edad) 
  @aux2=query2
  $tmp4= 0
  $tmp5= 0 
  @result= Array.new 
  limpiar_variables
  @aux2.each do |e|
    $tmp4=e.fetch(0)
    $tmp5=e.fetch(1)
    genero($tmp4, $tmp5)
    #@test=$tls
  end
  @result= total


  query3 = Patient.joins(:meetings).where("meetings.psychologist_id = ? AND meetings.termino = '2' AND meetings.start_time BETWEEN ? AND ?", current_user.psychologist_id, params[:date_start], params[:date_end]).pluck(:psychologist_id,:sexo, :edad)
  @aux3=query3
  @result2= Array.new
  @tmp6=0
  $tmp7=0
  $tmp8=0
  limpiar_variables
  @aux3.each do |e|
    item= e
    $tmp6=item.fetch(0)
    $tmp7=item.fetch(1)
    $tmp8=item.fetch(2)  
    genero($tmp7, $tmp8)
  end
  #@n= ""
  @n= psicologo($tmp6.to_s)
  @result2= total
    

  query4= Patient.joins(:meetings).where("meetings.psychologist_id = ? AND meetings.termino = '2' AND meetings.start_time BETWEEN ? AND ?", current_user.psychologist_id, params[:date_start], params[:date_end]).includes(:expediente).pluck(:programa_id,:sexo, :edad)
  @aux4=query4.sort

  @tempo=Array.new()
  @tempo1=Array.new()
  @tempo2=Array.new()
  @tempo3=Array.new()
  @tempo4=Array.new() 

  @aux4.each do |e|
    @tempo1<<e.fetch(0)
    @tempo2<<e.fetch(1)
    @tempo3<<e.fetch(2)
  end

  @tempo1.each do|e|
    query7=Programa.where(id: e).select(:programa)
    @tempo4<<query7.map {|e| e[:programa]}.pop
  end

    @tempo= @tempo4.zip(@tempo2,@tempo3)

  @prog=[]

  #$rs.clear
  #$tls.clear
  #$tls2.clear
  #limpiar_variables_2

  @tempo.each do |e|
    $tempo=e.fetch(0)
    $tempo2=e.fetch(1)
    $tempo3=e.fetch(2)
    mat_resultados($tempo, $tempo2, $tempo3)#, @tempo4)
    #genero($temp2, $tmp3)
  end  
  @prog=$rs1 
  $rs1=[] 

  query5= Patient.joins(:meetings).where("meetings.psychologist_id = ? AND meetings.termino = '2' AND meetings.start_time BETWEEN ? AND ?", current_user.psychologist_id, params[:date_start], params[:date_end]).includes(:expediente).pluck(:sustancium_id,:sexo, :edad)
  #sust = query5.map { |e| e[:sustancium_id] }
  #Patient.joins(:meetings).where("meetings.psychologist_id = ? AND meetings.termino = '2' AND meetings.start_time BETWEEN ? AND ?", 4, '2016-03-01', '2016-06-31').includes(:expediente).pluck(:sustancium_id, :sexo, :edad)
  @aux5=query5.sort
  
  #@result5= Array.new
  @temp=Array.new()
  @temp1=Array.new()
  @temp2=Array.new()
  @temp3=Array.new()
  @temp4=Array.new()
  @temp5=Array.new()
  #@temp3=Array.new()
  
  #$sus= Array.new
  #limpiar_variables
  @aux5.each do |e|
    @temp1<<e.fetch(0)
    @temp2<<e.fetch(1)
    @temp3<<e.fetch(2)
  end

  #query6= Sustancium.where(id: @temp1).select(:sustancia)
  #@temp4 = query6.map {|e| e[:sustancia]}
  @temp1.each do|e|
    query6=Sustancium.where(id: e).select(:clave)
    @temp4<<query6.map {|e| e[:clave]}.pop
  end

    @temp= @temp4.zip(@temp2,@temp3)
  

  #$rs=Array.new() 
  #$t3=[]
  #$t4=[]
  
  @sus=[]

  #limpiar_variables_2
  
  #$rs.clear
  @temp.each do |e|
    $temp=e.fetch(0)
    $temp2=e.fetch(1)
    $temp3=e.fetch(2) 
    mat_resultados($temp, $temp2, $temp3)#, @temp4)
    #genero($temp2, $tmp3)
  end  
    #@test=total
  @sus=$rs1
  $rs1=[]

  respond_to do |format|
    format.html
    format.pdf do
      render pdf:"Siceca",
      #header: { center: 'hola' },
      #footer: { center: 'hola' },
      header: {
          content: render_to_string(template: 'layouts/header.html.pdf.erb')
        },
        footer: {
          content: render_to_string(layout: 'layouts/footer.html.pdf.erb')
        },
      layout: 'layouts/pdf.html.erb',
      page_size: 'letter',
      file:'siceca/index.pdf.erb'
    end
  end 

  end

  def limpiar_variables 
    @n=""
    $eh1=0 
    $eh2=0 
    $eh3=0
    $eh4=0 
    $eh5=0 
    $eh6=0
    $eh7=0 
    $eh8=0 
    $eh9=0
    $eh10=0
    $em1=0 
    $em2=0 
    $em3=0
    $em4=0 
    $em5=0 
    $em6=0
    $em7=0 
    $em8=0 
    $em9=0
    $em10=0
    
  end
  
  def genero(aux1, aux2)  
    tmp1= aux1.to_s
    tmp2= aux2.to_i
    if tmp1.eql?("Masculino")
      if tmp2 >= 5 and tmp2 <= 9
        $eh1+=1 
      elsif tmp2 >= 10 and  tmp2 <= 11
        $eh2+=1
      elsif tmp2 >= 12 and  tmp2 <= 14
        $eh3+=1
      elsif tmp2 >= 15 and  tmp2 <= 17
        $eh4+=1 
      elsif tmp2 >= 18 and  tmp2 <= 19
        $eh5+=1 
      elsif tmp2 >= 20 and  tmp2 <= 29
        $eh6+=1
      elsif tmp2>= 30 and  tmp2 <= 34
        $eh7+=1 
      elsif tmp2 >= 35 and  tmp2 <= 49
        $eh8+=1 
      elsif tmp2 >= 50 and  tmp2<= 59
        $eh9+=1
      elsif tmp2 >= 60
        $eh10+=1
      end
    elsif tmp1.eql?("Femenino")
      if tmp2 >= 5 and tmp2 <= 9
        $em1+=1 
      elsif tmp2 >= 10 and tmp2 <= 11
        $em2+=1
      elsif tmp2 >= 12 and tmp2 <= 14
        $em3+=1
      elsif tmp2 >= 15 and tmp2 <= 17
        $em4+=1 
      elsif tmp2 >= 18 and tmp2 <= 19
        $em5+=1 
      elsif tmp2 >= 20 and tmp2 <= 29
        $em6+=1
      elsif tmp2 >= 30 and tmp2 <= 34
        $em7+=1 
      elsif tmp2 >= 35 and tmp2 <= 49
        $em8+=1 
      elsif tmp2 >= 50 and tmp2 <= 59
        $em9+=1
      elsif  tmp2 >= 60
        $em10+=1
      end
    end
  end  

  def total
    $t=$eh1+$em1+$eh2+$em2+$eh3+$em3+$eh4+$em4+$eh5+$em5+$eh6+$em6+$eh7+$em7+$eh8+$em8+$eh9+$em9+$eh10+$em10
    res=[$eh1,$em1,$eh2,$em2,$eh3,$em3,$eh4,$em4,$eh5,$em5,$eh6,$em6,$eh7,$em7,$eh8,$em8,$eh9,$em9,$eh10,$em10,$t]
    return res
  end 

  def psicologo(aux)
    tmp=aux.to_s
    if tmp.eql?("1")
      psc= "Ariadne"
    elsif tmp.eql?("2")
      psc= "Esmeralda"
    elsif tmp.eql?("3")
      psc= "Manuel Adolfo"
    elsif tmp.eql?("4")
      psc= "Susana"
    elsif tmp.eql?("5")
      psc= "Humberto"
    elsif tmp.eql?("6")
      psc= "Celso Israel"
    end
    return psc
  end

  
  def mat_resultados(aux1, aux2, aux3) #mun; gen, edad
    tmp1=aux1.to_s #nombre de municipio a evaluar
    tmp2=aux2.to_s # sexo a evaluar
    tmp3=aux3.to_i # edad a evaluar
    #$rs1=[]
    band="false"
    tls1=[]
    tls2=[]
    tls3=[]

    if ($rs1.empty?)
      limpiar_variables
      genero(tmp2, tmp3)#clasifica los datos por sexo y edad
      tls1= total #has el conteo y guardalo en el vector de totales
      tls1.insert(0,tmp1) #inserta el nombre del municipio
      $rs1<<tls1
      tls1=[]
    else
      n=($rs1.length)-1
      for i in 0..n do #BUSCAR VECTOR CON LA VARIABLE TMP1 
        if $rs1.fetch(i).include?tmp1
          band="true"
          tls1=$rs1.fetch(i) 
          $rs1.delete_at(i)
          break
        end
      end
      if band.eql?"true" 
        limpiar_variables
        genero(tmp2, tmp3)#clasifica los datos por sexo y edad
        tls2= total #has el conteo y guardalo en el vector de totales
        tls2.insert(0,tmp1) #inserta el nombre del municipio
     
        aux1=String(tls1.fetch(0))#.to_s
        aux2=String(tls2.fetch(0))#.to_s
        if aux1.eql?aux2
          for i in 1..21 do
            aux3=Integer(tls1.fetch(i))
            aux4=Integer(tls2.fetch(i))
            aux5=aux3+aux4
            tls3<<aux5
          end
          tls3.insert(0,tmp1)
          $rs1<<tls3
          tls1=[]
          tls2=[]
          tls3=[]
        end
      else
        limpiar_variables
        genero(tmp2, tmp3)#clasifica los datos por sexo y edad
        tls1= total #has el conteo y guardalo en el vector de totales
        tls1.insert(0,tmp1) #inserta el nombre del municipio
        $rs1<<tls1
        tls1=[]
      end   
    end
    return $rs1
  end
  
  def show

  end

  def new

  end

  def edit

  end

  def create

  end

  def update

  end

  def destroy

  end

  def set_siceca

  end

  def siceca_params

  end

end
