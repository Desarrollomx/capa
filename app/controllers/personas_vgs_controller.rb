class PersonasVgsController < ApplicationController
  before_action :set_personas_vg, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /personas_vgs
  # GET /personas_vgs.json
  def index
    @personas_vgs = PersonasVg.all
  end

  # GET /personas_vgs/1
  # GET /personas_vgs/1.json
  def show
  end

  # GET /personas_vgs/new
  def new
    @personas_vg = PersonasVg.new
  end

  # GET /personas_vgs/1/edit
  def edit
  end

  # POST /personas_vgs
  # POST /personas_vgs.json
  def create
    @personas_vg = PersonasVg.new(personas_vg_params)

    respond_to do |format|
      if @personas_vg.save
        format.html { redirect_to @personas_vg, notice: 'Personas vg was successfully created.' }
        format.json { render :show, status: :created, location: @personas_vg }
      else
        format.html { render :new }
        format.json { render json: @personas_vg.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personas_vgs/1
  # PATCH/PUT /personas_vgs/1.json
  def update
    respond_to do |format|
      if @personas_vg.update(personas_vg_params)
        format.html { redirect_to @personas_vg, notice: 'Personas vg was successfully updated.' }
        format.json { render :show, status: :ok, location: @personas_vg }
      else
        format.html { render :edit }
        format.json { render json: @personas_vg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /personas_vgs/1
  # DELETE /personas_vgs/1.json
  def destroy
    @personas_vg.destroy
    respond_to do |format|
      format.html { redirect_to personas_vgs_url, notice: 'Personas vg was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_personas_vg
      @personas_vg = PersonasVg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def personas_vg_params
      params.require(:personas_vg).permit(:h, :m, :fecha, :psychologist_id, :h1, :h2, :h3, :h4, :h5, :h6, :h7, :h8, :h9, :h10, :m1, :m2, :m3, :m4, :m5, :m6, :m7, :m8, :m9 ,:m10)
    end
end
