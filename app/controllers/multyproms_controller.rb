class MultypromsController < ApplicationController
  before_action :set_multyprom, only: [:show, :edit, :update, :destroy]

  # GET /multyproms
  # GET /multyproms.json
  def index
    @multyproms = Multyprom.all
  end

  # GET /multyproms/1
  # GET /multyproms/1.json
  def show
  end

  # GET /multyproms/new
  def new
    @multyprom = Multyprom.new
  end

  # GET /multyproms/1/edit
  def edit
  end

  # POST /multyproms
  # POST /multyproms.json
  def create
    @multyprom = Multyprom.new(multyprom_params)

    respond_to do |format|
      if @multyprom.save
        format.html { redirect_to @multyprom, notice: 'Multyprom was successfully created.' }
        format.json { render :show, status: :created, location: @multyprom }
      else
        format.html { render :new }
        format.json { render json: @multyprom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /multyproms/1
  # PATCH/PUT /multyproms/1.json
  def update
    respond_to do |format|
      if @multyprom.update(multyprom_params)
        format.html { redirect_to @multyprom, notice: 'Multyprom was successfully updated.' }
        format.json { render :show, status: :ok, location: @multyprom }
      else
        format.html { render :edit }
        format.json { render json: @multyprom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /multyproms/1
  # DELETE /multyproms/1.json
  def destroy
    @multyprom.destroy
    respond_to do |format|
      format.html { redirect_to multyproms_url, notice: 'Multyprom was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_multyprom
      @multyprom = Multyprom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def multyprom_params
      params.require(:multyprom).permit(:h, :m, :fecha, :h1, :h2, :h3, :h4, :h5, :h6, :h7, :h8, :h9, :h10, :m1, :m2, :m3, :m4, :m5, :m6, :m7, :m8, :m9, :m10, :psychologist_id)
    end
end
