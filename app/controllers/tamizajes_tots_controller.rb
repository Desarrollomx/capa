class TamizajesTotsController < ApplicationController
  before_action :set_tamizajes_tot, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /tamizajes_tots
  # GET /tamizajes_tots.json
  def index
    @tamizajes_tots = TamizajesTot.all
  end

  # GET /tamizajes_tots/1
  # GET /tamizajes_tots/1.json
  def show
  end

  # GET /tamizajes_tots/new
  def new
    @tamizajes_tot = TamizajesTot.new

  
    

    #@h=0
    #@m=0
    #@busq= @count.map {|m| m[:patient_id]}
    #  @sexo = Patient.find(@busq)
    #  @busq_sex = @sexo.map {|s| s[:sexo]}
    #  @busq_sex.each do |b|
    #    if (b == "Femenino")
    #      @m+=1
    #    elsif(b == "Masculino")
    #      @h+=1
    #  end
    #end
  end

  #def procesa_datos(psic)
    #date = Date.today
    #result=[]
    #mtot = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Femenino").count
    #m12a14 = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ? AND edad >= ? AND edad <= ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Femenino",12,14).count
    #m15a17 = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ? AND edad >= ? AND edad <= ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Femenino",15,17).count
    #m18a19 = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ? AND edad >= ? AND edad <= ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Femenino",18,19).count
    #m20a25 = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ? AND edad >= ? AND edad <= ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Femenino",20,25).count
    #htot = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Masculino").count    
    #h12a14 = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ? AND edad >= ? AND edad <= ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Masculino",12,14).count
    #h15a17 = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ? AND edad <= ? AND edad <= ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Masculino",15,17).count
    #h18a20 = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ? AND edad <= ? AND edad <= ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Masculino",18,19).count
    #h20a25 = Tamizaje.where("created_at >= ? AND created_at <= ? AND sexo = ? AND edad <= ? AND edad <= ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,"Masculino",20,25).count
    #result[mtot,m12a14,m15a17,m18a19,m20a25,htot,h12a14,h15a17,h18a20,h20a25]
    #return result
  #end

  def guardar_info
    params[]
  end

  # GET /tamizajes_tots/1/edit
  def edit
  end

  # POST /tamizajes_tots
  # POST /tamizajes_tots.json
  def create
    @tamizajes_tot = TamizajesTot.new(tamizajes_tot_params)

    respond_to do |format|
      if @tamizajes_tot.save
        format.html { redirect_to @tamizajes_tot, notice: 'Tamizajes tot was successfully created.' }
        format.json { render :show, status: :created, location: @tamizajes_tot }
      else
        format.html { render :new }
        format.json { render json: @tamizajes_tot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tamizajes_tots/1
  # PATCH/PUT /tamizajes_tots/1.json
  def update
    respond_to do |format|
      if @tamizajes_tot.update(tamizajes_tot_params)
        format.html { redirect_to @tamizajes_tot, notice: 'Tamizajes tot was successfully updated.' }
        format.json { render :show, status: :ok, location: @tamizajes_tot }
      else
        format.html { render :edit }
        format.json { render json: @tamizajes_tot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tamizajes_tots/1
  # DELETE /tamizajes_tots/1.json
  def destroy
    @tamizajes_tot.destroy
    respond_to do |format|
      format.html { redirect_to tamizajes_tots_url, notice: 'Tamizajes tot was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tamizajes_tot
      @tamizajes_tot = TamizajesTot.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tamizajes_tot_params
      params.require(:tamizajes_tot).permit(:h, :m, :fecha, :psychologist_id, :h3,:h4,:h5,:h6,:m3,:m4,:m5,:m6)
    end
end
