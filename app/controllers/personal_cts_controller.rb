class PersonalCtsController < ApplicationController
  before_action :set_personal_ct, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /personal_cts
  # GET /personal_cts.json
  def index
    @personal_cts = PersonalCt.all
  end

  # GET /personal_cts/1
  # GET /personal_cts/1.json
  def show
  end

  # GET /personal_cts/new
  def new
    @personal_ct = PersonalCt.new
  end

  # GET /personal_cts/1/edit
  def edit
  end

  # POST /personal_cts
  # POST /personal_cts.json
  def create
    @personal_ct = PersonalCt.new(personal_ct_params)

    respond_to do |format|
      if @personal_ct.save
        format.html { redirect_to @personal_ct, notice: 'Personal ct was successfully created.' }
        format.json { render :show, status: :created, location: @personal_ct }
      else
        format.html { render :new }
        format.json { render json: @personal_ct.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /personal_cts/1
  # PATCH/PUT /personal_cts/1.json
  def update
    respond_to do |format|
      if @personal_ct.update(personal_ct_params)
        format.html { redirect_to @personal_ct, notice: 'Personal ct was successfully updated.' }
        format.json { render :show, status: :ok, location: @personal_ct }
      else
        format.html { render :edit }
        format.json { render json: @personal_ct.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /personal_cts/1
  # DELETE /personal_cts/1.json
  def destroy
    @personal_ct.destroy
    respond_to do |format|
      format.html { redirect_to personal_cts_url, notice: 'Personal ct was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_personal_ct
      @personal_ct = PersonalCt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def personal_ct_params
      params.require(:personal_ct).permit(:h, :m, :fecha, :psychologist_id, :h1, :h2, :h3, :h4, :h5, :h6, :h7, :h8, :h9, :h10, :m1, :m2, :m3, :m4, :m5, :m6, :m7, :m8, :m9 ,:m10)
    end
end
