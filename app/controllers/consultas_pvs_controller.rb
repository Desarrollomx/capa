class ConsultasPvsController < ApplicationController
  before_action :set_consultas_pv, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /consultas_pvs
  # GET /consultas_pvs.json
  def index
    @consultas_pvs = ConsultasPv.all
  end

  # GET /consultas_pvs/1
  # GET /consultas_pvs/1.json
  def show
  end

  # GET /consultas_pvs/new
  def new
    @consultas_pv = ConsultasPv.new





    #date = Date.today
    #@count = Meeting.where("start_time >= ? AND start_time <= ? AND user_id = ? AND termino = ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,1, 2)#.count
    #@h=0
    #@m=0
    #@busq= @count.map {|m| m[:patient_id]}
      #@sexo = Patient.find(@busq)
      #@busq_sex = @sexo.map {|s| s[:sexo]}
      #@busq_sex.each do |b|
        #if (b == "Femenino")
          #@m+=1
        #elsif(b == "Masculino")
          #@h+=1
        #end
      #end
  end
  
  
  #def query(psic)
    #tmp= psic
    #date = Date.today
    #Meeting.joins(:patient).where(:psychologist_id=>params[tmp], :fecha=>(params[:date_start]..params[:date_end]),(date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31")  )
  #end

  #def guardar_info
    #params.require(:consultas_pv).permit(:h, :m, :fecha, :psychologist_id, :h1,:h2,:h3,:h4,:h5,:h6,:h7,:h8,:h8,:h10,:m1,:m2,:m3,:m4,:m5,:m6,:m7,:m8,:m9, :m10
  #end

  # GET /consultas_pvs/1/edit
  def edit
  end

  # POST /consultas_pvs
  # POST /consultas_pvs.json
  def create
    @consultas_pv = ConsultasPv.new(consultas_pv_params)

    respond_to do |format|
      if @consultas_pv.save
        format.html { redirect_to @consultas_pv, notice: 'Consultas pv was successfully created.' }
        format.json { render :show, status: :created, location: @consultas_pv }
      else
        format.html { render :new }
        format.json { render json: @consultas_pv.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /consultas_pvs/1
  # PATCH/PUT /consultas_pvs/1.json
  def update
    respond_to do |format|
      if @consultas_pv.update(consultas_pv_params)
        format.html { redirect_to @consultas_pv, notice: 'Consultas pv was successfully updated.' }
        format.json { render :show, status: :ok, location: @consultas_pv }
      else
        format.html { render :edit }
        format.json { render json: @consultas_pv.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /consultas_pvs/1
  # DELETE /consultas_pvs/1.json
  def destroy
    @consultas_pv.destroy
    respond_to do |format|
      format.html { redirect_to consultas_pvs_url, notice: 'Consultas pv was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_consultas_pv
      @consultas_pv = ConsultasPv.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def consultas_pv_params
      params.require(:consultas_pv).permit(:h,:m,:fecha,:psychologist_id,:h1,:h2,:h3,:h4,:h5,:h6,:h7,:h8,:h8,:h10,:m1,:m2,:m3,:m4,:m5,:m6,:m7,:m8,:m9,:m10
)
    end


end
