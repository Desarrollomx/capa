class PatientsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :authenticate_psicologo!,only: [:index,:create,:edit,:destroy,:update,:show,:new]


  # GET /patients
  # GET /patients.json
  def index
    @patients = Patient.ultimos

    if ((params[:search_id].present?) and (params[:search].present?))
      @id= params[:search_id]
      @id=Integer(@id.shift)
      @search=params[:search]
      if @id.eql?(1)
        #excepciones para prevenir que colapse la pag. cuando hay error en los parametros de busqueda
        begin
          #@total= Patient.all.count
          @search=Integer(@search)
          @resultado=Patient.find(@search)
        rescue ActiveRecord::RecordNotFound => e
            #render(:text => "Parece que quiere hablar de")
            flash[:notice] =  "el id del paciente no existe"
        rescue ArgumentError => e
          flash[:notice] =  "parametro de busqueda incorrecto"
        rescue => e
          flash[:notice] =  "dato invalido"
        end

      elsif @id.eql?(2)
        #excepciones para prevenir que colapse la pag. cuando hay error en los parametros de busqueda
        begin
          @search=String(@search)
          @resultado=Patient.find_by_nom(@search)
        rescue => e
          flash[:notice] =  "paciente no encontrado"
        end

      elsif @id.eql?(3)
        begin
          @search=String(@search)
          @resultado= Patient.find_by_app(@search)
        rescue  => e
          flash[:notice] =  "paciente no encontrado"
        end

      elsif @id.eql?(4)
        begin
        @search=String(@search)
        @resultado= Patient.find_by_apm(@search)
        rescue  => e
          flash[:notice] =  "paciente no encontrado"
        end
      end
    end
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    @expediente = Expediente.find_by_patient_id(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        render pdf:"PacienteID-#{@patient.id}-#{@patient.nom+@patient.app+@patient.apm}",
        #header: { center: 'hola' },
        #footer: { center: 'hola' },
        header: {
          content: render_to_string(template: 'layouts/header.html.pdf.erb')
        },
        footer: {
          content: render_to_string(layout: 'layouts/footer.html.pdf.erb')
        },
        layout: 'layouts/pdf.html.erb',
        page_size: 'letter',
        file:'patients/show.pdf.erb'
      end
    end
  end


  # GET /patients/new
  def new
    @patient = Patient.new
  end

  # GET /patients/1/edit
  def edit
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(patient_params)

    respond_to do |format|
      if @patient.save
        format.html { redirect_to @patient, notice: 'Paciente agregado satisfactoriamnete, Favor de agregar al Familiar Respondable.' }
        format.json { render :show, status: :created, location: @patient }
      else
        format.html { render :new }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    respond_to do |format|
      if @patient.update(patient_params)
        format.html { redirect_to @patient, notice: 'Paciente actulizado satisfactoriamnete.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :edit }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Paciente eliminado.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_params
      params.require(:patient).permit(:nom, :app, :apm, :sexo, :tel, :edad, :calle, :col,
      :aasm_state, :state_id, :municipio_id,:etnia_id,:fecha_nacimiento,:civil_id)
    end
end
