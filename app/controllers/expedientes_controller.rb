class ExpedientesController < ApplicationController
  before_action :set_expediente, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :authenticate_psicologo!,only: [:index,:create,:edit,:destroy,:update,:show,:new]

  # GET /expedientes
  # GET /expedientes.json
  def index
    @expedientes = Expediente.ultimos

    @expediente = Expediente.where("id_expediente = ?", params[:id_expediente])
    #if params[:id].present?
    #  @expediente = @expediente.where("id = ?", params[:id])
    #end
  end

  # GET /expedientes/1
  # GET /expedientes/1.json
  def show
    @expedientes = Expediente.all
    @paciente = Patient.find(@expediente.patient_id)
    @psychologist = Psychologist.find(@expediente.psychologist_id)
    @sustancia = Sustancium.find(@expediente.sustancium_id)
    @programa = Programa.find(@expediente.programa_id)
    @riesgo = Riesgo.find(@expediente.riesgo_id)

  end

  # GET /expedientes/new
  def new
    @expediente = Expediente.new
    @id = params[:param]
  end

  # GET /expedientes/1/edit
  def edit
  end

  # POST /expedientes
  # POST /expedientes.json
  def create
    @expediente = Expediente.new(expediente_params)
    @patient = @expediente.patient_id

    respond_to do |format|
      if @expediente.save
        format.html { redirect_to patient_path(@patient), notice: 'Expediente creado exitosamente.' }
        format.json { render :show, status: :created, location: @expediente }
      else
        format.html { render new_expediente_path(:param => @id) }
        format.json { render json: @expediente.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /expedientes/1
  # PATCH/PUT /expedientes/1.json
  def update
    respond_to do |format|
      if @expediente.update(expediente_params)
        format.html { redirect_to @expediente, notice: 'Expediente actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @expediente }
      else
        format.html { render :edit }
        format.json { render json: @expediente.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /expedientes/1
  # DELETE /expedientes/1.json
  def destroy
    @expediente.destroy
    respond_to do |format|
      format.html { redirect_to expedientes_url, notice: 'Expediente eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_expediente
      @expediente = Expediente.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def expediente_params
      params.require(:expediente).permit(:id_expediente, :pago, :patient_id, :psychologist_id, :programa_id, :sustancium_id, :riesgo_id)

    end
end
