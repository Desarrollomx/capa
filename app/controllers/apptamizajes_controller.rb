class ApptamizajesController < ApplicationController
  before_action :set_escuela, only: [:show]    
  # POST /tamizajes
  # POST /tamizajes.json
    
  def create2
      @escuela = AplicacionTamizaje.new(escuela_params)
      #current_user.psychologist_id
    
    respond_to do |format|
        if request.post?
            @escuela.psychologist_id = current_user.psychologist_id
            @escuela.status = "0"
            #Tambien @tamizaje[:id_escuela]
        end        
        if @escuela.save
            format.html { redirect_to tamizajes_path, notice: 'Escuela creada.' }
            format.json { render :new, status: :created, location: @escuela }
        else
            format.html { render '/tamizajes/index'}
            format.json { render json: @escuela.errors, status: :unprocessable_entity }
      end
    end
  end    

    
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_escuela
        @escuela = Tamizaje.find_by_aplicacion_tamizaje_id(params[:aplicacion_tamizaje_id])
    end    
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def escuela_params
        params.require(:aplicacion_tamizaje).permit(:escuela, :colonia, :municipio_id, :nivel_estudio_id, :psychologist_id, :status, :grupos_escuelas_attributes =>[:grupos, :aplicacion_tamizaje_id])
    end        
 
    
    
end
