class MeetingsController < ApplicationController
  before_action :set_meeting, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  #after_action :actualizar, only: [:actualizar]
  # GET /meetings
  # GET /meetings.json
  def index
    @meetings = Meeting.all
    #redirect_to root_path
  end

  def actualizar
    Meeting.update(params[:id],:termino => 2)
    Expediente.update_counters Meeting.find(params[:id]).patient.expediente.id, :sesion => 1
    guardar
    if(Meeting.find(params[:id]).patient.expediente.sesion == 1)
      Meeting.find(params[:id]).patient.primer_consulta!
    end
    if(Meeting.find(params[:id]).patient.expediente.sesion == Meeting.find(params[:id]).patient.expediente.programa.sesiones)
      Meeting.find(params[:id]).patient.termino_programa!
      if Meeting.find(params[:id]).patient.alta?
        guardar
      end
    end
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Consulta Terminada, no olvide la proxima consulta.' }
    end
  end

  # GET /meetings/1
  # GET /meetings/1.json
  def show
    @meetings = Meeting.all
    @user = User.find(@meeting.user_id)
    @psychologist = Psychologist.find(@user.psychologist_id)
    @patient = Patient.find(@meeting.patient_id)
  end

  # GET /meetings/new
  def new
    @meeting = Meeting.new
    @meetings = Meeting.all
    @usuario = User.all
  end

  # GET /meetings/1/edit
  def edit
  end

  # POST /meetings
  # POST /meetings.json
  def create
    #@meeting = Meeting.new(meeting_params)
    @meeting = current_user.meetings.new(meeting_params)
    @meetings = Meeting.all
    #@usuario = User.find(1)

    respond_to do |format|
      if @meeting.save
        format.html { redirect_to root_path, notice: 'El evento a sido creado' }
        format.json { render :show, status: :created, location: @meeting }
      else
        format.html { render :new }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meetings/1
  # PATCH/PUT /meetings/1.json
  def update
    respond_to do |format|
      if @meeting.update(meeting_params)
        format.html { redirect_to @meeting, notice: 'El evento a sido actualizado' }
        format.json { render :show, status: :ok, location: @meeting }
      else
        format.html { render :edit }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meetings/1
  # DELETE /meetings/1.json
  def destroy
    @meeting.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'El evento a sido eliminado.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meeting
      @meeting = Meeting.find(params[:id])
    end

    def guardar
      PatientCount.create(fecha:Time.now,aasm_status: Meeting.find(params[:id]).patient.aasm_state, patient_id: Meeting.find(params[:id]).patient_id, psychologist_id: Meeting.find(params[:id]).psychologist_id )
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meeting_params
      params.require(:meeting).permit(:name, :start_time, :user_id, :psychologist_id, :patient_id)
    end
end
