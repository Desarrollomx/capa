class IndicadoresController < ApplicationController
  before_action :set_indicador, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :authenticate_psicologo!,only: [:index,:create,:edit,:destroy,:update,:show,:new] 

  def index
    #@psychologists =Psychologist.where(:id=> params[:id], :fecha => (params[:date_start]..params[:date_end]))

    #@psychologists = Psychologist.find(current_user.psychologist_id)
    @id = Psychologist.find(current_user.psychologist_id)
    @fi=params[:date_start]
    @ff=params[:date_end]
    @tamizajes_tots =TamizajesTot.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@tamizajes_tots =TamizajesTot.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @ta=0
     temp= @tamizajes_tots.map {|m| m[:h]}
        temp.each do |e|
          @ta+=e
        end
    @ta2=0
    @ta4=0
      temp= @tamizajes_tots.map {|m| m[:m]}
        temp.each do |e|
          @ta2+=e
        end

    @ta4=@ta+@ta2


    @adolescentes_aps =AdolescentesAp.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@adolescentes_aps = AdolescentesAp.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @adol=0
    temp2= @adolescentes_aps.map {|m| m[:h]}
      temp2.each do |e|
        @adol+=e
      end
    @adol2=0
    @adol4=0
    temp2= @adolescentes_aps.map {|m| m[:m]}
      temp2.each do |e|
        @adol2+=e
      end
    @adol4=@adol+@adol2

    @consultas_pvs =ConsultasPv.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@consultas_pvs = ConsultasPv.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @con=0
    temp3= @consultas_pvs.map {|m| m[:h]}
      temp3.each do |e|
        @con+=e
      end
    @con2=0
    @con4=0
    temp3= @consultas_pvs.map {|m| m[:m]}
      temp3.each do |e|
        @con2+=e
      end

    @con4=@con+@con2

    @tratamientos_bcs =TratamientosBc.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@tratamientos_bcs = TratamientosBc.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @tra=0
    temp4= @tratamientos_bcs.map {|m| m[:h]}
      temp4.each do |e|
        @tra+=e
      end
    @tra2=0
    @tra4=0
    temp4= @tratamientos_bcs.map {|m| m[:m]}
      temp4.each do |e|
        @tra2+=e
      end

    @tra4=@tra+@tra2


    @adolescentes_its =AdolescentesIt.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@adolescentes_its = AdolescentesIt.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @ad=0
    temp5= @adolescentes_its.map {|m| m[:h]}
      temp5.each do |e|
        @ad+=e
      end
    @ad2=0
    @ad4=0

    temp5= @adolescentes_its.map {|m| m[:m]}
      temp5.each do |e|
        @ad2+=e
      end
    @ad4=@ad+@ad2

    @consultas_subs =ConsultasSub.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@consultas_subs=ConsultasSub.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @cs=0
    temp6= @consultas_subs.map {|m| m[:h]}
      temp6.each do |e|
        @cs+=e
      end
    @cs2=0
    @cs4=0

    temp6= @consultas_subs.map {|m| m[:m]}
      temp6.each do |e|
        @cs2+=e
      end
    @cs4=@cs+@cs2

    @personal_cts =PersonalCt.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@personal_cts = PersonalCt.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @pc=0
    temp7= @personal_cts.map {|m| m[:h]}
      temp7.each do |e|
        @pc+=e
      end
    @pc2=0
    @pc4=0

    temp7= @personal_cts.map {|m| m[:m]}
      temp7.each do |e|
        @pc2+=e
      end
    @pc4=@pc+@pc2

    @capacitaciones_ots =CapacitacionesOt.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@capacitaciones_ots= CapacitacionesOt.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @co=0
    temp8= @capacitaciones_ots.map {|m| m[:h]}
      temp8.each do |e|
        @co+=e
      end

    @co2=0
    @co4=0
    temp8= @capacitaciones_ots.map {|m| m[:m]}
      temp8.each do |e|
        @co2+=e
      end

    @co4=@co+@co2

    @capacitaciones_recs =CapacitacionesRec.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@capacitaciones_recs= CapacitacionesRec.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @cpt=0
    temp9= @capacitaciones_recs.map {|m| m[:h]}
      temp9.each do |e|
        @cpt+=e
      end

    @cpt=0
    @cpt2=0
    temp9= @capacitaciones_recs.map {|m| m[:m]}
      temp9.each do |e|
        @cpt2+=e
      end
    @cpt4=@cpt+@cpt2


    @personas_vgs =PersonasVg.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    #@personas_vgs= PersonasVg.find_by_psychologist_id_and_fecha(params[:id],params[:fec])
    @per=0
    temp10= @personas_vgs.map {|m| m[:h]}
      temp10.each do |e|
        @per+=e
      end

    @per2=0
    @per4=0
    temp10= @personas_vgs.map {|m| m[:m]}
      temp10.each do |e|
        @per2+=e
      end

    @per4=@per+@per2

    @beneficiados =Beneficiado.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    @ben=0
    temp11= @beneficiados.map {|m| m[:h]}
      temp11.each do |e|
        @ben+=e
      end
    @ben2=0
    @ben4=0
    temp11= @beneficiados.map {|m| m[:m]}
      temp11.each do |e|
        @ben2+=e
      end

    @ben4=@ben+@ben2

    @multyproms =Multyprom.where(:psychologist_id=> current_user.psychologist_id, :fecha => (params[:date_start]..params[:date_end])).select(:h, :m)
    @mult=0
    temp12= @multyproms.map {|m| m[:h]}
      temp12.each do |e|
        @mult+=e
      end
    @mult2=0
    @mult4=0
    temp12= @multyproms.map {|m| m[:m]}
      temp12.each do |e|
        @mult2+=e
      end

    @mult4=@mult+@mult2

    #@psychologists = Psychologist.where("id LIKE ? OR nombre LIKE ? OR app LIKE ? OR apm LIKE ?", "%#{params[:id]}%", "%#{params[:nombre]}%", "%#{params[:app]}%", "%#{params[:apm]}%")
    #@tamizajes_tots = TamizajesTot.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@adolescentes_aps = AdolescentesAp.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@consultas_pvs = ConsultasPv.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@tratamientos_bcs = TratamientosBc.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@adolescentes_its = AdolescentesIt.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@consultas_subs=ConsultasSub.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@personal_cts = PersonalCt.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@capacitaciones_ots= CapacitacionesOt.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@capacitaciones_recs= CapacitacionesRec.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@personas_vgs= PersonasVg.where(":psychologist_id = ? AND :fecha = ?", "#{params[:id]}", "#{params[:fec]}").uniq
    #@indicadores = '#{tamizajes_tots, adolescentes_aps, consultas_pvs, tratamientos_bcs, adolescentes_its, consultas_subs, personal_cts, capacitaciones_ots, capacitaciones_recs, personas_vgs}'


    respond_to do |format|
      @psychologists = Psychologist.find_by_id(@id)
      format.html
      format.pdf do
        render pdf:"Indicadores",
        #header: { center: 'hola' },
        #footer: { center: 'hola' },
        header: {
          content: render_to_string(template: 'layouts/header.html.pdf.erb')
        },
        footer: {
          content: render_to_string(layout: 'layouts/footer.html.pdf.erb')
        },
        layout: 'layouts/pdf.html.erb',
        page_size: 'letter',
        file:'indicadores/index.pdf.erb'
      end
    end

  end

  def show

  end

  def new
    #@indicadores = #{@tamizajes,@adolescentesAP}
    #tamizajes_tots= TamizajesTot.new
    #@adolescentes_aps = AdolescentesAp.new
    #@consultas_pvs = ConsultasPv.new
    #@tratamientos_bcs = TratamientosBc.new
    #@adolescentes_its = AdolescentesIt.new
    #@consultas_subs=ConsultasSub.new
    #@personal_cts = PersonalCt.new
    #@capacitaciones_ots= CapacitacionesOt.new
    #@capacitaciones_recs= CapacitacionesRec.new
    #@personas_vgs= PersonasVg.new

  end

  def edit
  end

  def create
    #@tamizajes_tots= TamizajesTot.new(indicadores_params)
    #@adolescentes_aps = AdollescentesAp.new(indicadores_params)
    #@consultas_pvs = ConsultasPv.new(indicadores_params)
    #@tratatamientos_bcs = TratamientosBc.new(indicadores_params)
    #@adolescentes_its = AdolescentesIt.new(indicadores_params)
    #@consultas_subs=ConsultasSub.new(indicadores_params)
    #@personal_cts = PersonalCt.new(indicadores_params)
    #@capacitaciones_ots= CapacitacionesOt.new(indicadores_params)
    #@capacitaciones_recs= CapacitacionesRec.new(indicadores_params)
    #@personas_vgs= PersonasVg.new(indicadores_params)



  end

  def update
  end

  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_indicador
    #@tamizajes_tots= TamizajesTot.find(params[:id])
    #@adolescentes_aps = AdolescentesAp.find(params[:id])
    #@consultas_pvs = ConsultasPv.find(params[:id])
    #@tratatamientos_bcs = TratamientosBc.find(params[:id])
    #@adolescentes_its = AdolescentesIt.find(params[:id])
    #@consultas_subs=ConsultasSub.find(params[:id])
    #@personal_cts = PersonalCt.find(params[:id])
    #@capacitaciones_ots= CapacitacionesOt.find(params[:id])
    #@capacitaciones_recs= CapacitacionesRec.find(params[:id])
    #@personas_vgs= PersonasVg.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def indicador_params
      #params.require(:tamizajes_tots).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:adolescentes_aps).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:consultas_pvs).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:tratamientos_bcs).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:adolescentes_its).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:consultas_subs).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:personal_cts).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:capacitaciones_otors).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:capacitaciones_recs).permit(:h, :m, :fec, :psychologist_id)
      #params.require(:personas_vgs).permit(:h, :m, :fec, :psychologist_id)
  end
end
