class TratamientosBcsController < ApplicationController
  before_action :set_tratamientos_bc, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /tratamientos_bcs
  # GET /tratamientos_bcs.json
  def index
    @tratamientos_bcs = TratamientosBc.all
  end

  # GET /tratamientos_bcs/1
  # GET /tratamientos_bcs/1.json
  def show
  end

  # GET /tratamientos_bcs/new
  def new
    @tratamientos_bc = TratamientosBc.new
    
  end

  # GET /tratamientos_bcs/1/edit
  def edit
  end

  # POST /tratamientos_bcs
  # POST /tratamientos_bcs.json
  def create
    @tratamientos_bc = TratamientosBc.new(tratamientos_bc_params)

    respond_to do |format|
      if @tratamientos_bc.save
        format.html { redirect_to @tratamientos_bc, notice: 'Tratamientos bc was successfully created.' }
        format.json { render :show, status: :created, location: @tratamientos_bc }
      else
        format.html { render :new }
        format.json { render json: @tratamientos_bc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tratamientos_bcs/1
  # PATCH/PUT /tratamientos_bcs/1.json
  def update
    respond_to do |format|
      if @tratamientos_bc.update(tratamientos_bc_params)
        format.html { redirect_to @tratamientos_bc, notice: 'Tratamientos bc was successfully updated.' }
        format.json { render :show, status: :ok, location: @tratamientos_bc }
      else
        format.html { render :edit }
        format.json { render json: @tratamientos_bc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tratamientos_bcs/1
  # DELETE /tratamientos_bcs/1.json
  def destroy
    @tratamientos_bc.destroy
    respond_to do |format|
      format.html { redirect_to tratamientos_bcs_url, notice: 'Tratamientos bc was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tratamientos_bc
      @tratamientos_bc = TratamientosBc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tratamientos_bc_params
      params.require(:tratamientos_bc).permit(:h, :m, :fecha, :psychologist_id, :h1,:h2,:h3,:h4,:h5,:h6,:h7,:h8,:h8,:h10,:m1,:m2,:m3,:m4,:m5,:m6,:m7,:m8,:m9, :m10
)
    end
end
