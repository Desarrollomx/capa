class ReportesController < ApplicationController
  before_action :set_reporte, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!



  def index
    @t1=0
    @t2=0
    @t3=0
    @t4=0
    @t5=0
    @t6=0
    @t7=0
    @t8=0
    @t9=0
    @t10=0
    @t11=0
    @t12=0
    @t13=0
    @t14=0
    @t15=0
    @t16=0
    @t17=0
    @t18=0
    @t19=0
    @t20=0
    @t21=0
    @t22=0
    @t23=0
    @t24=0
    @t25=0
    @t26=0
    @t27=0
    @t28=0
    @t29=0
    @t30=0
    @t31=0
    @t32=0
    @t33=0
    @t34=0
    @t35=0
    @t36=0


  	@expedientes =Expediente.all
  	@psychologists = Psychologist.all
  	@patients = Patient.last

    @meetings_count = Meeting.where(:user_id => current_user).count
    @nombre = Psychologist.find_by_id(current_user.psychologist_id)
    time = Time.new
    @mes = time.month

    @id = Psychologist.find_by_id(params[:id])
    @fi=params[:date_start]
    @ff=params[:date_end]

    $ari= psicologo(1)
    @metas1=$ari.fetch(0)
    @desempeño1=$ari.fetch(1)
    @total1=[(@desempeño1.fetch(0)+@desempeño1.fetch(1)),(@desempeño1.fetch(2)+@desempeño1.fetch(3)),(@desempeño1.fetch(4)+@desempeño1.fetch(5)),(@desempeño1.fetch(6)+@desempeño1.fetch(7)),(@desempeño1.fetch(8)+@desempeño1.fetch(9)),(@desempeño1.fetch(10)+@desempeño1.fetch(11)),(@desempeño1.fetch(12)+@desempeño1.fetch(13)),(@desempeño1.fetch(14)+@desempeño1.fetch(15)),(@desempeño1.fetch(16)+@desempeño1.fetch(17)),(@desempeño1.fetch(18)+@desempeño1.fetch(19)),(@desempeño1.fetch(20)+@desempeño1.fetch(21)),(@desempeño1.fetch(22)+@desempeño1.fetch(23))]

    $esme= psicologo(2)
    @metas2=$esme.fetch(0)
    @desempeño2=$esme.fetch(1)    
    @total2=[(@desempeño2.fetch(0)+@desempeño2.fetch(1)),(@desempeño2.fetch(2)+@desempeño2.fetch(3)),(@desempeño2.fetch(4)+@desempeño2.fetch(5)),(@desempeño2.fetch(6)+@desempeño2.fetch(7)),(@desempeño2.fetch(8)+@desempeño2.fetch(9)),(@desempeño2.fetch(10)+@desempeño2.fetch(11)),(@desempeño2.fetch(12)+@desempeño2.fetch(13)),(@desempeño2.fetch(14)+@desempeño2.fetch(15)),(@desempeño2.fetch(16)+@desempeño2.fetch(17)),(@desempeño2.fetch(18)+@desempeño2.fetch(19)),(@desempeño2.fetch(20)+@desempeño2.fetch(21)),(@desempeño2.fetch(22)+@desempeño2.fetch(23))]


    $man = psicologo(3)
    @metas3=$man.fetch(0)
    @desempeño3=$man.fetch(1)
    @total3=[(@desempeño3.fetch(0)+@desempeño3.fetch(1)),(@desempeño3.fetch(2)+@desempeño3.fetch(3)),(@desempeño3.fetch(4)+@desempeño3.fetch(5)),(@desempeño3.fetch(6)+@desempeño3.fetch(7)),(@desempeño3.fetch(8)+@desempeño3.fetch(9)),(@desempeño3.fetch(10)+@desempeño3.fetch(11)),(@desempeño3.fetch(12)+@desempeño3.fetch(13)),(@desempeño3.fetch(14)+@desempeño3.fetch(15)),(@desempeño3.fetch(16)+@desempeño3.fetch(17)),(@desempeño3.fetch(18)+@desempeño3.fetch(19)),(@desempeño3.fetch(20)+@desempeño3.fetch(21)),(@desempeño3.fetch(22)+@desempeño3.fetch(23))]

    $sus = psicologo(4)
    @metas4=$sus.fetch(0)
    @desempeño4=$sus.fetch(1)
    @total4=[(@desempeño4.fetch(0)+@desempeño4.fetch(1)),(@desempeño4.fetch(2)+@desempeño4.fetch(3)),(@desempeño4.fetch(4)+@desempeño4.fetch(5)),(@desempeño4.fetch(6)+@desempeño4.fetch(7)),(@desempeño4.fetch(8)+@desempeño4.fetch(9)),(@desempeño4.fetch(10)+@desempeño4.fetch(11)),(@desempeño4.fetch(12)+@desempeño4.fetch(13)),(@desempeño4.fetch(14)+@desempeño4.fetch(15)),(@desempeño4.fetch(16)+@desempeño4.fetch(17)),(@desempeño4.fetch(18)+@desempeño4.fetch(19)),(@desempeño4.fetch(20)+@desempeño4.fetch(21)),(@desempeño4.fetch(22)+@desempeño4.fetch(23))]

    $humb = psicologo(5)
    @metas5=$humb.fetch(0)
    @desempeño5=$humb.fetch(1)
    @total5=[(@desempeño5.fetch(0)+@desempeño5.fetch(1)),(@desempeño5.fetch(2)+@desempeño5.fetch(3)),(@desempeño5.fetch(4)+@desempeño5.fetch(5)),(@desempeño5.fetch(6)+@desempeño5.fetch(7)),(@desempeño5.fetch(8)+@desempeño5.fetch(9)),(@desempeño5.fetch(10)+@desempeño5.fetch(11)),(@desempeño5.fetch(12)+@desempeño5.fetch(13)),(@desempeño5.fetch(14)+@desempeño5.fetch(15)),(@desempeño5.fetch(16)+@desempeño5.fetch(17)),(@desempeño5.fetch(18)+@desempeño5.fetch(19)),(@desempeño5.fetch(20)+@desempeño5.fetch(21)),(@desempeño5.fetch(22)+@desempeño5.fetch(23))]

    $cel = psicologo(6)
    @metas6=$cel.fetch(0)
    @desempeño6=$cel.fetch(1)
    @total6=[(@desempeño6.fetch(0)+@desempeño6.fetch(1)),(@desempeño6.fetch(2)+@desempeño6.fetch(3)),(@desempeño6.fetch(4)+@desempeño6.fetch(5)),(@desempeño6.fetch(6)+@desempeño6.fetch(7)),(@desempeño6.fetch(8)+@desempeño6.fetch(9)),(@desempeño6.fetch(10)+@desempeño6.fetch(11)),(@desempeño6.fetch(12)+@desempeño6.fetch(13)),(@desempeño6.fetch(14)+@desempeño6.fetch(15)),(@desempeño6.fetch(16)+@desempeño6.fetch(17)),(@desempeño6.fetch(18)+@desempeño6.fetch(19)),(@desempeño6.fetch(20)+@desempeño6.fetch(21)),(@desempeño6.fetch(22)+@desempeño6.fetch(23))]

    total

    respond_to do |format|
      format.html
      format.pdf do
        render pdf:"Reporte",
        #header: { center: 'hola' },
        #footer: { center: 'hola' },
        header: {
          content: render_to_string(template: 'layouts/header.html.pdf.erb')
        },
        footer: {
          content: render_to_string(layout: 'layouts/footer.html.pdf.erb')
        },
        layout: 'layouts/pdf.html.erb',
        page_size: 'letter',
        file:'reportes/index.pdf.erb'
      end
    end      
  end

  def psicologo(id)
    tmp=id
    $metas=[]    
    $desempeño=[]

    @metas =Metum.where(:fecha => (params[:date_start]..params[:date_end]))
    $t1=0
    tam1= @metas.map {|m| m[:tamizajes]} 
    tam1.each do |e| 
      $t1+=e 
    end

    $t4=0
    adolap1= @metas.map {|m| m[:adol_acc_prev]} 
      adolap1.each do |e| 
        $t4+=e 
      end

    $t7=0  
    consupv1= @metas.map {|m| m[:cons_1a_vez]}
      consupv1.each do |e| 
        $t7+=e 
      end

    $t10=0    
    tratbc1= @metas.map {|m| m[:trat_brev_conc]} 
      tratbc1.each do |e| 
      $t10+=e 
    end  
  
    $t13=0
    adolit1= @metas.map {|m| m[:adol_ini_trat]} 
      adolit1.each do |e| 
        $t13=e 
      end  
    $t16=0
    consub1= @metas.map {|m| m[:cons_subs]} 
      consub1.each do |e| 
        $t16+=e 
      end 
    $t19=0  
    perscap1= @metas.map {|m| m[:pers_cap_ces_tab]} 
      perscap1.each do |e| 
        $t19+=e 
      end
    $t22=0
    capot1= @metas.map {|m| m[:cap_otor_prev_trat]} 
      capot1.each do |e| 
        $t22+=e 
      end
    $t25=0
    caprec1= @metas.map {|m| m[:cap_rec_prev_trat]} 
      caprec1.each do |e| 
        $t25+=e 
      end

    $t28=0  
    pervg1= @metas.map {|m| m[:pers_det_viol_gen]} 
      pervg1.each do |e| 
        $t28+=e 
      end   

    $t31=0  
    benef1= @metas.map {|m| m[:beneficiados]} 
      benef1.each do |e| 
        $t31+=e 
      end     

    $t34=0  
    mult1= @metas.map {|m| m[:multip_promot]} 
      mult1.each do |e| 
        $t34+=e 
      end           

    @tamizajes_tots =TamizajesTot.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
    $t2=0 
    $t3=0
    tamtot2= @tamizajes_tots.map {|m| m[:h]} 
      tamtot2.each do |e| 
        $t2+=e 
      end
    tamtot3= @tamizajes_tots.map {|m| m[:m]} 
      tamtot3.each do |e| 
        $t3+=e 
      end
  

    @adolescentes_aps = AdolescentesAp.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end])) 
    $t5=0 
    $t6=0
    adolap2= @adolescentes_aps.map {|m| m[:h]} 
      adolap2.each do |e| 
        $t5+=e 
      end
    adolap3= @adolescentes_aps.map {|m| m[:m]} 
      adolap3.each do |e| 
        $t6+=e 
      end
            
    @consultas_pvs = ConsultasPv.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
      $t8=0 
      $t9=0
      consupv2= @consultas_pvs.map {|m| m[:h]} 
        consupv2.each do |e| 
          $t8+=e 
        end
      consupv3= @consultas_pvs.map {|m| m[:m]} 
        consupv3.each do |e| 
          $t9+=e 
        end
    @tratamientos_bcs = TratamientosBc.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
      $t11=0 
      $t12=0
      tratbc2= @tratamientos_bcs.map {|m| m[:h]} 
        tratbc2.each do |e| 
           $t11+=e 
        end
        tratbc3= @tratamientos_bcs.map {|m| m[:m]} 
          tratbc3.each do |e| 
            $t12+=e 
          end
    @adolescentes_its = AdolescentesIt.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
    $t14=0 
    $t15=0
      adolit2= @adolescentes_its.map {|m| m[:h]} 
        adolit2.each do |e| 
          $t14+=e 
        end
      adolit3= @adolescentes_its.map {|m| m[:m]} 
        adolit3.each do |e| 
          $t15+=e 
        end
    @consultas_subs=ConsultasSub.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
      $t17=0 
      $t18=0
      consub2= @consultas_subs.map {|m| m[:h]} 
        consub2.each do |e| 
          $t17+=e 
        end
      consub3= @consultas_subs.map {|m| m[:m]} 
        consub3.each do |e| 
          $t18+=e 
        end
    @personal_cts = PersonalCt.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
      $t20=0 
      $t21=0
      perscap2= @personal_cts.map {|m| m[:h]} 
        perscap2.each do |e| 
          $t20+=e 
        end
      perscap3= @personal_cts.map {|m| m[:m]} 
        perscap3.each do |e| 
          $t21+=e 
        end

    @capacitaciones_ots= CapacitacionesOt.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
    $t23=0 
    $t24=0
      capot2= @capacitaciones_ots.map {|m| m[:h]} 
        capot2.each do |e| 
          $t23+=e 
        end
      capot3= @capacitaciones_ots.map {|m| m[:m]} 
        capot3.each do |e| 
          $t24+=e 
        end

    @capacitaciones_recs= CapacitacionesRec.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
    $t26=0 
    $t27=0
      caprec2= @capacitaciones_recs.map {|m| m[:h]} 
        caprec2.each do |e| 
          $t26+=e 
        end
      caprec3= @capacitaciones_recs.map {|m| m[:m]} 
        caprec3.each do |e| 
          $t27+=e 
        end
    @personas_vgs= PersonasVg.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
    $t29=0
    $t30=0
    pervg2= @personas_vgs.map {|m| m[:h]} 
      pervg2.each do |e| 
        $t29+=e 
      end
    pervg3= @personas_vgs.map {|m| m[:m]} 
      pervg3.each do |e| 
        $t30+=e 
      end

    @beneficiarios= Beneficiado.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
    $t32=0
    $t33=0
    benef2= @beneficiarios.map {|m| m[:h]} 
      benef2.each do |e| 
        $t32+=e 
      end
    benef3= @beneficiarios.map {|m| m[:m]} 
      benef3.each do |e| 
        $t33+=e 
      end 

    @mult_prom= Multyprom.where(:psychologist_id=> tmp, :fecha => (params[:date_start]..params[:date_end]))
    $t35=0
    $t36=0
    mult2= @mult_prom.map {|m| m[:h]} 
      mult2.each do |e| 
        $t35+=e 
      end
    mult3= @mult_prom.map {|m| m[:m]} 
      mult3.each do |e| 
        $t36+=e 
      end         

    $metas=[$t1,$t4,$t7,$t10,$t13,$t16,$t19,$t22,$t25,$t28,$t31,$t34]    
    $desempeño=[$t2,$t3,$t5,$t6,$t8,$t9,$t11,$t12,$t14,$t15,$t17,$t18,$t20,$t21,$t23,$t24,$t26,$t27,$t29,$t30,$t32,$t33,$t35,$t36]
    return [$metas, $desempeño]
 
  end

  def total
    @t1= @metas1.fetch(0)+@metas2.fetch(0)+@metas3.fetch(0)+ @metas4.fetch(0)+@metas5.fetch(0)+@metas6.fetch(0)
    @t4= @metas1.fetch(1)+@metas2.fetch(1)+@metas3.fetch(1)+ @metas4.fetch(1)+@metas5.fetch(1)+@metas6.fetch(1)
    @t7= @metas1.fetch(2)+@metas2.fetch(2)+@metas3.fetch(2)+ @metas4.fetch(2)+@metas5.fetch(2)+@metas6.fetch(2)
    @t10= @metas1.fetch(3)+@metas2.fetch(3)+@metas3.fetch(3)+ @metas4.fetch(3)+@metas5.fetch(3)+@metas6.fetch(3)
    @t13= @metas1.fetch(4)+@metas2.fetch(4)+@metas3.fetch(4)+ @metas4.fetch(4)+@metas5.fetch(4)+@metas6.fetch(4)
    @t16= @metas1.fetch(5)+@metas2.fetch(5)+@metas3.fetch(5)+ @metas4.fetch(5)+@metas5.fetch(5)+@metas6.fetch(5)
    @t19= @metas1.fetch(6)+@metas2.fetch(6)+@metas3.fetch(6)+ @metas4.fetch(6)+@metas5.fetch(6)+@metas6.fetch(6)
    @t22= @metas1.fetch(7)+@metas2.fetch(7)+@metas3.fetch(7)+ @metas4.fetch(7)+@metas5.fetch(7)+@metas6.fetch(7)
    @t25= @metas1.fetch(8)+@metas2.fetch(8)+@metas3.fetch(8)+ @metas4.fetch(8)+@metas5.fetch(8)+@metas6.fetch(8)
    @t28= @metas1.fetch(9)+@metas2.fetch(9)+@metas3.fetch(9)+ @metas4.fetch(9)+@metas5.fetch(9)+@metas6.fetch(9)
    @t31= @metas1.fetch(10)+@metas2.fetch(10)+@metas3.fetch(10)+ @metas4.fetch(10)+@metas5.fetch(10)+@metas6.fetch(10)
    @t34= @metas1.fetch(11)+@metas2.fetch(11)+@metas3.fetch(11)+ @metas4.fetch(11)+@metas5.fetch(11)+@metas6.fetch(11)


    @t2=@desempeño1.fetch(0)+@desempeño2.fetch(0)+@desempeño3.fetch(0)+@desempeño4.fetch(0)+@desempeño5.fetch(0)+@desempeño6.fetch(0)
    @t3=@desempeño1.fetch(1)+@desempeño2.fetch(1)+@desempeño3.fetch(1)+@desempeño4.fetch(1)+@desempeño5.fetch(1)+@desempeño6.fetch(1)
    @t5=@desempeño1.fetch(2)+@desempeño2.fetch(2)+@desempeño3.fetch(2)+@desempeño4.fetch(2)+@desempeño5.fetch(2)+@desempeño6.fetch(2)
    @t6=@desempeño1.fetch(3)+@desempeño2.fetch(3)+@desempeño3.fetch(3)+@desempeño4.fetch(3)+@desempeño5.fetch(3)+@desempeño6.fetch(3)
    @t8=@desempeño1.fetch(4)+@desempeño2.fetch(4)+@desempeño3.fetch(4)+@desempeño4.fetch(4)+@desempeño5.fetch(4)+@desempeño6.fetch(4)
    @t9=@desempeño1.fetch(5)+@desempeño2.fetch(5)+@desempeño3.fetch(5)+@desempeño4.fetch(5)+@desempeño5.fetch(5)+@desempeño6.fetch(5)
    @t11=@desempeño1.fetch(6)+@desempeño2.fetch(6)+@desempeño3.fetch(6)+@desempeño4.fetch(6)+@desempeño5.fetch(6)+@desempeño6.fetch(6)
    @t12=@desempeño1.fetch(7)+@desempeño2.fetch(7)+@desempeño3.fetch(7)+@desempeño4.fetch(7)+@desempeño5.fetch(7)+@desempeño6.fetch(7)
    @t14=@desempeño1.fetch(8)+@desempeño2.fetch(8)+@desempeño3.fetch(8)+@desempeño4.fetch(8)+@desempeño5.fetch(8)+@desempeño6.fetch(8)
    @t15=@desempeño1.fetch(9)+@desempeño2.fetch(9)+@desempeño3.fetch(9)+@desempeño4.fetch(9)+@desempeño5.fetch(9)+@desempeño6.fetch(9)
    @t17=@desempeño1.fetch(10)+@desempeño2.fetch(10)+@desempeño3.fetch(10)+@desempeño4.fetch(10)+@desempeño5.fetch(10)+@desempeño6.fetch(10)
    @t18=@desempeño1.fetch(11)+@desempeño2.fetch(11)+@desempeño3.fetch(11)+@desempeño4.fetch(11)+@desempeño5.fetch(11)+@desempeño6.fetch(11)
    @t20=@desempeño1.fetch(12)+@desempeño2.fetch(12)+@desempeño3.fetch(12)+@desempeño4.fetch(12)+@desempeño5.fetch(12)+@desempeño6.fetch(12)
    @t21=@desempeño1.fetch(13)+@desempeño2.fetch(13)+@desempeño3.fetch(13)+@desempeño4.fetch(13)+@desempeño5.fetch(13)+@desempeño6.fetch(13)
    @t23=@desempeño1.fetch(14)+@desempeño2.fetch(14)+@desempeño3.fetch(14)+@desempeño4.fetch(14)+@desempeño5.fetch(14)+@desempeño6.fetch(14)
    @t24=@desempeño1.fetch(15)+@desempeño2.fetch(15)+@desempeño3.fetch(15)+@desempeño4.fetch(15)+@desempeño5.fetch(15)+@desempeño6.fetch(15)
    @t26=@desempeño1.fetch(16)+@desempeño2.fetch(16)+@desempeño3.fetch(16)+@desempeño4.fetch(16)+@desempeño5.fetch(16)+@desempeño6.fetch(16)
    @t27=@desempeño1.fetch(17)+@desempeño2.fetch(17)+@desempeño3.fetch(17)+@desempeño4.fetch(17)+@desempeño5.fetch(17)+@desempeño6.fetch(17)
    @t29=@desempeño1.fetch(18)+@desempeño2.fetch(18)+@desempeño3.fetch(18)+@desempeño4.fetch(18)+@desempeño5.fetch(18)+@desempeño6.fetch(18)
    @t30=@desempeño1.fetch(19)+@desempeño2.fetch(19)+@desempeño3.fetch(19)+@desempeño4.fetch(19)+@desempeño5.fetch(19)+@desempeño6.fetch(19)
    @t32=@desempeño1.fetch(20)+@desempeño2.fetch(20)+@desempeño3.fetch(20)+@desempeño4.fetch(20)+@desempeño5.fetch(20)+@desempeño6.fetch(20)
    @t33=@desempeño1.fetch(21)+@desempeño2.fetch(21)+@desempeño3.fetch(21)+@desempeño4.fetch(21)+@desempeño5.fetch(21)+@desempeño6.fetch(21)
    @t35=@desempeño1.fetch(22)+@desempeño2.fetch(22)+@desempeño3.fetch(22)+@desempeño4.fetch(22)+@desempeño5.fetch(22)+@desempeño6.fetch(22)
    @t36=@desempeño1.fetch(23)+@desempeño2.fetch(23)+@desempeño3.fetch(23)+@desempeño4.fetch(23)+@desempeño5.fetch(23)+@desempeño6.fetch(23)

    @metas=[@t1,@t4,@t7,@t10,@t13,@t16,@t19,@t22,@t25,@t28,@t31,@t34]    
    @desempeño=[(@t2+@t3),(@t5+@t6),(@t8+@t9),(@t11+@t12),(@t14+@t15),(@t17+@t18),(@t20+@t21),(@t23+@t24),(@t26+@t27),(@t29+@t30),(@t32+@t33),(@t35+@t36)]
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
  end

  def update
  end

  def destroy
  end

  def set_reporte
  end

  def reporte_params
  end
end
