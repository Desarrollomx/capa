class AdolescentesItsController < ApplicationController
  before_action :set_adolescentes_it, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /adolescentes_its
  # GET /adolescentes_its.json
  def index
    @adolescentes_its = AdolescentesIt.all
    @adolit= Meeting.where(:start_time => (params[:date_start]..params[:date_end]), :termino => '2').select(:patient_id)
    #Patient.where(:edad).count

    @adolit2= @adolit.map { |e| e[:patient_id]}
    
    #@num=@m.count
    @edades= []
    @adolit2.each do |e|
      adit= Patient.select(:edad).where(:id => e, :sexo =>'Masculino')
      adol=adit.map {|m| m[:edad]} 
      @edades.push m 
    end

    @edades.each do |e|
      @h= AdolescentesIt
      @m= AdolescentesIt
    end
  end

  # GET /adolescentes_its/1
  # GET /adolescentes_its/1.json
  def show
  end

  # GET /adolescentes_its/new
  def new
    @adolescentes_it = AdolescentesIt.new
  end

  # GET /adolescentes_its/1/edit
  def edit
  end

  # POST /adolescentes_its
  # POST /adolescentes_its.json
  def create
    @adolescentes_it = AdolescentesIt.new(adolescentes_it_params)

    respond_to do |format|
      if @adolescentes_it.save
        format.html { redirect_to @adolescentes_it, notice: 'Adolescentes it was successfully created.' }
        format.json { render :show, status: :created, location: @adolescentes_it }
      else
        format.html { render :new }
        format.json { render json: @adolescentes_it.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adolescentes_its/1
  # PATCH/PUT /adolescentes_its/1.json
  def update
    respond_to do |format|
      if @adolescentes_it.update(adolescentes_it_params)
        format.html { redirect_to @adolescentes_it, notice: 'Adolescentes it was successfully updated.' }
        format.json { render :show, status: :ok, location: @adolescentes_it }
      else
        format.html { render :edit }
        format.json { render json: @adolescentes_it.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adolescentes_its/1
  # DELETE /adolescentes_its/1.json
  def destroy
    @adolescentes_it.destroy
    respond_to do |format|
      format.html { redirect_to adolescentes_its_url, notice: 'Adolescentes it was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adolescentes_it
      @adolescentes_it = AdolescentesIt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adolescentes_it_params
      params.require(:adolescentes_it).permit(:h, :m, :fecha, :psychologist_id, :h3, :h4, :m3, :m4)
    end
end
