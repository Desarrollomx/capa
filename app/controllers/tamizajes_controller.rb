class TamizajesController < ApplicationController
  before_action :set_tamizaje, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :authenticate_psicologo!,only: [:index,:create,:edit,:destroy,:update,:show,:new]

  # GET /tamizajes
  # GET /tamizajes.json
  def index
    @escuelas = AplicacionTamizaje.all
    @escuela = AplicacionTamizaje.new
    @escuela.grupos_escuelas.build

    if params[:id] == ""
        redirect_to escuelas_path
        @flag = params[:id]
    else
        @str = params[:id]

        str1 = ""
        if(!@str.nil?)
            str1 = @str["00b1b22cf54d65=1434?"]
        end


        if @str != "00a1b22cf54d65=1434?" && str1 != "00b1b22cf54d65=1434?"
            @flag = "0"
        else
            @flag = "1"
        end


        if str1 == "00b1b22cf54d65=1434?" && @flag == "1"
            @flag += "-2"
        end
        #redirect_to escuelas_path(0)
    end
  end

  # GET /tamizajes/1
  # GET /tamizajes/1.json
  def show
      #@tamizajes = Tamizaje.where("id_escuela = #{params[:id]}")
      @esctemp = AplicacionTamizaje.find(params[:id])
      #redirect_to tamizaje_path(@esctemp)
      respond_to do |format|
        format.html
        format.pdf do
            render pdf:"Informe-#{@esctemp.nivel_estudio.nivel}-#{@esctemp.escuela}-#{@esctemp.municipio}",
          #header: { center: 'hola' },
          #footer: { center: 'hola' },
          layout: 'layouts/pdf.html.erb',
          page_size: 'letter',
          file:'tamizajes/show.pdf.erb'
        end
      end
  end

  def show_tamizaje

      if params[:id] == "new"
          redirect_to new_tamizaje_path
      else
          if request.post?
              id = {
                  :id => params[:'a1b22cf54d65=1435?']
              };
              @tamizaje = Tamizaje.find(id[:id])
          end
          #@otro = Tamizaje.all
      end
  end

  def show_grupos
        if request.post?
            id = {
                :id => params[:'a1c32cf54d65=1434?']
            };
            #@grupos = Tamizaje.find_by_aplicacion_tamizaje_id(id[:id])
            @grupos = Tamizaje.where("aplicacion_tamizaje_id = #{id[:id]}").order(:grado,:grupo,:apellidoP,:apellidoM)
            @esctemp = AplicacionTamizaje.find(id[:id])

            @riesgoMayor = Tamizaje.where("aplicacion_tamizaje_id = #{id[:id]} AND total_uso_sustancias > 3").order(:grado,:grupo,:apellidoP,:apellidoM)

            @riesgoMenor = Tamizaje.where("aplicacion_tamizaje_id = #{id[:id]} AND total_uso_sustancias <= 3 AND total_uso_sustancias > 0").order(:grado,:grupo,:apellidoP,:apellidoM)

        end

          respond_to do |format|
            format.html
            format.pdf do
                render pdf:"Informe-#{@esctemp.nivel_estudio.nivel}-#{@esctemp.escuela}-#{@esctemp.municipio}",
              #header: { center: 'hola' },
              #footer: { center: 'hola' },
              layout: 'layouts/pdf_style_null.html.erb',
              page_size: 'letter',
              file:'tamizajes/show_grupos.pdf.erb',
              orientation: 'Landscape',
              margin:  {   top:10,                     # default 10 (mm)
                            bottom:10,
                            left:10,
                            right:10 }
            end
          end

  end

  def prueba
      @grupos = Tamizaje.where("aplicacion_tamizaje_id = #{params[:id]}").order(:grado,:grupo,:apellidoP,:apellidoM)
      @esctemp = AplicacionTamizaje.find(params[:id])

      @riesgoMayor = Tamizaje.where("aplicacion_tamizaje_id = #{params[:id]} AND total_uso_sustancias > 3").order(:grado,:grupo,:apellidoP,:apellidoM)

            @riesgoMenor = Tamizaje.where("aplicacion_tamizaje_id = #{params[:id]} AND total_uso_sustancias <= 3 AND total_uso_sustancias > 0").order(:grado,:grupo,:apellidoP,:apellidoM)


          respond_to do |format|
            format.html
            format.pdf do
                render pdf:"Informe-#{@esctemp.nivel_estudio.nivel}-#{@esctemp.escuela}-#{@esctemp.municipio}",
              #header: { center: 'hola' },
              #footer: { center: 'hola' },
              layout: 'layouts/pdf_style_null.html.erb',
              page_size: 'letter',
              file:'tamizajes/show_grupos.pdf.erb',
              orientation: 'Landscape',
              margin:  {   top:10,                     # default 10 (mm)
                            bottom:10,
                            left:10,
                            right:10 }

            end
          end

  end


  def show_riesgo
      @esctemp = AplicacionTamizaje.find(params[:id])

      @riesgoMayor = Tamizaje.where("aplicacion_tamizaje_id = #{params[:id]} AND total_uso_sustancias > 3").order(:grado,:grupo,:apellidoP,:apellidoM)

            @riesgoMenor = Tamizaje.where("aplicacion_tamizaje_id = #{params[:id]} AND total_uso_sustancias <= 3 AND total_uso_sustancias > 0").order(:grado,:grupo,:apellidoP,:apellidoM)


          respond_to do |format|
            format.html
            format.pdf do
                render pdf:"Informe-#{@esctemp.nivel_estudio.nivel}-#{@esctemp.escuela}-#{@esctemp.municipio}",
              #header: { center: 'hola' },
              #footer: { center: 'hola' },
              layout: 'layouts/pdf_style_null.html.erb',
              page_size: 'letter',
                file:'tamizajes/show_riesgo.pdf.erb',
              margin:  {   top:10,                     # default 10 (mm)
                            bottom:10,
                            left:10,
                            right:10 }

            end
          end
  end

  def tabla_siceca

  end

  # GET /tamizajes/new
  def new
    @tamizaje = Tamizaje.new
    if request.post?
        id = {
            :id => params[:'a1b22cf54d65=1434?']
        };
        @esctemp = AplicacionTamizaje.find(id[:id])

        x = GruposEscuela.find_by_aplicacion_tamizaje_id(id[:id])
        array = x.grupos.split(",")
        @g_g = {}
        array.each do |a|
            y = a.split("-")
            @g_g [y[0]]=y[1]
        end
    end
  end

  def termino_tam
      if request.post?
          id = {
              :id => params[:'term1b22cf54d65=1434?']
          }
      end
      esc = AplicacionTamizaje.find(id[:id])
      esc.status = "1"
      esc.save
      redirect_to tamizaje_path(esc)

  end

  # GET /tamizajes/1/edit
  def edit
      @tamizaje = Tamizaje.find(params[:id])
  end

  # POST /tamizajes
  # POST /tamizajes.json
  def create
    @tamizaje = Tamizaje.new(tamizaje_params)

    respond_to do |format|
        if request.post?
            @tamizaje.aplicacion_tamizaje_id = params[:"a1b22cf54d65=1434?"]
            @esctemp = AplicacionTamizaje.find(params[:"a1b22cf54d65=1434?"])
            x = GruposEscuela.find_by_aplicacion_tamizaje_id(params[:"a1b22cf54d65=1434?"])
            array = x.grupos.split(",")
            @g_g = {}
            array.each do |a|
                y = a.split("-")
                @g_g [y[0]]=y[1]
            end
            #Tambien @tamizaje[:id_escuela]
        end

        if @tamizaje.save
            format.html { redirect_to tamizaje_path(@tamizaje.aplicacion_tamizaje_id), notice: '¡Tamizaje guardado correctamente!.' }
            #format.html { redirect_to show_tamizaje_url(@tamizaje.id_escuela ,:'a1b22cf54d65=1435?'=>@tamizaje.id),notice: 'Tamizaje was successfully created.' }
        format.json { render :new, status: :created, location: @tamizaje }
      else
            #format.html { redirect_to escuelas_path }
        format.html { render action: 'new' }
        format.json { render json: @tamizaje.errors, status: :unprocessable_entity }
      end
    end
      #:'a1b22cf54d65=1435?'=>@tamizaje.id_escuela),:method => :post
  end

  # PATCH/PUT /tamizajes/1
  # PATCH/PUT /tamizajes/1.json
  def update
    respond_to do |format|
      if @tamizaje.update(tamizaje_params)
        format.html { redirect_to @tamizaje, notice: 'Tamizaje was successfully updated.' }
        format.json { render :show, status: :ok, location: @tamizaje }
      else
        format.html { render :edit }
        format.json { render json: @tamizaje.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tamizajes/1
  # DELETE /tamizajes/1.json
  def destroy
    @tamizaje.destroy
    respond_to do |format|
      format.html { redirect_to tamizajes_url, notice: 'Tamizaje was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tamizaje
        #@tamizaje = Tamizaje.find(params[:id])
        @tamizajes = Tamizaje.where("aplicacion_tamizaje_id = #{params[:id]}")
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tamizaje_params
      params.require(:tamizaje).permit(:nombre, :apellidoP, :apellidoM, :edad, :sexo, :grado, :grupo, :folio_cuestionario, :p_usoS, :total_uso_sustancias, :p_saludM, :total_salud_mental, :p_relacionF, :total_relacion_familiar, :p_relacionA, :total_relacion_amigos, :p_nivelE, :total_nivel_educativo, :p_interesL, :total_interes_laboral, :p_conductaA, :total_conducta_agresiva, :resultado, :aplicacion_tamizaje_id)
    end
end
