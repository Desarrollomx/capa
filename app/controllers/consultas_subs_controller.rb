class ConsultasSubsController < ApplicationController
  before_action :set_consultas_sub, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /consultas_subs
  # GET /consultas_subs.json
  def index
    @consultas_subs = ConsultasSub.all
  end

  # GET /consultas_subs/1
  # GET /consultas_subs/1.json
  def show
  end

  # GET /consultas_subs/new
  def new
    @consultas_sub = ConsultasSub.new
  end

  # GET /consultas_subs/1/edit
  def edit
  end

  # POST /consultas_subs
  # POST /consultas_subs.json
  def create
    @consultas_sub = ConsultasSub.new(consultas_sub_params)

    respond_to do |format|
      if @consultas_sub.save
        format.html { redirect_to @consultas_sub, notice: 'Consultas sub was successfully created.' }
        format.json { render :show, status: :created, location: @consultas_sub }
      else
        format.html { render :new }
        format.json { render json: @consultas_sub.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /consultas_subs/1
  # PATCH/PUT /consultas_subs/1.json
  def update
    respond_to do |format|
      if @consultas_sub.update(consultas_sub_params)
        format.html { redirect_to @consultas_sub, notice: 'Consultas sub was successfully updated.' }
        format.json { render :show, status: :ok, location: @consultas_sub }
      else
        format.html { render :edit }
        format.json { render json: @consultas_sub.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /consultas_subs/1
  # DELETE /consultas_subs/1.json
  def destroy
    @consultas_sub.destroy
    respond_to do |format|
      format.html { redirect_to consultas_subs_url, notice: 'Consultas sub was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_consultas_sub
      @consultas_sub = ConsultasSub.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def consultas_sub_params
      params.require(:consultas_sub).permit(:h, :m, :fecha, :psychologist_id, :h1,:h2,:h3,:h4,:h5,:h6,:h7,:h8,:h8,:h10,:m1,:m2,:m3,:m4,:m5,:m6,:m7,:m8,:m9, :m10
)
    end
end
