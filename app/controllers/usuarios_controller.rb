class UsuariosController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin!,only: [:index]

  def index
    @usuarios = User.all
  end

end
