class EtniaController < ApplicationController
  before_action :set_etnium, only: [:show, :edit, :update, :destroy]

  # GET /etnia
  # GET /etnia.json
  def index
    @etnia = Etnium.all
  end

  # GET /etnia/1
  # GET /etnia/1.json
  def show
  end

  # GET /etnia/new
  def new
    @etnium = Etnium.new
  end

  # GET /etnia/1/edit
  def edit
  end

  # POST /etnia
  # POST /etnia.json
  def create
    @etnium = Etnium.new(etnium_params)

    respond_to do |format|
      if @etnium.save
        format.html { redirect_to @etnium, notice: 'Etnium was successfully created.' }
        format.json { render :show, status: :created, location: @etnium }
      else
        format.html { render :new }
        format.json { render json: @etnium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /etnia/1
  # PATCH/PUT /etnia/1.json
  def update
    respond_to do |format|
      if @etnium.update(etnium_params)
        format.html { redirect_to @etnium, notice: 'Etnium was successfully updated.' }
        format.json { render :show, status: :ok, location: @etnium }
      else
        format.html { render :edit }
        format.json { render json: @etnium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /etnia/1
  # DELETE /etnia/1.json
  def destroy
    @etnium.destroy
    respond_to do |format|
      format.html { redirect_to etnia_url, notice: 'Etnium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_etnium
      @etnium = Etnium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def etnium_params
      params.require(:etnium).permit(:etnia)
    end
end
