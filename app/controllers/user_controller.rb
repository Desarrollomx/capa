class UserController < ApplicationController
	def index
		@user = User.all
		@psychologists = Psychologist.all
	end

	def update
	  #@user.update(user_params)
		User.update(params[:id],user_params)
		@id = User.find(params[:id])
		Psychologist.update(@id.id,:asignado => 1)
		redirect_to usuarios_path, notice: "Psicologo agregado"
	end

	private
	def set_users
    @user = User.find(params[:id])
  end
	def user_params
    params.require(:user).permit(:psychologist_id,:permisson_level)
  end
end
