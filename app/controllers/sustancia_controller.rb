class SustanciaController < ApplicationController
  before_action :set_sustancium, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /sustancia
  # GET /sustancia.json
  def index
    @sustanciums = Sustancium.ultimos
    @sustancium = Sustancium.new
  end

  # GET /sustancia/1
  # GET /sustancia/1.json
  def show
  end

  # GET /sustancia/new
  def new
    @sustancium = Sustancium.new
  end

  # GET /sustancia/1/edit
  def edit
  end

  # POST /sustancia
  # POST /sustancia.json
  def create
    @sustancium = Sustancium.new(sustancium_params)

    respond_to do |format|
      if @sustancium.save
        format.html { redirect_to @sustancium, notice: 'Sustancia agregada exitosamente.' }
        format.json { render :show, status: :created, location: @sustancium }
      else
        format.html { render :new }
        format.json { render json: @sustancium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sustancia/1
  # PATCH/PUT /sustancia/1.json
  def update
    respond_to do |format|
      if @sustancium.update(sustancium_params)
        format.html { redirect_to @sustancium, notice: 'Sustancia actualizada exitosamente.' }
        format.json { render :show, status: :ok, location: @sustancium }
      else
        format.html { render :edit }
        format.json { render json: @sustancium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sustancia/1
  # DELETE /sustancia/1.json
  def destroy
    @sustancium.destroy
    respond_to do |format|
      format.html { redirect_to sustancia_url, notice: 'Sustancia eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sustancium
      @sustancium = Sustancium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sustancium_params
      params.require(:sustancium).permit(:sustancia, :clave)
    end
end
