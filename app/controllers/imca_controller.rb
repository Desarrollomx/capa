class ImcaController < ApplicationController
  before_action :set_imca, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    #@id = Psychologist.find_by_id(current_user.psychologist_id)
    @fi=params[:date_start]
    @ff=params[:date_end]

    @htmzt=0
    @htmz3=0
    @htmz4=0
    @htmz5=0
    @htmz6=0
    @mtmzt=0
    @mtmz3=0
    @mtmz4=0
    @mtmz5=0
    @mtmz6=0
    query6= TamizajesTot.where(:fecha => (params[:date_start]..params[:date_end]))
      temp=  query6.map {|item| item[:h]}
      temp.each do |e|
        @htmzt+=e
      end
      temp= query6.map {|item| item[:h3]}
      temp.each do |e|
        @htmz3+=e
      end
      temp= query6.map {|item| item[:h4]}
      temp.each do |e|
        @htmz4+=e
      end
      temp= query6.map {|item| item[:h5]}
      temp.each do |e|
        @htmz5+=e
      end
      temp= query6.map {|item| item[:h6]}
      temp.each do |e|
        @htmz6+=e
      end
      temp= query6.map {|item| item[:m]}
      temp.each do |e|
        @mtmzt+=e
      end
      temp= query6.map {|item| item[:m3]}
      temp.each do |e|
        @mtmz3+=e
      end
      temp= query6.map {|item| item[:m4]}
      temp.each do |e|
        @mtmz4+=e
      end
      temp= query6.map {|item| item[:m5]}
      temp.each do |e|
        @mtmz5+=e
      end
      temp= query6.map {|item| item[:m6]}
      temp.each do |e|
        @mtmz6+=e
      end


    @hapt=0
    @hap3=0
    @hap4=0
    @mapt=0
    @map3=0
    @map4=0

    query7= AdolescentesAp.where(:fecha => (params[:date_start]..params[:date_end]))
      temp=  query7.map {|item| item[:h]}
      temp.each do |e|
        @hapt+=e
      end
      temp= query7.map {|item| item[:h3]}
      temp.each do |e|
        @hap3+=e
      end
      temp= query7.map {|item| item[:h4]}
      temp.each do |e|
        @hap4+=e
      end
      temp= query7.map {|item| item[:m]}
      temp.each do |e|
        @mapt+=e
      end
      temp= query7.map {|item| item[:m3]}
      temp.each do |e|
        @map3+=e
      end
      temp= query7.map {|item| item[:m4]}
      temp.each do |e|
        @map4+=e
      end

      @hcpvt=0
      @hcpv1=0
      @hcpv2=0
      @hcpv3=0
      @hcpv4=0
      @hcpv5=0
      @hcpv6=0
      @hcpv7=0
      @hcpv8=0
      @hcpv9=0
      @hcpv10=0
      @mcpvt=0
      @mcpv1=0
      @mcpv2=0
      @mcpv3=0
      @mcpv4=0
      @mcpv5=0
      @mcpv6=0
      @mcpv7=0
      @mcpv8=0
      @mcpv9=0
      @mcpv10=0

     query8= ConsultasPv.where(:fecha => (params[:date_start]..params[:date_end]))
      temp=  query8.map {|item| item[:h]}
      temp.each do |e|
        @hcpvt+=e
      end
      temp=  query8.map {|item| item[:h1]}
      temp.each do |e|
        @hcpv1+=e
      end
      temp= query8.map {|item| item[:h2]}
      temp.each do |e|
        @hcpv2+=e
      end
      temp= query8.map {|item| item[:h3]}
      temp.each do |e|
        @hcpv3+=e
      end
      temp= query8.map {|item| item[:h4]}
      temp.each do |e|
        @hcpv4+=e
      end
      temp= query8.map {|item| item[:h5]}
      temp.each do |e|
        @hcpv5+=e
      end
      temp= query8.map {|item| item[:h6]}
      temp.each do |e|
        @hcpv6+=e
      end
      temp= query8.map {|item| item[:h7]}
      temp.each do |e|
        @hcpv7+=e
      end
      temp= query8.map {|item| item[:h8]}
      temp.each do |e|
        @hcpv8+=e
      end
      temp= query8.map {|item| item[:h9]}
      temp.each do |e|
        @hcpv9+=e
      end
      temp= query8.map {|item| item[:h10]}
      temp.each do |e|
        @hcpv10+=e
      end
      temp= query8.map {|item| item[:m]}
      temp.each do |e|
        @mcpvt+=e
      end
      temp= query8.map {|item| item[:m1]}
      temp.each do |e|
        @mcpv1+=e
      end
      temp= query8.map {|item| item[:m2]}
      temp.each do |e|
        @mcpv2+=e
      end
      temp= query8.map {|item| item[:m3]}
      temp.each do |e|
        @mcpv3+=e
      end
      temp= query8.map {|item| item[:m4]}
      temp.each do |e|
        @mcpv4+=e
      end
      temp= query8.map {|item| item[:m5]}
      temp.each do |e|
        @mcpv5+=e
      end
      temp= query8.map {|item| item[:m6]}
      temp.each do |e|
        @mcpv6+=e
      end
      temp= query8.map {|item| item[:m7]}
      temp.each do |e|
        @mcpv7+=e
      end
      temp= query8.map {|item| item[:m8]}
      temp.each do |e|
        @mcpv8+=e
      end
      temp= query8.map {|item| item[:m9]}
      temp.each do |e|
        @mcpv9+=e
      end
      temp= query8.map {|item| item[:m10]}
      temp.each do |e|
        @mcpv10+=e
      end


      @htbt=0
      @htb1=0
      @htb2=0
      @htb3=0
      @htb4=0
      @htb5=0
      @htb6=0
      @htb7=0
      @htb8=0
      @htb9=0
      @htb10=0
      @mtbt=0
      @mtb1=0
      @mtb2=0
      @mtb3=0
      @mtb4=0
      @mtb5=0
      @mtb6=0
      @mtb7=0
      @mtb8=0
      @mtb9=0
      @mtb10=0

    query9= TratamientosBc.where(:fecha => (params[:date_start]..params[:date_end]))
      temp=  query9.map {|item| item[:h]}
      temp.each do |e|
        @htbt+=e
      end
      temp=  query9.map {|item| item[:h1]}
      temp.each do |e|
        @htb1+=e
      end
      temp= query9.map {|item| item[:h2]}
      temp.each do |e|
        @htb2+=e
      end
      temp= query9.map {|item| item[:h3]}
      temp.each do |e|
        @htb3+=e
      end
      temp= query9.map {|item| item[:h4]}
      temp.each do |e|
        @htb4+=e
      end
      temp= query9.map {|item| item[:h5]}
      temp.each do |e|
        @htb5+=e
      end
      temp= query9.map {|item| item[:h6]}
      temp.each do |e|
        @htb6+=e
      end
      temp= query9.map {|item| item[:h7]}
      temp.each do |e|
        @htb7+=e
      end
      temp= query9.map {|item| item[:h8]}
      temp.each do |e|
        @htb8+=e
      end
      temp= query9.map {|item| item[:h9]}
      temp.each do |e|
        @htb9+=e
      end
      temp= query9.map {|item| item[:h10]}
      temp.each do |e|
        @htb10+=e
      end
      temp= query9.map {|item| item[:m]}
      temp.each do |e|
        @mtbt+=e
      end
      temp= query9.map {|item| item[:m1]}
      temp.each do |e|
        @mtb1+=e
      end
      temp= query9.map {|item| item[:m2]}
      temp.each do |e|
        @mtb2+=e
      end
      temp= query9.map {|item| item[:m3]}
      temp.each do |e|
        @mtb3+=e
      end
      temp= query9.map {|item| item[:m4]}
      temp.each do |e|
        @mtb4+=e
      end
      temp= query9.map {|item| item[:m5]}
      temp.each do |e|
        @mtb5+=e
      end
      temp= query9.map {|item| item[:m6]}
      temp.each do |e|
        @mtb6+=e
      end
      temp= query9.map {|item| item[:m7]}
      temp.each do |e|
        @mtb7+=e
      end
      temp= query9.map {|item| item[:m8]}
      temp.each do |e|
        @mtb8+=e
      end
      temp= query9.map {|item| item[:m9]}
      temp.each do |e|
        @mtb9+=e
      end
      temp= query9.map {|item| item[:m10]}
      temp.each do |e|
        @mtb10+=e
      end

    @hitt=0
    @hit3=0
    @hit4=0
    @mitt=0
    @mit3=0
    @mit4=0

    query10= AdolescentesIt.where(:fecha => (params[:date_start]..params[:date_end]))
      temp=  query10.map {|item| item[:h]}
      temp.each do |e|
        @hitt+=e
      end
      temp= query10.map {|item| item[:h3]}
      temp.each do |e|
        @hit3+=e
      end
      temp= query10.map {|item| item[:h4]}
      temp.each do |e|
        @hit4+=e
      end
      temp= query10.map {|item| item[:m]}
      temp.each do |e|
        @mitt+=e
      end
      temp= query10.map {|item| item[:m3]}
      temp.each do |e|
        @mit3+=e
      end
      temp= query10.map {|item| item[:m4]}
      temp.each do |e|
        @mit4+=e
      end


      @hcst=0
      @hcs1=0
      @hcs2=0
      @hcs3=0
      @hcs4=0
      @hcs5=0
      @hcs6=0
      @hcs7=0
      @hcs8=0
      @hcs9=0
      @hcs10=0
      @mcst=0
      @mcs1=0
      @mcs2=0
      @mcs3=0
      @mcs4=0
      @mcs5=0
      @mcs6=0
      @mcs7=0
      @mcs8=0
      @mcs9=0
      @mcs10=0

     query5= ConsultasSub.where(:fecha => (params[:date_start]..params[:date_end]))
      temp=  query5.map {|item| item[:h]}
      temp.each do |e|
        @hcst+=e
      end
      temp=  query5.map {|item| item[:h1]}
      temp.each do |e|
        @hcs1+=e
      end
      temp= query5.map {|item| item[:h2]}
      temp.each do |e|
        @hcs2+=e
      end
      temp= query5.map {|item| item[:h3]}
      temp.each do |e|
        @hcs3+=e
      end
      temp= query5.map {|item| item[:h4]}
      temp.each do |e|
        @hcs4+=e
      end
      temp= query5.map {|item| item[:h5]}
      temp.each do |e|
        @hcs5+=e
      end
      temp= query5.map {|item| item[:h6]}
      temp.each do |e|
        @hcs6+=e
      end
      temp= query5.map {|item| item[:h7]}
      temp.each do |e|
        @hcs7+=e
      end
      temp= query5.map {|item| item[:h8]}
      temp.each do |e|
        @hcs8+=e
      end
      temp= query5.map {|item| item[:h9]}
      temp.each do |e|
        @hcs9+=e
      end
      temp= query5.map {|item| item[:h10]}
      temp.each do |e|
        @hcs10+=e
      end
      temp= query5.map {|item| item[:m]}
      temp.each do |e|
        @mcst+=e
      end
      temp= query5.map {|item| item[:m1]}
      temp.each do |e|
        @mcs1+=e
      end
      temp= query5.map {|item| item[:m2]}
      temp.each do |e|
        @mcs2+=e
      end
      temp= query5.map {|item| item[:m3]}
      temp.each do |e|
        @mcs3+=e
      end
      temp= query5.map {|item| item[:m4]}
      temp.each do |e|
        @mcs4+=e
      end
      temp= query5.map {|item| item[:m5]}
      temp.each do |e|
        @mcs5+=e
      end
      temp= query5.map {|item| item[:m6]}
      temp.each do |e|
        @mcs6+=e
      end
      temp= query5.map {|item| item[:m7]}
      temp.each do |e|
        @mcs7+=e
      end
      temp= query5.map {|item| item[:m8]}
      temp.each do |e|
        @mcs8+=e
      end
      temp= query5.map {|item| item[:m9]}
      temp.each do |e|
        @mcs9+=e
      end
      temp= query5.map {|item| item[:m10]}
      temp.each do |e|
        @mcs10+=e
      end

      @hcot=0
      @hco1=0
      @hco2=0
      @hco3=0
      @hco4=0
      @hco5=0
      @hco6=0
      @hco7=0
      @hco8=0
      @hco9=0
      @hco10=0
      @mcot=0
      @mco1=0
      @mco2=0
      @mco3=0
      @mco4=0
      @mco5=0
      @mco6=0
      @mco7=0
      @mco8=0
      @mco9=0
      @mco10=0


     query= CapacitacionesOt.where(:fecha => (params[:date_start]..params[:date_end]))
      temp=  query.map {|item| item[:h]}
      temp.each do |e|
        @hcot+=e
      end
      #temp=  query.map {|item| item[:h1]}
      #temp.each do |e|
      #  @hco1+=e
      #end
      #temp= query.map {|item| item[:h2]}
      #temp.each do |e|
      #  @hco2+=e
      #end
      temp= query.map {|item| item[:h3]}
      temp.each do |e|
        @hco3+=e
      end
      temp= query.map {|item| item[:h4]}
      temp.each do |e|
        @hco4+=e
      end
      temp= query.map {|item| item[:h5]}
      temp.each do |e|
        @hco5+=e
      end
      temp= query.map {|item| item[:h6]}
      temp.each do |e|
        @hco6+=e
      end
      temp= query.map {|item| item[:h7]}
      temp.each do |e|
        @hco7+=e
      end
      temp= query.map {|item| item[:h8]}
      temp.each do |e|
        @hco8+=e
      end
      temp= query.map {|item| item[:h9]}
      temp.each do |e|
        @hco9+=e
      end
      temp= query.map {|item| item[:h10]}
      temp.each do |e|
        @hco10+=e
      end
      temp= query.map {|item| item[:m]}
      temp.each do |e|
        @mcot+=e
      end
      #temp= query.map {|item| item[:m1]}
      #temp.each do |e|
      #  @mco1+=e
      #end
      #temp= query.map {|item| item[:m2]}
      #temp.each do |e|
      #  @mco2+=e
      #end
      temp= query.map {|item| item[:m3]}
      temp.each do |e|
        @mco3+=e
      end
      temp= query.map {|item| item[:m4]}
      temp.each do |e|
        @mco4+=e
      end
      temp= query.map {|item| item[:m5]}
      temp.each do |e|
        @mco5+=e
      end
      temp= query.map {|item| item[:m6]}
      temp.each do |e|
        @mco6+=e
      end
      temp= query.map {|item| item[:m7]}
      temp.each do |e|
        @mco7+=e
      end
      temp= query.map {|item| item[:m8]}
      temp.each do |e|
        @mco8+=e
      end
      temp= query.map {|item| item[:m9]}
      temp.each do |e|
        @mco9+=e
      end
      temp= query.map {|item| item[:m10]}
      temp.each do |e|
        @mco10+=e
      end

      @hcrt=0
      @hcr1=0
      @hcr2=0
      @hcr3=0
      @hcr4=0
      @hcr5=0
      @hcr6=0
      @hcr7=0
      @hcr8=0
      @hcr9=0
      @hcr10=0
      @mcrt=0
      @mcr1=0
      @mcr2=0
      @mcr3=0
      @mcr4=0
      @mcr5=0
      @mcr6=0
      @mcr7=0
      @mcr8=0
      @mcr9=0
      @mcr10=0

    query2= CapacitacionesRec.where(:fecha => (params[:date_start]..params[:date_end]))
      temp= query2.map {|item| item[:h]}
      temp.each do |e|
        @hcrt+=e
      end
      temp= query2.map {|item| item[:h1]}
      temp.each do |e|
        @hcr1+=e
      end
      temp= query2.map {|item| item[:h2]}
      temp.each do |e|
        @hcr2+=e
      end
      temp= query2.map {|item| item[:h3]}
      temp.each do |e|
        @hcr3+=e
      end
      temp= query2.map {|item| item[:h4]}
      temp.each do |e|
        @hcr4+=e
      end
      temp= query2.map {|item| item[:h5]}
      temp.each do |e|
        @hcr5+=e
      end
      temp= query2.map {|item| item[:h6]}
      temp.each do |e|
        @hcr6+=e
      end
      temp= query2.map {|item| item[:h7]}
      temp.each do |e|
        @hcr7+=e
      end
      temp= query2.map {|item| item[:h8]}
      temp.each do |e|
        @hcr8+=e
      end
      temp= query2.map {|item| item[:h9]}
      temp.each do |e|
        @hcr9+=e
      end
      temp= query2.map {|item| item[:h10]}
      temp.each do |e|
        @hcr10+=e
      end
      temp= query2.map {|item| item[:m]}
      temp.each do |e|
        @mcrt+=e
      end
      temp= query2.map {|item| item[:m1]}
      temp.each do |e|
        @mcr1+=e
      end
      temp= query2.map {|item| item[:m2]}
      temp.each do |e|
        @mcr2+=e
      end
      temp= query2.map {|item| item[:m3]}
      temp.each do |e|
        @mcr3+=e
      end
      temp= query2.map {|item| item[:m4]}
      temp.each do |e|
        @mcr4+=e
      end
      temp= query2.map {|item| item[:m5]}
      temp.each do |e|
        @mcr5+=e
      end
      temp= query2.map {|item| item[:m6]}
      temp.each do |e|
        @mcr6+=e
      end
      temp= query2.map {|item| item[:m7]}
      temp.each do |e|
        @mcr7+=e
      end
      temp= query2.map {|item| item[:m8]}
      temp.each do |e|
        @mcr8+=e
      end
      temp= query2.map {|item| item[:m9]}
      temp.each do |e|
        @mcr9+=e
      end
      temp= query2.map {|item| item[:m10]}
      temp.each do |e|
        @mcr10+=e
      end


      @hpt=0
      @hp1=0
      @hp2=0
      @hp3=0
      @hp4=0
      @hp5=0
      @hp6=0
      @hp7=0
      @hp8=0
      @hp9=0
      @hp10=0
      @mpt=0
      @mp1=0
      @mp2=0
      @mp3=0
      @mp4=0
      @mp5=0
      @mp6=0
      @mp7=0
      @mp8=0
      @mp9=0
      @mp10=0


    query3= PersonalCt.where(:fecha => (params[:date_start]..params[:date_end]))
      temp= query3.map {|item| item[:h]}
      temp.each do |e|
        @hpt+=e
      end
      temp= query3.map {|item| item[:h1]}
      temp.each do |e|
        @hp1+=e
      end
      temp= query3.map {|item| item[:h2]}
      temp.each do |e|
        @hp2+=e
      end
      temp= query3.map {|item| item[:h3]}
      temp.each do |e|
        @hp3+=e
      end
      temp= query3.map {|item| item[:h4]}
      temp.each do |e|
        @hp4+=e
      end
      temp= query3.map {|item| item[:h5]}
      temp.each do |e|
        @hp5+=e
      end
      temp= query3.map {|item| item[:h6]}
      temp.each do |e|
        @hp6+=e
      end
      temp= query3.map {|item| item[:h7]}
      temp.each do |e|
        @hp7+=e
      end
      temp= query3.map {|item| item[:h8]}
      temp.each do |e|
        @hp8+=e
      end
      temp= query3.map {|item| item[:h9]}
      temp.each do |e|
        @hp9+=e
      end
      temp= query3.map {|item| item[:h10]}
      temp.each do |e|
        @hp10+=e
      end
      temp= query3.map {|item| item[:m]}
      temp.each do |e|
        @mpt+=e
      end
      temp= query3.map {|item| item[:m1]}
      temp.each do |e|
        @mp1+=e
      end
      temp= query3.map {|item| item[:m2]}
      temp.each do |e|
        @mp2+=e
      end
      temp= query3.map {|item| item[:m3]}
      temp.each do |e|
        @mp3+=e
      end
      temp= query3.map {|item| item[:m4]}
      temp.each do |e|
        @mp4+=e
      end
      temp= query3.map {|item| item[:m5]}
      temp.each do |e|
        @mp5+=e
      end
      temp= query3.map {|item| item[:m6]}
      temp.each do |e|
        @mp6+=e
      end
      temp= query3.map {|item| item[:m7]}
      temp.each do |e|
        @mp7+=e
      end
      temp= query3.map {|item| item[:m8]}
      temp.each do |e|
        @mp8+=e
      end
      temp= query3.map {|item| item[:m9]}
      temp.each do |e|
        @mp9+=e
      end
      temp= query3.map {|item| item[:m10]}
      temp.each do |e|
        @mp10+=e
      end


      @hvt=0
      @hv1=0
      @hv2=0
      @hv3=0
      @hv4=0
      @hv5=0
      @hv6=0
      @hv7=0
      @hv8=0
      @hv9=0
      @hv10=0
      @mvt=0
      @mv1=0
      @mv2=0
      @mv3=0
      @mv4=0
      @mv5=0
      @mv6=0
      @mv7=0
      @mv8=0
      @mv9=0
      @mv10=0

    query4= PersonasVg.where(:fecha => (params[:date_start]..params[:date_end]))
      temp= query4.map {|item| item[:h]}
      temp.each do |e|
        @hvt+=e
      end
      temp= query4.map {|item| item[:h1]}
      temp.each do |e|
        @hv1+=e
      end
      temp= query4.map {|item| item[:h2]}
      temp.each do |e|
        @hv2+=e
      end
      temp= query4.map {|item| item[:h3]}
      temp.each do |e|
        @hv3+=e
      end
      temp= query4.map {|item| item[:h4]}
      temp.each do |e|
        @hv4+=e
      end
      temp= query4.map {|item| item[:h5]}
      temp.each do |e|
        @hv5+=e
      end
      temp= query4.map {|item| item[:h6]}
      temp.each do |e|
        @hv6+=e
      end
      temp= query4.map {|item| item[:h7]}
      temp.each do |e|
        @hv7+=e
      end
      temp= query4.map {|item| item[:h8]}
      temp.each do |e|
        @hv8+=e
      end
      temp= query4.map {|item| item[:h9]}
      temp.each do |e|
        @hv9+=e
      end
      temp= query4.map {|item| item[:h10]}
      temp.each do |e|
        @hv10+=e
      end
      temp= query4.map {|item| item[:m]}
      temp.each do |e|
        @mvt+=e
      end
      temp= query4.map {|item| item[:m1]}
      temp.each do |e|
        @mv1+=e
      end
      temp= query4.map {|item| item[:m2]}
      temp.each do |e|
        @mv2+=e
      end
      temp= query4.map {|item| item[:m3]}
      temp.each do |e|
        @mv3+=e
      end
      temp= query4.map {|item| item[:m4]}
      temp.each do |e|
        @mv4+=e
      end
      temp= query4.map {|item| item[:m5]}
      temp.each do |e|
        @mv5+=e
      end
      temp= query4.map {|item| item[:m6]}
      temp.each do |e|
        @mv6+=e
      end
      temp= query4.map {|item| item[:m7]}
      temp.each do |e|
        @mv7+=e
      end
      temp= query4.map {|item| item[:m8]}
      temp.each do |e|
        @mv8+=e
      end
      temp= query4.map {|item| item[:m9]}
      temp.each do |e|
        @mv9+=e
      end
      temp= query4.map {|item| item[:m10]}
      temp.each do |e|
        @mv10+=e
      end

      @hbt=0
      @hb1=0
      @hb2=0
      @hb3=0
      @hb4=0
      @hb5=0
      @hb6=0
      @hb7=0
      @hb8=0
      @hb9=0
      @hb10=0
      @mbt=0
      @mb1=0
      @mb2=0
      @mb3=0
      @mb4=0
      @mb5=0
      @mb6=0
      @mb7=0
      @mb8=0
      @mb9=0
      @mb10=0

      query11= Beneficiado.where(:fecha => (params[:date_start]..params[:date_end]))
      temp= query11.map {|item| item[:h]}
      temp.each do |e|
        @hbt+=e
      end
      temp= query11.map {|item| item[:h1]}
      temp.each do |e|
        @hb1+=e
      end
      temp= query11.map {|item| item[:h2]}
      temp.each do |e|
        @hb2+=e
      end
      temp= query11.map {|item| item[:h3]}
      temp.each do |e|
        @hb3+=e
      end
      temp= query11.map {|item| item[:h4]}
      temp.each do |e|
        @hb4+=e
      end
      temp= query11.map {|item| item[:h5]}
      temp.each do |e|
        @hb5+=e
      end
      temp= query11.map {|item| item[:h6]}
      temp.each do |e|
        @hb6+=e
      end
      temp= query11.map {|item| item[:h7]}
      temp.each do |e|
        @hb7+=e
      end
      temp= query11.map {|item| item[:h8]}
      temp.each do |e|
        @hb8+=e
      end
      temp= query11.map {|item| item[:h9]}
      temp.each do |e|
        @hb9+=e
      end
      temp= query11.map {|item| item[:h10]}
      temp.each do |e|
        @hb10+=e
      end
      temp= query11.map {|item| item[:m]}
      temp.each do |e|
        @mbt+=e
      end
      temp= query11.map {|item| item[:m1]}
      temp.each do |e|
        @mb1+=e
      end
      temp= query11.map {|item| item[:m2]}
      temp.each do |e|
        @mb2+=e
      end
      temp= query11.map {|item| item[:m3]}
      temp.each do |e|
        @mb3+=e
      end
      temp= query11.map {|item| item[:m4]}
      temp.each do |e|
        @mb4+=e
      end
      temp= query11.map {|item| item[:m5]}
      temp.each do |e|
        @mb5+=e
      end
      temp= query11.map {|item| item[:m6]}
      temp.each do |e|
        @mb6+=e
      end
      temp= query11.map {|item| item[:m7]}
      temp.each do |e|
        @mb7+=e
      end
      temp= query11.map {|item| item[:m8]}
      temp.each do |e|
        @mb8+=e
      end
      temp= query11.map {|item| item[:m9]}
      temp.each do |e|
        @mb9+=e
      end
      temp= query11.map {|item| item[:m10]}
      temp.each do |e|
        @mb10+=e
      end


      @hmpt=0
      @hmp1=0
      @hmp2=0
      @hmp3=0
      @hmp4=0
      @hmp5=0
      @hmp6=0
      @hmp7=0
      @hmp8=0
      @hmp9=0
      @hmp10=0
      @mmpt=0
      @mmp1=0
      @mmp2=0
      @mmp3=0
      @mmp4=0
      @mmp5=0
      @mmp6=0
      @mmp7=0
      @mmp8=0
      @mmp9=0
      @mmp10=0

      query12= Multyprom.where(:fecha => (params[:date_start]..params[:date_end]))
      temp= query12.map {|item| item[:h]}
      temp.each do |e|
        @hmpt+=e
      end
      temp= query12.map {|item| item[:h1]}
      temp.each do |e|
        @hmp1+=e
      end
      temp= query12.map {|item| item[:h2]}
      temp.each do |e|
        @hmp2+=e
      end
      temp= query12.map {|item| item[:h3]}
      temp.each do |e|
        @hmp3+=e
      end
      temp= query12.map {|item| item[:h4]}
      temp.each do |e|
        @hmp4+=e
      end
      temp= query12.map {|item| item[:h5]}
      temp.each do |e|
        @hmp5+=e
      end
      temp= query12.map {|item| item[:h6]}
      temp.each do |e|
        @hmp6+=e
      end
      temp= query12.map {|item| item[:h7]}
      temp.each do |e|
        @hmp7+=e
      end
      temp= query12.map {|item| item[:h8]}
      temp.each do |e|
        @hmp8+=e
      end
      temp= query12.map {|item| item[:h9]}
      temp.each do |e|
        @hmp9+=e
      end
      temp= query12.map {|item| item[:h10]}
      temp.each do |e|
        @hmp10+=e
      end
      temp= query12.map {|item| item[:m]}
      temp.each do |e|
        @mmpt+=e
      end
      temp= query12.map {|item| item[:m1]}
      temp.each do |e|
        @mmp1+=e
      end
      temp= query12.map {|item| item[:m2]}
      temp.each do |e|
        @mmp2+=e
      end
      temp= query12.map {|item| item[:m3]}
      temp.each do |e|
        @mmp3+=e
      end
      temp= query12.map {|item| item[:m4]}
      temp.each do |e|
        @mmp4+=e
      end
      temp= query12.map {|item| item[:m5]}
      temp.each do |e|
        @mmp5+=e
      end
      temp= query12.map {|item| item[:m6]}
      temp.each do |e|
        @mmp6+=e
      end
      temp= query12.map {|item| item[:m7]}
      temp.each do |e|
        @mmp7+=e
      end
      temp= query12.map {|item| item[:m8]}
      temp.each do |e|
        @mmp8+=e
      end
      temp= query12.map {|item| item[:m9]}
      temp.each do |e|
        @mmp9+=e
      end
      temp= query12.map {|item| item[:m10]}
      temp.each do |e|
        @mmp10+=e
      end



    respond_to do |format|
      format.html
      format.pdf do
        render pdf:"Reporte",
        #header: { center: 'hola' },
        #footer: { center: 'hola' },
        header: {
          content: render_to_string(template: 'layouts/header.html.pdf.erb')
        },
        footer: {
          content: render_to_string(layout: 'layouts/footer.html.pdf.erb')
        },
        layout: 'layouts/pdf.html.erb',
        page_size: 'letter',
        file:'imca/index.pdf.erb'
      end
    end    
    

  end

  def show
    
  end

  def new
    
  end

  def edit

  end

  def create   

  end

  def update
  end

  def destroy

  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_imca
   
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def imca_params
     
  end


end
