class CapacitacionesRecsController < ApplicationController
  before_action :set_capacitaciones_rec, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /capacitaciones_recs
  # GET /capacitaciones_recs.json
  def index
    @capacitaciones_recs = CapacitacionesRec.all
  end

  # GET /capacitaciones_recs/1
  # GET /capacitaciones_recs/1.json
  def show
  end

  # GET /capacitaciones_recs/new
  def new
    @capacitaciones_rec = CapacitacionesRec.new
  end

  # GET /capacitaciones_recs/1/edit
  def edit
  end

  # POST /capacitaciones_recs
  # POST /capacitaciones_recs.json
  def create
    @capacitaciones_rec = CapacitacionesRec.new(capacitaciones_rec_params)

    respond_to do |format|
      if @capacitaciones_rec.save
        format.html { redirect_to @capacitaciones_rec, notice: 'Capacitaciones rec was successfully created.' }
        format.json { render :show, status: :created, location: @capacitaciones_rec }
      else
        format.html { render :new }
        format.json { render json: @capacitaciones_rec.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /capacitaciones_recs/1
  # PATCH/PUT /capacitaciones_recs/1.json
  def update
    respond_to do |format|
      if @capacitaciones_rec.update(capacitaciones_rec_params)
        format.html { redirect_to @capacitaciones_rec, notice: 'Capacitaciones rec was successfully updated.' }
        format.json { render :show, status: :ok, location: @capacitaciones_rec }
      else
        format.html { render :edit }
        format.json { render json: @capacitaciones_rec.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /capacitaciones_recs/1
  # DELETE /capacitaciones_recs/1.json
  def destroy
    @capacitaciones_rec.destroy
    respond_to do |format|
      format.html { redirect_to capacitaciones_recs_url, notice: 'Capacitaciones rec was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_capacitaciones_rec
      @capacitaciones_rec = CapacitacionesRec.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def capacitaciones_rec_params
      params.require(:capacitaciones_rec).permit(:h, :m, :fecha, :psychologist_id,:h1, :h2, :h3, :h4, :h5, :h6, :h7, :h8, :h9, :h10, :m1, :m2, :m3, :m4, :m5, :m6, :m7, :m8, :m9 ,:m10)
    end
end
