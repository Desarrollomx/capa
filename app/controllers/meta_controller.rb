class MetaController < ApplicationController
  before_action :set_metum, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /meta
  # GET /meta.json
  def index
    #@psychologists= Psychologist.all
    @meta = Metum.where(:fecha => (params[:date_start]..params[:date_end]))
    #@p= @meta.map { |e| e[:tamizajes]}
    @metas = Metum.ultimas
  end

  # GET /meta/1
  # GET /meta/1.json
  def show
  end

  # GET /meta/new
  def new
    @metas = Metum.new
  end

  # GET /meta/1/edit
  def edit
  end

  # POST /meta
  # POST /meta.json
  def create
    @metas = Metum.new(metum_params)

    respond_to do |format|
      if @metas.save
        format.html { redirect_to @metas, notice: 'Metum was successfully created.' }
        format.json { render :show, status: :created, location: @metas }
      else
        format.html { render :new }
        format.json { render json: @metas.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meta/1
  # PATCH/PUT /meta/1.json
  def update
    respond_to do |format|
      if @metas.update(metum_params)
        format.html { redirect_to @metas, notice: 'Metum was successfully updated.' }
        format.json { render :show, status: :ok, location: @metas }
      else
        format.html { render :edit }
        format.json { render json: @metas.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meta/1
  # DELETE /meta/1.json
  def destroy
    @metas.destroy
    respond_to do |format|
      format.html { redirect_to meta_url, notice: 'Metum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_metum
      @metas = Metum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def metum_params
      params.require(:metum).permit(:tamizajes, :adol_acc_prev, :cons_1a_vez, :trat_brev_conc, :adol_ini_trat, :cons_subs, :pers_cap_ces_tab, :cap_otor_prev_trat, :cap_rec_prev_trat, :pers_det_viol_gen, :fecha, :beneficiados, :multip_promot)
    end
end
