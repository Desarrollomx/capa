class WelcomeController < ApplicationController
	#before_action :authenticate_user!
	def index
		date = Date.today
		@meetings = Meeting.all
		@m = Meeting.where("start_time >= ? AND start_time <= ? ", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31"))
		@psychologists = Psychologist.all
		@meetings_count = Meeting.where("start_time >= ? AND start_time <= ? AND psychologist_id = ? AND termino = ?", (date.strftime "%Y-%m-01"),(date.strftime "%Y-%m-31") ,current_user.psychologist_id, 2).count
    @nombre = Psychologist.find_by_id(current_user.psychologist_id)
		respond_to do |format|
      format.html
      format.pdf do
        render pdf:'Agenda-'+Time.now.strftime("%m-%Y"),
				header: {
          content: render_to_string(template: 'layouts/header.html.pdf.erb')
        },
        footer: {
          content: render_to_string(layout: 'layouts/footer.html.pdf.erb')
        },
        layout: 'layouts/pdf.html.erb',
        page_size: 'letter',
        file:'welcome/index.pdf.erb'
      end
    end
	end

	def unregistered
	end
end
