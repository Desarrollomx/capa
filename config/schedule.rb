# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

#set :bundle_command, "/usr/local/bin/bundle"
#PATH=$PATH: "/usr/local/lib"

#require File.expand_path('../boot', __FILE__)
#require "rails/all"
env :GEM_PATH, ENV['GEM_PATH']
env :PATH, ENV['PATH']
#set :environment, :development
set :enviroment, :production
set :output, { error: "/home/atlacomulco/capa/log/error.log", standard: "/home/atlacomulco/capa/log/cron.log" }

every '00 10 20 * *' do# :month, :at=> '16:00' do 
  for i in 1..6 do	
   runner "ConsultasPv.query(#{i}, #{1})"
   #command "echo #{fechaIni} ' Los datos se han insertado exitosamente ' #{fechaFin}"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
  end
end

every '00 10 1 * *' do# :month, :at=> '16:00' do 
  for i in 1..6 do  
   runner "ConsultasPv.query(#{i}, #{2})"
  end
  #command "echo #{fechaIni} ' Los datos se han insertado exitosamente ' #{fechaFin}"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }

end

every '10 10 20 * *' do# :month, :at=> '16:00' do   
  for i in 1..6 do  
   runner "ConsultasSub.query(#{i},#{1})"
  end
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

every '10 11 1 * *' do# :month, :at=> '16:00' do   
  for i in 1..6 do	
   runner "ConsultasSub.query(#{i},#{2})"
  end
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

every '20 10 20 * *' do# :month, :at=> '16:00' do 
  for i in 1..6 do	
   runner "TratamientosBc.query(#{i},#{1})"
  end
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

every '20 11 1 * *' do# :month, :at=> '16:00' do 
  for i in 1..6 do  
   runner "TratamientosBc.query(#{i},#{2})"
  end
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

#every '30 10 20 * *' do# :month, :at=> '16:00' do 
  #for i in 1..6 do	
    #runner "AdolescentesAp.query(#{i},#{1})"
  #end
#end

#every '30 10 1 * *' do# :month, :at=> '16:00' do 
  #for i in 1..6 do  
    #runner "AdolescentesAp.query(#{i},#{2},)"
  #end
#end
 
every '30 10 20 * *' do# :month, :at=> '16:00' do    
  for i in 1..6 do	
   runner "AdolescentesIt.query(#{i},#{1})"

  end
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

every '30 10 1 * *' do# :month, :at=> '16:00' do 
  for i in 1..6 do  
   runner "AdolescentesIt.query(#{i},#{2})"

  end
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

every '40 10 20 * *' do# :month, :at=> '16:00' do 
  for i in 1..6 do	
   runner "TamizajesTot.query(#{i},#{1})"
  end
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

every '40 10 1 * *' do# :month, :at=> '16:00' do 
  for i in 1..6 do  
   runner "TamizajesTot.query(#{i},#{2})"
  end
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

every '50 11 1 * *' do# :month, :at=> '16:00' do 
   runner "Metum.query"
   #command "echo 'Los datos se han insertado exitosamente'"#, output: { error: "/home/benja/Proyectos_Rails/capa/log/error.log", standard: "/home/benja/Proyectos_Rails/capa/log/cron.log" }
end

