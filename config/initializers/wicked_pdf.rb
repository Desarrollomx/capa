# WickedPDF Global Configuration
#
# Use this to set up shared configuration options for your entire application.
# Any of the configuration options shown here can also be applied to single
# models by passing arguments to the `render :pdf` call.
#
# To learn more, check out the README:
#
# https://github.com/mileszs/wicked_pdf/blob/master/README.md


WickedPdf.config = {
  #exe_path: '/usr/local/bin/wkhtmltopdf'
  # Path to the wkhtmltopdf executable: This usually isn't needed if using
  # one of the wkhtmltopdf-binary family of gems.
  #exe_path: '/usr/local/bin/wkhtmltopdf',
  #exe_path: '#{Rails.root}/bin/wkhtmltopdf',
  #   or
  exe_path: Gem.bin_path('wkhtmltopdf-binary', 'wkhtmltopdf'),
  javascript_delay: 3000,
  :layout => 'layout',
  :margin => {
    :top => 38,
    :bottom => 25
  }
  # Layout file to be used for all PDFs
  # (but can be overridden in `render :pdf` calls)
  # layout: 'pdf.html',
}



def wicked_pdf_stylesheet_link_tag(*sources)
   sources.collect { |source|
     asset = Rails.application.assets.find_asset("#{source}.css")

     if asset.nil?
       raise "could not find asset for #{source}.css"
     else
       "<style type='text/css'>#{asset.body}</style>"
     end
   }.join("\n").gsub(/url\(['"](.+)['"]\)(.+)/,%[url("#{wicked_pdf_image_location("\\1")}")\\2]).html_safe
 end
