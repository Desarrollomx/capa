Rails.application.routes.draw do

  resources :multyproms
  resources :beneficiados
  resources :riesgos
  resources :familiars
    post '/tamizajes' => "apptamizajes#create2", :controller => "apptamizajes"
    post  '/tamizajes/escuela/:id' => "tamizajes#show_tamizaje", as: "show_tamizaje"
    post  '/tamizajes/escuela/grupos/:id' => "tamizajes#show_grupos", as: "show_grupos"
    post  '/tamizajes/escuela/termino_tam/:id' => "tamizajes#termino_tam", as: "termino_tam"
    get  '/tamizajes/escuela/grupos/riesgo/:id' => "tamizajes#show_riesgo", as: "show_riesgo"

  resources :parentescos
  resources :etnia
  resources :civils
    #resources :tamizajes do
    get     "/prueba/:id" => "tamizajes#prueba", as: "prueba"
    post    "/tamizajes/.:id" => "tamizajes#index", as: "escuelas_new_flag"
    get    "/tamizajes/" => "tamizajes#index", as: "escuelas"
    post   "/tamizajes/escuela/tam/new/create" => "tamizajes#create", :controller =>"tamizajes", as: "tam_create"
    post    "/tamizajes/escuela/tam/new"  => "tamizajes#new", as: "new_tamizaje"
    get    "/tamizajes/escuela/:id/edit" => "tamizajes#edit", as: "edit_tamizaje"
    get    "/tamizajes/:id" => "tamizajes#show", as: "tamizaje"
    patch  "/tamizajes/escuela/:id" => "tamizajes#update"
    put    "/tamizajes/escuela/:id" => "tamizajes#update"
    delete "/tamizajes/escuela/:id" => "tamizajes#destroy"
    #end

    resources :estatuses
  resources :patients #do
  get '/patients/datos/:id', to: 'patients#datos', as: 'datos'
    #get :datos
  #end
  #get "meetings/conteo/:id"  :controller(/:action(/:id))
  #get 'meetings/:id/actualizar'
  get '/meetings/actualizar/:id', to: 'meetings#actualizar', as: 'actualiza'
  #get ':controller(/:action(/:id))'
  resources :imca
  resources :usuarios
  resources :tamizajes_tots
  resources :tratamientos_bcs
  resources :personal_cts
  resources :personas_vgs
  resources :consultas_subs
  resources :consultas_pvs
  resources :capacitaciones_recs
  resources :capacitaciones_ots
  resources :adolescentes_its
  resources :adolescentes_aps
  resources :indicadores
  resources :meta
  #resources :tamizajes
  resources :psychologists
  resources :expedientes
  resources :sustancia
  resources :programas
  resources :states
  resources :municipios
  resources :meetings
  resources :reportes
  resources :siceca
  resources :indicadoresxedad
  resources :reporte_pacientes
    
  devise_for :users

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  resources :user
  get "welcome/index"
  get "meetings/new"

  authenticated :user do
    root 'welcome#index'
  end

  unauthenticated :user do
    devise_scope :user do
      root "welcome#unregistered", as: :unregistered_root
    end
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
